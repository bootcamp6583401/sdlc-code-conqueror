import axios from "@/lib/axios"
import { VehicleType } from "@/types"

export default function vehicleTypesService(token: string) {
  const getVehicleTypes = async () => {
    const res = await axios.get(`/vehicle-type/all`)

    return res.data as VehicleType[]
  }

  const createVehicleType = async (name: string, feePerHour: number) => {
    const res = await axios.post(
      `/vehicle-type`,
      {
        name,
        feePerHour,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return res.data as VehicleType
  }

  const updateVehicleTypeById = async (
    id: number,
    name: string,
    feePerHour: number,
  ) => {
    const res = await axios.put(
      `/vehicle-type/${id}`,
      {
        name,
        feePerHour,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return res.data as VehicleType
  }

  const deleteVehicleTypeById = async (id: number) => {
    await axios.delete(`/vehicle-type/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
  }

  return {
    getVehicleTypes,
    createVehicleType,
    deleteVehicleTypeById,
    updateVehicleTypeById,
  }
}

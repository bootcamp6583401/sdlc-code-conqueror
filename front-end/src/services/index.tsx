import axios from "@/lib/axios"
import { User, VehicleType } from "@/types"

export const getVehicleTypes = async () => {
  const res = await axios.get(`/vehicle-type/all`)

  return res.data as VehicleType[]
}

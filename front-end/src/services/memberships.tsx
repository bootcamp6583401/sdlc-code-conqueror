import axios from "@/lib/axios"
import { Membership, User } from "@/types"

export const grantMembership = async (userId: number, end: string) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.post(
    "/membership",
    {
      user: {
        id: userId,
      },
      end,
    },
    {
      headers: {
        Authorization: `Bearer ${user.accessToken}`,
      },
    },
  )

  return res.data as Membership
}

export const revokeMembership = async (id: number) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  await axios.delete(`/membership/${id}`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })
}

export const getMembershipToken = async ({
  vehicleTypeId,
  plateNumber,
  expiryDate,
  userId,
}: {
  vehicleTypeId: number
  plateNumber: string
  expiryDate: string
  userId: number
}) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.post(
    `/pay/membership`,
    {
      plateNumber,
      vehicleTypeId,
      expiryDate,
      userId,
    },
    {
      headers: {
        Authorization: `Bearer ${user.accessToken}`,
      },
    },
  )

  return res.data as string
}

export const handlePayMembershipSuccess = async (
  orderId: string,
  plateNumber: string,
  expiryDate: string,
  vehicleTypeId: number,
  userId: number,
) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.post(
    `/pay/handle-success/${orderId}`,
    {
      plateNumber,
      expiryDate,
      vehicleTypeId,
      userId,
    },
    {
      headers: {
        Authorization: `Bearer ${user.accessToken}`,
      },
    },
  )

  return res.data as Membership
}

export default function membershipService(token: string) {
  const grantMembership = async (
    userId: number,
    end: string,
    plateNumber: string,
    vehicleTypeId: number,
  ) => {
    const res = await axios.post(
      "/membership",
      {
        user: {
          id: userId,
        },
        vehicleType: {
          id: vehicleTypeId,
        },
        plateNumber,
        end,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return res.data as Membership
  }

  const revokeMembership = async (id: number) => {
    await axios.delete(`/membership/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
  }

  const getMembershipToken = async ({
    vehicleTypeId,
    plateNumber,
    expiryDate,
    userId,
  }: {
    vehicleTypeId: number
    plateNumber: string
    expiryDate: string
    userId: number
  }) => {
    const res = await axios.post(
      `/pay/membership`,
      {
        plateNumber,
        vehicleTypeId,
        expiryDate,
        userId,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return res.data as string
  }

  const handlePayMembershipSuccess = async (
    orderId: string,
    plateNumber: string,
    expiryDate: string,
    vehicleTypeId: number,
    userId: number,
  ) => {
    const res = await axios.post(
      `/pay/handle-success/${orderId}`,
      {
        plateNumber,
        expiryDate,
        vehicleTypeId,
        userId,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return res.data as Membership
  }
  return {
    grantMembership,
    revokeMembership,
    getMembershipToken,
    handlePayMembershipSuccess,
  }
}

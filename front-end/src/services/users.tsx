import axios from "@/lib/axios"
import { IUser, Page, User } from "@/types"

export const getAllUsers = async () => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.get(`/user`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })

  return res.data as IUser[]
}

export const getPagebleUsers = async (pageNumber: number = 0) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.get(`/user/paginated?page=${pageNumber}`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })

  return res.data as Page<IUser>
}

export const getUserById = async (id: number) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.get(`/user/${id}`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })

  return res.data as IUser
}

export const updateUserById = async (
  id: number,
  { phone }: { phone: string },
) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  await axios.put(
    `/user/${id}`,
    {
      phone,
    },
    {
      headers: {
        Authorization: `Bearer ${user.accessToken}`,
      },
    },
  )
}

export const uploadProfileImage = async (id: number, image: File) => {
  const formData = new FormData()

  formData.append("image", image)
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.put(`/user/${id}/update-profile-image`, formData, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
      "Content-Type": "multipart/form-data",
    },
  })

  return res.data as IUser
}

export const searchUserByKeyword = async (keyword: string) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.get(`/user/search=keyword?${keyword}`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })

  return res.data as IUser
}

export default function usersService(token: string) {
  const getAllUsers = async () => {
    const res = await axios.get(`/user`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data as IUser[]
  }

  const getPagebleUsers = async (pageNumber: number = 0) => {
    const res = await axios.get(`/user/paginated?page=${pageNumber}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data as Page<IUser>
  }

  const getUserById = async (id: number) => {
    const res = await axios.get(`/user/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data as IUser
  }

  const updateUserById = async (id: number, { phone }: { phone: string }) => {
    await axios.put(
      `/user/${id}`,
      {
        phone,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )
  }

  const uploadProfileImage = async (id: number, image: File) => {
    const formData = new FormData()

    formData.append("image", image)
    const res = await axios.put(`/user/${id}/update-profile-image`, formData, {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "multipart/form-data",
      },
    })

    return res.data as IUser
  }

  const searchUserByKeyword = async (keyword: string) => {
    const res = await axios.get(`/user/search=keyword?${keyword}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data as IUser
  }

  return {
    getAllUsers,
    getPagebleUsers,
    getUserById,
    uploadProfileImage,
    searchUserByKeyword,
    updateUserById,
  }
}

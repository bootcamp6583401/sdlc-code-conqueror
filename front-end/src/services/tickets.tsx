import axios from "@/lib/axios"
import { Page, Ticket, User } from "@/types"

export const generateTicket = async (
  plateNumber: string,
  vehicleTypeId: number,
  userId: number,
) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.post(
    "/ticket",
    {
      plateNumber: plateNumber,
      vehicleType: {
        id: vehicleTypeId,
      },
      user: {
        id: userId,
      },
    },
    {
      headers: {
        Authorization: `Bearer ${user.accessToken}`,
      },
    },
  )

  return res.data as Ticket
}

export const generateGuestTicket = async (
  plateNumber: string,
  vehicleTypeId: number,
) => {
  const res = await axios.post("/ticket/guest", {
    plateNumber: plateNumber,
    vehicleType: {
      id: vehicleTypeId,
    },
  })

  return res.data as Ticket
}

export const getTicketById = async (id: string) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User

  const res = await axios.get(`/ticket/${id}`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })

  return res.data as Ticket
}

export const guestGetTicketById = async (id: string) => {
  const res = await axios.get(`/ticket/guest/${id}`)

  return res.data as Ticket
}

export const getUserTickets = async (
  userId: number,
  pageNumber: number = 0,
) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.get(`/user/${userId}/tickets?page=${pageNumber}`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })

  return res.data as Page<Ticket>
}

export const getAllTickets = async () => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.get(`/ticket`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })

  return res.data as Ticket[]
}

export const getPagebleTickets = async (pageNumber: number = 0) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.get(`/ticket/paginated?page=${pageNumber}`, {
    headers: {
      Authorization: `Bearer ${user.accessToken}`,
    },
  })

  return res.data as Page<Ticket>
}

export const payTicket = async (id: string) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.put(
    `/ticket/${id}/pay`,
    {},
    {
      headers: {
        Authorization: `Bearer ${user.accessToken}`,
      },
    },
  )

  return res.data as Ticket
}

export const admitTicket = async (id: string) => {
  const user = JSON.parse(localStorage.getItem("user")!) as User
  const res = await axios.put(
    `/ticket/${id}/admit`,
    {},
    {
      headers: {
        Authorization: `Bearer ${user.accessToken}`,
      },
    },
  )

  return res.data as Ticket
}

export default function ticketsService(token: string) {
  const generateTicket = async (
    plateNumber: string,
    vehicleTypeId: number,
    userId: number,
  ) => {
    const res = await axios.post(
      "/ticket",
      {
        plateNumber: plateNumber,
        vehicleType: {
          id: vehicleTypeId,
        },
        user: {
          id: userId,
        },
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return res.data as Ticket
  }

  const generateGuestTicket = async (
    plateNumber: string,
    vehicleTypeId: number,
  ) => {
    const res = await axios.post("/ticket/guest", {
      plateNumber: plateNumber,
      vehicleType: {
        id: vehicleTypeId,
      },
    })

    return res.data as Ticket
  }

  const getTicketById = async (id: string) => {
    const res = await axios.get(`/ticket/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data as Ticket
  }

  const guestGetTicketById = async (id: string) => {
    const res = await axios.get(`/ticket/guest/${id}`)

    return res.data as Ticket
  }

  const getUserTickets = async (userId: number, pageNumber: number = 0) => {
    const res = await axios.get(`/user/${userId}/tickets?page=${pageNumber}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data as Page<Ticket>
  }

  const getAllTickets = async () => {
    const res = await axios.get(`/ticket`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data as Ticket[]
  }

  const getPagebleTickets = async (pageNumber: number = 0) => {
    const res = await axios.get(`/ticket/paginated?page=${pageNumber}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })

    return res.data as Page<Ticket>
  }

  const payTicket = async (id: string) => {
    const res = await axios.put(
      `/ticket/${id}/pay`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return res.data as Ticket
  }

  const admitTicket = async (id: string) => {
    const res = await axios.put(
      `/ticket/${id}/admit`,
      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    )

    return res.data as Ticket
  }

  return {
    admitTicket,
    payTicket,
    getPagebleTickets,
    getAllTickets,
    getUserTickets,
    generateGuestTicket,
    getTicketById,
    generateTicket,
    guestGetTicketById,
  }
}

import { BoolFilter, IUser, Ticket, TicketState, VehicleType } from "@/types"
import { type ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"
import html2canvas from "html2canvas"
import { format, isWithinInterval, parseISO } from "date-fns"
import * as XLSX from "xlsx"

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

export function countHoursBetween(startDate: Date, endDate: Date) {
  const differenceMs = endDate.getTime() - startDate.getTime()

  const hours = differenceMs / (1000 * 60 * 60)

  const roundedHours = Math.ceil(hours)

  return roundedHours
}

export function countDaysBetween(startDate: Date, endDate: Date) {
  const differenceMs = endDate.getTime() - startDate.getTime()

  const hours = differenceMs / (1000 * 60 * 60 * 24)

  const roundedDays = Math.ceil(hours)

  return roundedDays
}

export const formRupiah = (num: number) =>
  new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    currencySign: "accounting",
    minimumFractionDigits: 0,
  }).format(num)

export const isUserAdmin = (user: IUser) =>
  user.roles.some(r => r.name === "ROLE_ADMIN")

export const mapTicketState = (
  ticketState: TicketState,
  valIfPending: any,
  valIfActive: any,
  valIfPaid: any,
) => {
  if (ticketState === TicketState.ACTIVE) return valIfActive
  if (ticketState === TicketState.PENDING) return valIfPending
  if (ticketState === TicketState.PAID) return valIfPaid
}

export const getProjectedTicketTotal = (ticket: Ticket): number => {
  if (ticket.paymentTime) return ticket.totalFee!
  if (!ticket.entryTime) return 0
  const totalHours = countHoursBetween(new Date(ticket.entryTime), new Date())

  return ticket.doesUserHaveActiveMembership
    ? 0
    : totalHours * ticket.feePerHour
}

export const filterTickets = (
  tickets: Ticket[],
  ticketFilter: TicketState,
  membershipFilter: BoolFilter,
  vehicleTypeFilter: string,
) => {
  return tickets.filter(
    ut =>
      (ticketFilter === TicketState.ACTIVE
        ? !!!ut.paymentTime && !!ut.entryTime
        : ticketFilter === TicketState.PAID
          ? !!ut.paymentTime
          : !!!ut.entryTime) &&
      (membershipFilter === BoolFilter.ALL ||
        (membershipFilter === BoolFilter.YES
          ? ut.onMembership
          : !ut.onMembership)) &&
      (vehicleTypeFilter === "ALL" || vehicleTypeFilter === ut.vehicleTypeName),
  )
}

export const sortTickets = (
  tickets: Ticket[],
  sortDir: "ASC" | "DESC",
  sortBy: "ENTRY" | "PLATE" | "VEHICLE",
) => {
  return tickets.sort((a, b) => {
    let val: number = 0
    if (sortBy === "ENTRY") {
      if (a.entryTime === null && b.entryTime === null) {
        return 0
      } else if (a.entryTime === null) {
        return -1
      } else if (b.entryTime === null) {
        return 1
      }
      val = new Date(a.entryTime).getTime() - new Date(b.entryTime).getTime()
    } else if (sortBy === "PLATE") {
      val = a.plateNumber > b.plateNumber ? -1 : 1
    } else {
    }

    return sortDir === "ASC" ? val : -val
  })
}

export const filterAndSortUserTickets = (
  tickets: Ticket[],
  ticketFilter: TicketState,
  sortDir: "ASC" | "DESC",
  sortBy: "ENTRY" | "PLATE" | "VEHICLE",
  membershipFilter: BoolFilter,
  vehicleTypeFilter: string,
): Ticket[] => {
  return sortTickets(
    filterTickets(tickets, ticketFilter, membershipFilter, vehicleTypeFilter),
    sortDir,
    sortBy,
  )
}

export const filterUsers = (users: IUser[], membershipFilter: BoolFilter) => {
  return users.filter(
    u =>
      membershipFilter === BoolFilter.ALL ||
      (membershipFilter === BoolFilter.YES ? u.membership : !u.membership),
  )
}

export const sortUsers = (
  users: IUser[],
  sortDir: "ASC" | "DESC",
  sortBy: "ID" | "USERNAME" | "EMAIL",
) => {
  return users.sort((a, b) => {
    let val: number = 0
    if (sortBy === "USERNAME") {
      val = a.username > b.username ? -1 : 1
    } else if (sortBy === "EMAIL") {
      val = a.email > b.email ? -1 : 1
    } else {
      val = a.id > b.id ? -1 : 1
    }

    return sortDir === "ASC" ? val : -val
  })
}

export const copyElementToClipboard = async (el: HTMLElement) => {
  try {
    const canvas = await html2canvas(el)

    canvas.toBlob(blob => {
      if (blob) {
        navigator.clipboard.write([
          new ClipboardItem({
            "image/png": blob,
          }),
        ])
      }
    })
    return true
  } catch (error) {
    return false
  }
}

export const downloadElementAsImage = async (
  el: HTMLElement,
  fileName?: string,
) => {
  const canvas = await html2canvas(el)

  canvas.toBlob(blob => {
    if (blob) {
      const url = URL.createObjectURL(blob)

      const link = document.createElement("a")
      link.href = url
      link.download = `${fileName ? `${fileName}` : "file"}.png`
      document.body.appendChild(link)

      link.click()

      URL.revokeObjectURL(url)
      document.body.removeChild(link)
    }
  })
}

export function filterTicketsByDateRange(
  tickets: Ticket[],
  startDate: string,
  endDate: string,
): Ticket[] {
  const start = parseISO(startDate)
  const end = parseISO(endDate)
  return tickets.filter(ticket => {
    const entryTime = parseISO(ticket.entryTime ? ticket.entryTime : "")
    return isWithinInterval(entryTime, { start, end })
  })
}

export function exportToExcel(data: Ticket[], fileName: string): void {
  // Create a new workbook
  const workbook = XLSX.utils.book_new()
  // Convert the data to a worksheet
  const worksheet = XLSX.utils.json_to_sheet(
    data.map(ticket => ({
      TicketID: ticket.id,
      LicensePlate: ticket.plateNumber,
      EntryTime: ticket.entryTime
        ? format(new Date(ticket.entryTime), "yyyy-MM-dd HH:mm:ss")
        : "None",
      ExitTime: ticket.exitTime
        ? format(new Date(ticket.exitTime), "yyyy-MM-dd HH:mm:ss")
        : "None",
      VehicleType: ticket.vehicleTypeName,
      Membership: ticket.doesUserHaveActiveMembership ? "Yes" : "No",
      TotalFee: ticket.totalFee ? `Rp ${ticket.totalFee}` : "None",
    })),
  )
  // Add the worksheet to the workbook
  XLSX.utils.book_append_sheet(workbook, worksheet, "Tickets")
  // Generate an XLSX file
  XLSX.writeFile(workbook, `${fileName}.xlsx`)
}

export const sortVehicleTypes = (
  vehicleTypes: VehicleType[],
  sortDir: "ASC" | "DESC",
  sortBy: "NAME" | "ID" | "FEE",
) => {
  return vehicleTypes.sort((a, b) => {
    let val: number = 0
    if (sortBy === "NAME") {
      val = a.name > b.name ? -1 : 1
    } else if (sortBy === "FEE") {
      val = a.feePerHour > b.feePerHour ? -1 : 1
    } else {
      val = a.id > b.id ? -1 : 1
    }

    return sortDir === "ASC" ? val : -val
  })
}

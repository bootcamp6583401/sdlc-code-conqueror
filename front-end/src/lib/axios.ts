import axios from "axios"

const devMode = import.meta.env.VITE_DEV_MODE as string

const baseURL =
  devMode === "local"
    ? "http://localhost:8080"
    : (import.meta.env.VITE_NGROK_SERVER as string)

export default axios.create({
  baseURL: baseURL,
  headers: {
    "ngrok-skip-browser-warning": "69420",
  },
})

import { RouterProvider, createBrowserRouter } from "react-router-dom"
import "./App.css"
import TicketDetails from "./pages/tickets/TicketDetails"
import AdminLayout from "./components/layouts/AdminLayout"
import AdminDashboard from "./pages/admin/Dashboard"
import UserDashboard from "./pages/user/Dashboard"
import { Realtime } from "ably"
import { AblyProvider } from "ably/react"
import Scan from "./pages/admin/Scan"
import Register from "./pages/auth/Register"
import AuthLayout from "./components/layouts/AuthLayout"
import Login from "./pages/auth/Login"
import Home from "./pages/Home"
import UserLayout from "./components/layouts/UserLayout"
import PrivateRoute from "./components/PrivateRoute"
import { useEffect } from "react"
import { useAppDispatch, useAppSelector } from "./app/hooks"
import {
  clear,
  init,
  initComplete,
  selectError,
} from "./features/auth/authSlice"
import { Toaster } from "./components/ui/toaster"
import { useToast } from "@/components/ui/use-toast"
import Memerships from "./pages/admin/Users"
import Support from "./pages/user/Support"
import AdminSupport from "./pages/admin/Support"
import Profile from "./pages/user/Profile"
import Tickets from "./pages/admin/Tickets"
import Membership from "./pages/user/Membership"
import VehicleTypes from "./pages/admin/VehicleTypes"

const client = new Realtime.Promise({
  key: import.meta.env.VITE_ABLY_API_KEY,
  clientId: "nex-park-scanner",
})

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/auth",
    element: <AuthLayout />,
    children: [
      {
        path: "register",
        element: <Register />,
      },
      {
        path: "login",
        element: <Login />,
      },
    ],
  },

  {
    path: "/admin",
    element: (
      <PrivateRoute>
        <AdminLayout />
      </PrivateRoute>
    ),
    children: [
      {
        path: "dashboard",
        element: <AdminDashboard />,
      },
      {
        path: "users",
        element: <Memerships />,
      },
      {
        path: "tickets/:id",
        element: <TicketDetails adminView />,
      },
      {
        path: "tickets",
        element: <Tickets />,
      },
      {
        path: "scan",
        element: <Scan />,
      },
      {
        path: "support/:id",
        element: <AdminSupport />,
      },
      {
        path: "vehicle-types",
        element: <VehicleTypes />,
      },
    ],
  },
  {
    path: "/user",
    element: (
      <PrivateRoute>
        <UserLayout />
      </PrivateRoute>
    ),
    children: [
      {
        path: "dashboard",
        element: <UserDashboard />,
      },
      {
        path: "support",
        element: <Support />,
      },
      {
        path: "profile",
        element: <Profile />,
      },
      {
        path: "membership",
        element: <Membership />,
      },
      {
        path: "tickets/:id",
        element: <TicketDetails />,
      },
    ],
  },
])

const App = () => {
  const { toast } = useToast()
  const dispatch = useAppDispatch()
  const authError = useAppSelector(selectError)
  useEffect(() => {
    dispatch(initComplete(null))
  }, [dispatch])
  useEffect(() => {
    if (authError) {
      toast({
        title: authError,
      })
      // dispatch(clear())
    }
  }, [authError])
  return (
    <div className="max-w-screen min-h-screen">
      <AblyProvider client={client}>
        <RouterProvider router={router} />
        <Toaster />
      </AblyProvider>
    </div>
  )
}

export default App

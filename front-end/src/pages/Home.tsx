import BaseHeader from "@/components/layouts/BaseHeader"
import { useEffect, useRef, useState } from "react"
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import hero from "@/assets/images/hero-landing.png"
import ImgFlexible from "@/assets/icons/parking1.svg"
import ImgEasy from "@/assets/icons/parking2.svg"
import ImgSafe from "@/assets/icons/parking3.svg"
import IconParking from "@/assets/icons/service-parking.png"
import IconSubs from "@/assets/icons/servis-subs.png"
import IconValet from "@/assets/icons/servis-valet.png"
import CardBenefit from "@/components/ui/cardBenefit"
import CardService from "@/components/ui/cardService"
import { Button, buttonVariants } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import BaseFooter from "@/components/layouts/BaseFooter"
import { Link } from "react-router-dom"
import { cn, copyElementToClipboard, downloadElementAsImage } from "@/lib/utils"
import { Ticket, VehicleType } from "@/types"
import { Loader2 } from "lucide-react"
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"
import TicketPreview from "@/components/TicketPreview"
import { toast } from "@/components/ui/use-toast"
import { useService } from "@/contexts/ServiceContext"

export default function Home() {
  const [anonymousTicket, setAnonymousTicket] = useState<Ticket | null>(null)
  const [generateTicketLoading, setGenerateTicketLoading] = useState(false)
  const [vehicleTypeId, setVehicleTypeId] = useState("1")
  const [vehicleTypes, setVehicleTypes] = useState<VehicleType[]>([])
  const [plateNumber, setPlateNumber] = useState("")

  const { vehicleTypesService, ticketsService } = useService()

  const ref = useRef<HTMLDivElement>(null)

  useEffect(() => {
    ;(async () => {
      const vts = await vehicleTypesService.getVehicleTypes()
      setVehicleTypes(vts)
      setVehicleTypeId(vts[0].id.toString())
    })()
  }, [])

  return (
    <div>
      <BaseHeader />
      <div className="mx-[20px]">
        <section className="flex flex-col lg:flex-row items-center justify-between my-[80px] lg:gap-4 mx-auto max-w-[1183px]">
          <div className="space-y-[15px] lg:w-[800px]">
            <h3 className="text-white text-[20px] lg:text-[24px] font-semibold">
              Welcome to NexPark
            </h3>
            <h1 className="text-[30px] lg:text-[48px] font-bold leading-tight">
              Discover Modern and Efficient Parking Solutions
            </h1>
            <p className="text-white text-xl">
              NexParking provides cutting-edge parking solutions to meet your
              parking needs. With the latest technology, we ensure a seamless
              and convenient parking experience for every user.
            </p>
            <div className="flex gap-2 w-full">
              <Link
                to="/auth/register"
                className={cn(buttonVariants(), "font-bold grow")}
              >
                Get Started
              </Link>
              <Dialog>
                <DialogTrigger asChild>
                  <Button className="font-bold grow" variant="outline">
                    Park as Guest
                  </Button>
                </DialogTrigger>
                <DialogContent>
                  <DialogHeader>
                    <DialogTitle>Guest Ticket</DialogTitle>
                    <DialogDescription>
                      Use Nex Park without logging in.
                    </DialogDescription>
                  </DialogHeader>
                  <div>
                    <div className="flex justify-center w-full min-h-[300px] items-center">
                      {generateTicketLoading ? (
                        <div className="flex flex-col items-center">
                          <Loader2 size={64} className="animate-spin" />
                          <div>Generating Ticket...</div>
                        </div>
                      ) : anonymousTicket ? (
                        <div className="space-y-4">
                          <div ref={ref}>
                            <TicketPreview ticket={anonymousTicket} />
                          </div>
                          <div className="flex gap-4 justify-end">
                            <Button
                              variant="outline"
                              onClick={async () => {
                                if (ref.current) {
                                  try {
                                    await copyElementToClipboard(ref.current)
                                    toast({
                                      title: "Copied ticket to clipboard.",
                                      description:
                                        "Make sure you paste this somewhere.",
                                    })
                                  } catch (error) {
                                    toast({
                                      title: "Error copying to clipboard.",
                                    })
                                  }
                                }
                              }}
                            >
                              Copy Ticket
                            </Button>
                            <Button
                              onClick={() => {
                                if (ref.current) {
                                  downloadElementAsImage(
                                    ref.current,
                                    `nexpark-ticket-${plateNumber}`,
                                  )
                                }
                              }}
                            >
                              Download Ticket
                            </Button>
                          </div>
                        </div>
                      ) : (
                        <div className="space-y-4">
                          <Input
                            type="text"
                            placeholder="B 2343"
                            required
                            value={plateNumber}
                            onChange={e => setPlateNumber(e.target.value)}
                          />
                          <Select
                            onValueChange={val => setVehicleTypeId(val)}
                            value={vehicleTypeId}
                          >
                            <SelectTrigger className="w-full">
                              <SelectValue />
                            </SelectTrigger>
                            <SelectContent>
                              {vehicleTypes.map(vt => (
                                <SelectItem
                                  key={vt.id}
                                  value={vt.id.toString()}
                                >
                                  {vt.name}
                                </SelectItem>
                              ))}
                            </SelectContent>
                          </Select>
                          <Button
                            className="font-bold w-full"
                            onClick={async () => {
                              setGenerateTicketLoading(true)
                              const ticket =
                                await ticketsService.generateGuestTicket(
                                  plateNumber,
                                  parseInt(vehicleTypeId),
                                )
                              const createdTicket =
                                await ticketsService.guestGetTicketById(
                                  ticket.id,
                                )
                              setAnonymousTicket(createdTicket)
                              setGenerateTicketLoading(false)
                            }}
                          >
                            Generate Ticket
                          </Button>
                        </div>
                      )}
                    </div>
                  </div>
                </DialogContent>
              </Dialog>
            </div>
          </div>
          <div className="order-first mb-[30px] lg:mb-0 lg:order-last">
            <img src={hero} alt="hero img" />
          </div>
        </section>

        <section className="text-center py-16">
          <h1 className="text-[30px] lg:text-[48px] font-bold">
            Why Choose NexParking?
          </h1>

          <div className="grid grid-cols-1 lg:grid-cols-3 gap-8 w-full mx-auto max-w-[1183px]">
            <CardBenefit
              imgUrl={ImgFlexible}
              title={"Flexible"}
              description={
                "NexParking offers a range of parking options, from daily parking to monthly subscriptions, tailored to your schedule and parking needs."
              }
            />

            <CardBenefit
              imgUrl={ImgEasy}
              title={"Easy to Use"}
              description={
                "With the NexParking app, you can easily find and reserve parking spots in advance, and make quick and secure payments right from your phone."
              }
            />

            <CardBenefit
              imgUrl={ImgSafe}
              title={"Safe and Secure"}
              description={
                "We prioritize the security of your vehicles. With 24/7 surveillance systems and trained security personnel, you can leave your vehicle with peace of mind in NexParking's parking areas."
              }
            />
          </div>
        </section>

        {/* <section>
          <div className="bg-primary p-[20px]">
            <Carousel className="">
              <CarouselContent className="">
                {imageUrls.map((url, index) => (
                  <CarouselItem className="lg:basis-1/4 " key={index}>
                    <img
                      className="object-cover h-96 w-full"
                      src={url}
                      alt={`Image ${index}`}
                    />
                  </CarouselItem>
                ))}
              </CarouselContent>
              <CarouselPrevious />
              <CarouselNext />
            </Carousel>
          </div>
        </section> */}

        <section className="flex flex-col justify-center items-center space-y-8 py-16">
          <h1 className="text-[30px] lg:text-[48px] font-bold ">
            Our Services
          </h1>

          <div className="grid grid-cols-1 lg:grid-cols-3 gap-8 w-full mx-auto max-w-[1183px]">
            <CardService
              title={"Daily Parking"}
              iconUrl={IconParking}
              description={
                "Enjoy the convenience of daily parking at competitive rates at NexParking."
              }
            />

            <CardService
              title={"Monthly Subscriptions"}
              iconUrl={IconSubs}
              description={
                "Get exclusive access to our parking areas with flexible and convenient monthly subscriptions."
              }
            />

            <CardService
              title={"Valet Services"}
              iconUrl={IconValet}
              description={
                "Take advantage of our valet services for a stress-free and premium parking experience."
              }
            />
          </div>
        </section>

        <section className="bg-gray flex flex-col lg:flex-row justify-between items-center p-[50px] lg:mx-[130px] my-10 space-y-10 lg:space-x-0">
          <div className="">
            <h1 className="text-[24px] lg:text-[40px] font-bold text-black">
              Join NexParking Today!
            </h1>
            <p className="text-black">
              Enhance your parking experience with NexParking. Join us today and
              experience the comfort of modern parking on every journey!
            </p>
          </div>

          <Link to="/auth/register">
            <Button className="px-[100px] font-bold">Register Now</Button>
          </Link>
        </section>
      </div>
      <BaseFooter />
    </div>
  )
}

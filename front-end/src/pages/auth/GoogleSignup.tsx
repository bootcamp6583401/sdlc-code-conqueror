import { Button } from "@/components/ui/button"
import { useGoogleLogin } from "@react-oauth/google"
import React, { useEffect, useState } from "react"
import IconGoogle from "@/assets/icons/flat-color-icons_google.png"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "@/app/store"
import { loginGoogle } from "@/features/auth/authSlice"
import { useToast } from "@/components/ui/use-toast"

interface UserToken {
  access_token: string
}

export default function GoogleSignup() {
  const dispatch = useDispatch<any>()
  const { toast } = useToast()

  const LoginGoogle = useGoogleLogin({
    onSuccess: codeResponse => setUserGoogle(codeResponse),
    onError: error =>
      toast({
        title: "SSO Failed",
      }),
  })

  const [userGoogle, setUserGoogle] = useState<UserToken | null>(null)

  useEffect(() => {
    if (userGoogle) {
      dispatch(loginGoogle({ token: userGoogle.access_token }))
    }
  }, [userGoogle])

  // useEffect(() => {
  // 	if (data.token != null) {
  // 		localStorage.setItem("user", JSON.stringify(data.user));
  // 		localStorage.setItem("token", JSON.stringify(data.token));
  // 		navigate("/");
  // 	}
  // }, [data]);

  return (
    <Button
      onClick={() => LoginGoogle()}
      className="bg-light-gray w-full gap-2"
    >
      <img src={IconGoogle} alt="icon-google" />
      Continue with Google
    </Button>
  )
}

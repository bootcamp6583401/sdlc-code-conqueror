"use client"

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import IconGoogle from "@/assets/icons/flat-color-icons_google.png"

import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { z } from "zod"
import { useAppDispatch, useAppSelector } from "@/app/hooks"
import {
  AuthStatus,
  clear,
  register,
  selectAuth,
  selectAuthStatus,
  selectError,
  selectLoading,
} from "@/features/auth/authSlice"
import { useEffect, useState } from "react"
import { Link, useNavigate } from "react-router-dom"
import { GoogleOAuthProvider } from "@react-oauth/google"
import GoogleSignup from "./GoogleSignup"

const formSchema = z.object({
  username: z.string().min(2, {
    message: "Username must be at least 2 characters.",
  }),
  email: z.string().min(2, {
    message: "email must be at least 2 characters.",
  }),
  password: z.string().min(2, {
    message: "password must be at least 2 characters.",
  }),
})

export default function Register() {
  const dispatch = useAppDispatch()
  const authError = useAppSelector(selectError)
  const authLoading = useAppSelector(selectLoading)
  const user = useAppSelector(selectAuth)
  const status = useAppSelector(selectAuthStatus)
  const navigate = useNavigate()

  const [hasAttempted, setHasAttempted] = useState(false)
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: "",
      password: "",
      email: "",
    },
  })

  async function onSubmit(values: z.infer<typeof formSchema>) {
    await dispatch(
      register({
        username: values.username,
        password: values.password,
        email: values.email,
      }),
    )
    setHasAttempted(true)
  }

  useEffect(() => {
    if (status === AuthStatus.REGISTER_SUCCESS) {
      navigate("/auth/login")
    }
  }, [status])

  useEffect(() => {
    if (user) {
      user.roles.includes("ROLE_ADMIN")
        ? navigate("/admin/dashboard")
        : navigate("/user/dashboard")
    }
  }, [user])

  return (
    <div className="bg-black text-white p-8 flex flex-col gap-2 justify-center w-full">
      <h1 className="text-[#E2F853] text-[30px] lg:text-[40px] font-semibold text-center">
        Create Account
      </h1>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="space-y-8 max-w-[500px] mx-auto w-full"
        >
          <div className="space-y-4">
            <FormField
              control={form.control}
              name="username"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Username</FormLabel>
                  <FormControl>
                    <Input placeholder="Input Username" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="email"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Email</FormLabel>
                  <FormControl>
                    <Input placeholder="Input Email" type="email" {...field} />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Password</FormLabel>
                  <FormControl>
                    <Input
                      placeholder="Input Password"
                      type="password"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
          </div>
          <Button
            type="submit"
            className="bg-[#E2F853] text-black block w-full font-bold"
          >
            Regiter Now
          </Button>

          <div className="text-center">
            Already have an account?{" "}
            <Link to="/auth/login" className="text-primary">
              Login
            </Link>
          </div>

          <div>
            <GoogleOAuthProvider
              clientId={
                "985053297790-salht4sph21lug83bderivsmq7f78jhn.apps.googleusercontent.com"
              }
            >
              <GoogleSignup />
            </GoogleOAuthProvider>
          </div>
        </form>
      </Form>
    </div>
  )
}

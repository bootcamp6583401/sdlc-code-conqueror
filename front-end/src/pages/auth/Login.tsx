"use client"

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import IconGoogle from "@/assets/icons/flat-color-icons_google.png"
import { GoogleOAuthProvider, useGoogleLogin } from "@react-oauth/google"

import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { z } from "zod"
import { useAppDispatch, useAppSelector } from "@/app/hooks"
import { login, selectAuth } from "@/features/auth/authSlice"
import { Link, useNavigate } from "react-router-dom"
import { useEffect, useState } from "react"
import GoogleSignup from "./GoogleSignup"

const formSchema = z.object({
  username: z.string().min(2, {
    message: "username must be at least 2 characters.",
  }),
  password: z.string().min(2, {
    message: "password must be at least 2 characters.",
  }),
})

export default function Login() {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const user = useAppSelector(selectAuth)

  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      username: "",
      password: "",
    },
  })

  async function onSubmit(values: z.infer<typeof formSchema>) {
    try {
      await dispatch(
        login({ username: values.username, password: values.password }),
      )
    } catch (error) {}
  }

  useEffect(() => {
    if (user) {
      user.roles.includes("ROLE_ADMIN")
        ? navigate("/admin/dashboard")
        : navigate("/user/dashboard")
    }
  }, [user])

  return (
    <div className="bg-black text-white p-8 flex flex-col gap-2 justify-center w-full">
      <h1 className="text-[#E2F853] text-[30px] lg:text-[40px] font-semibold text-center">
        Welcome Back
      </h1>
      <Form {...form}>
        <form
          onSubmit={form.handleSubmit(onSubmit)}
          className="space-y-8 max-w-[500px] mx-auto w-full"
        >
          <div className="space-y-4">
            <FormField
              control={form.control}
              name="username"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Username</FormLabel>
                  <FormControl>
                    <Input placeholder="Username" type="text" {...field} />
                  </FormControl>
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormItem>
                  <FormLabel>Password</FormLabel>
                  <FormControl>
                    <Input placeholder="password" type="password" {...field} />
                  </FormControl>
                </FormItem>
              )}
            />
          </div>
          <Button
            type="submit"
            className="bg-[#E2F853] text-black block w-full font-bold"
          >
            Login
          </Button>

          <div className="text-center">
            Don't have an account?{" "}
            <Link to="/auth/register" className="text-primary">
              Register
            </Link>
          </div>

          <div>
            <GoogleOAuthProvider
              clientId={
                "985053297790-salht4sph21lug83bderivsmq7f78jhn.apps.googleusercontent.com"
              }
            >
              <GoogleSignup />
            </GoogleOAuthProvider>
          </div>
        </form>
      </Form>
    </div>
  )
}

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import { Loader2, SortAsc, SortDesc } from "lucide-react"
import { useAppSelector } from "@/app/hooks"
import { selectAuth, selectUserData } from "@/features/auth/authSlice"
import { Button } from "@/components/ui/button"
import { useEffect, useState } from "react"
import { BoolFilter, Page, Ticket, TicketState, VehicleType } from "@/types"
import { Input } from "@/components/ui/input"
import TicketPreview from "@/components/TicketPreview"
import { format } from "date-fns"
import { useNavigate } from "react-router-dom"
import {
  filterAndSortUserTickets,
  filterTickets,
  formRupiah,
  getProjectedTicketTotal,
  mapTicketState,
} from "@/lib/utils"
import logo from "@/assets/icons/nex-park-icon.svg"
import { Label } from "@/components/ui/label"
import { useService } from "@/contexts/ServiceContext"
export default function UserDashboard() {
  const [generateTicketLoading, setGenerateTicketLoading] = useState(false)
  const [selectedTicket, setSelectedTicket] = useState<Ticket | null>(null)
  const [plateNumber, setPlateNumber] = useState("")
  const [vehicleTypeId, setVehicleTypeId] = useState("1")
  const user = useAppSelector(selectAuth)
  const [userTickets, setUserTickets] = useState<Ticket[]>([])
  const [userTicketPageData, setUserTicketPageData] =
    useState<Page<Ticket> | null>(null)
  const [vehicleTypes, setVehicleTypes] = useState<VehicleType[]>([])
  const [generateTicketModalOpen, setGenerateTicketModalOpen] = useState(false)
  const [previewModalOpen, setPreviewModalOpen] = useState(false)
  const navigate = useNavigate()
  const [ticketFilter, setTicketFilter] = useState<TicketState>(
    TicketState.PENDING,
  )
  const [membershipFilter, setMembershipFilter] = useState<BoolFilter>(
    BoolFilter.ALL,
  )
  const [vehicleTypeFilter, setVehicleTypeFilter] = useState<string>("ALL")
  const [sortDir, setSortDir] = useState<"ASC" | "DESC">("ASC")
  const [sortBy, setSortBy] = useState<"PLATE" | "ENTRY" | "VEHICLE">("ENTRY")

  const userData = useAppSelector(selectUserData)

  const { ticketsService, vehicleTypesService } = useService()

  useEffect(() => {
    ;(async () => {
      if (user) {
        const vts = await vehicleTypesService.getVehicleTypes()
        setVehicleTypes(vts)
        const pageData = await ticketsService.getUserTickets(user.id)
        setUserTicketPageData(pageData)
        setVehicleTypeId(vts[0].id.toString())
      }
    })()
  }, [])

  useEffect(() => {
    if (userTicketPageData) {
      setUserTickets(userTicketPageData.content)
    }
  }, [userTicketPageData])
  return (
    <div className="max-w-full overflow-hidden pb-24">
      <div className="">
        <h2 className="text-3xl font-bold tracking-tight">
          Welcome back, {user?.username}
        </h2>
      </div>
      <div className="mt-4 space-y-8 overflow-auto">
        <div className="border-border rounded-md border p-4 min-h-[500px] flex flex-col">
          <h3 className="text-2xl font-semibold mb-4">Your Tickets</h3>

          <div className="flex justify-between mb-4 overflow-auto items-end">
            <div className="flex gap-2 items-end">
              <div className="bg-primary flex gap-1 p-1 rounded-md overflow-hidden">
                <Button
                  onClick={() => setTicketFilter(TicketState.PENDING)}
                  variant={
                    ticketFilter === TicketState.PENDING
                      ? "secondary"
                      : "default"
                  }
                >
                  Pending
                </Button>
                <Button
                  onClick={() => setTicketFilter(TicketState.ACTIVE)}
                  variant={
                    ticketFilter === TicketState.ACTIVE
                      ? "secondary"
                      : "default"
                  }
                >
                  Active
                </Button>
                <Button
                  onClick={() => setTicketFilter(TicketState.PAID)}
                  variant={
                    ticketFilter === TicketState.PAID ? "secondary" : "default"
                  }
                >
                  Paid
                </Button>
              </div>
              <div className="flex gap-4  ">
                <div>
                  <Label>Sort</Label>
                  <div className="flex gap-2">
                    <Button
                      variant="outline"
                      size="icon"
                      onClick={() =>
                        setSortDir(sortDir === "ASC" ? "DESC" : "ASC")
                      }
                    >
                      {sortDir === "ASC" ? <SortAsc /> : <SortDesc />}
                    </Button>
                    <Select
                      value={sortBy}
                      onValueChange={val => setSortBy(val as "ENTRY" | "PLATE")}
                    >
                      <SelectTrigger className="w-[100px] border-border">
                        <SelectValue placeholder="Sort By" />
                      </SelectTrigger>
                      <SelectContent>
                        <SelectItem value="ENTRY">Entry</SelectItem>
                        <SelectItem value="PLATE">Plate</SelectItem>
                      </SelectContent>
                    </Select>
                  </div>
                </div>

                <div>
                  <Label>Membership</Label>
                  <Select
                    value={membershipFilter}
                    onValueChange={val => {
                      const value = val as BoolFilter
                      setMembershipFilter(value)
                    }}
                  >
                    <SelectTrigger className="w-[100px] border-border">
                      <SelectValue placeholder="Sort By" />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectItem value={BoolFilter.ALL}>All</SelectItem>
                      <SelectItem value={BoolFilter.YES}>Yes</SelectItem>
                      <SelectItem value={BoolFilter.NO}>No</SelectItem>
                    </SelectContent>
                  </Select>
                </div>
                <div>
                  <Label>Vehicle Type</Label>
                  <Select
                    value={vehicleTypeFilter}
                    onValueChange={val => {
                      setVehicleTypeFilter(val)
                    }}
                  >
                    <SelectTrigger className="w-[100px] border-border">
                      <SelectValue placeholder="Sort By" />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectItem value="ALL">All</SelectItem>
                      {vehicleTypes.map(vt => (
                        <SelectItem key={vt.id} value={vt.name}>
                          {vt.name}
                        </SelectItem>
                      ))}
                    </SelectContent>
                  </Select>
                </div>
              </div>
            </div>
            <Dialog
              open={generateTicketModalOpen}
              onOpenChange={setGenerateTicketModalOpen}
            >
              <DialogTrigger asChild>
                <Button className="font-bold">Generate New</Button>
              </DialogTrigger>
              <DialogContent>
                <DialogHeader>
                  <DialogTitle>Generate New Ticket</DialogTitle>
                  <DialogDescription>
                    Looking to park? Generate new ticket.
                  </DialogDescription>
                </DialogHeader>
                <div>
                  <div className="flex justify-center w-full min-h-[300px] items-center">
                    {generateTicketLoading ? (
                      <div className="flex flex-col items-center">
                        <Loader2 size={64} className="animate-spin" />
                        <div>Generating Ticket...</div>
                      </div>
                    ) : (
                      <div className="space-y-4">
                        <Input
                          type="text"
                          placeholder="B 2343"
                          required
                          value={plateNumber}
                          onChange={e => setPlateNumber(e.target.value)}
                        />
                        <Select
                          onValueChange={val => setVehicleTypeId(val)}
                          value={vehicleTypeId}
                        >
                          <SelectTrigger className="w-full">
                            <SelectValue />
                          </SelectTrigger>
                          <SelectContent>
                            {vehicleTypes.map(vt => (
                              <SelectItem key={vt.id} value={vt.id.toString()}>
                                {vt.name}
                              </SelectItem>
                            ))}
                          </SelectContent>
                        </Select>
                        <Button
                          className="font-bold w-full"
                          onClick={async () => {
                            setGenerateTicketLoading(true)
                            const ticket = await ticketsService.generateTicket(
                              plateNumber,
                              parseInt(vehicleTypeId),
                              user!.id,
                            )
                            const createdTicket =
                              await ticketsService.getTicketById(ticket.id)
                            setSelectedTicket(createdTicket)
                            setPreviewModalOpen(true)
                            setUserTickets(prev => [...prev, createdTicket])
                            setGenerateTicketLoading(false)
                            setGenerateTicketModalOpen(false)
                          }}
                        >
                          Generate Ticket
                        </Button>
                      </div>
                    )}
                  </div>
                </div>
              </DialogContent>
            </Dialog>
          </div>
          <Table className="max-w-full overflow-scroll">
            {/* <TableCaption>A list of your tickets</TableCaption> */}
            <TableHeader>
              <TableRow>
                <TableHead className="w-[100px]">Ticket No.</TableHead>
                <TableHead>License Plate</TableHead>
                <TableHead>Entry Time</TableHead>
                <TableHead>Vehile Type</TableHead>
                <TableHead>Membership?</TableHead>
                <TableHead className="text-right">
                  {mapTicketState(
                    ticketFilter,
                    "Enter First",
                    "Projected Total",
                    "Paid Total",
                  )}
                </TableHead>
                <TableHead className="text-right">Actions</TableHead>
              </TableRow>
            </TableHeader>
            <TableBody>
              {filterTickets(
                userTickets,
                ticketFilter,
                membershipFilter,
                vehicleTypeFilter,
              ).length === 0 && (
                <TableRow>
                  <TableCell colSpan={7} className="text-center">
                    No tickets found.
                  </TableCell>
                </TableRow>
              )}
              {filterAndSortUserTickets(
                userTickets,
                ticketFilter,
                sortDir,
                sortBy,
                membershipFilter,
                vehicleTypeFilter,
              ).map(ut => (
                <TableRow key={ut.id}>
                  <TableCell className="font-medium">
                    {ut.id.slice(0, 5)}...
                  </TableCell>
                  <TableCell>{ut.plateNumber}</TableCell>
                  <TableCell>
                    {ut.entryTime
                      ? format(new Date(ut.entryTime), "dd MM yyyy, hh:mm:ss")
                      : "None"}
                  </TableCell>
                  <TableCell>{ut.vehicleTypeName}</TableCell>
                  <TableCell>
                    {mapTicketState(
                      ticketFilter,
                      ut.doesUserHaveActiveMembership ? "Yes" : "No",
                      ut.doesUserHaveActiveMembership ? "Yes" : "No",
                      ut.onMembership ? "Yes" : "No",
                    )}
                  </TableCell>
                  <TableCell className="text-right">
                    {mapTicketState(
                      ticketFilter,
                      "Enter First",
                      formRupiah(getProjectedTicketTotal(ut)),
                      ut.totalFee !== null ? formRupiah(ut.totalFee) : "None",
                    )}
                  </TableCell>
                  <TableCell className="flex gap-2 justify-end">
                    <Button
                      variant="outline"
                      onClick={() => {
                        setSelectedTicket(ut)
                        setPreviewModalOpen(true)
                      }}
                    >
                      Preview
                    </Button>
                    <Button
                      variant="outline"
                      onClick={() => navigate(`/user/tickets/${ut.id}`)}
                    >
                      Details
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <div className="flex gap-2 mt-auto">
            {Array.from({ length: userTicketPageData?.totalPages || 0 }).map(
              (_, index) => (
                <Button
                  key={index + 1}
                  variant={
                    userTicketPageData?.pageable.pageNumber === index
                      ? "default"
                      : "outline"
                  }
                  onClick={async () => {
                    if (user) {
                      setUserTicketPageData(
                        await ticketsService.getUserTickets(user.id, index),
                      )
                    }
                  }}
                  size="sm"
                >
                  {index + 1}
                </Button>
              ),
            )}
          </div>
        </div>
        <div className="border-border rounded-md border p-4">
          <h3 className="text-2xl font-semibold mb-4">
            Your Membership Status
          </h3>
          <div>
            {userData?.membership ? (
              <div className="space-y-4 pb-8">
                <div className="py-8 flex items-center justify-center flex-col gap-8">
                  <img src={logo} className="w-48" />
                  <div className="text-2xl font-semibold">
                    You are currently a member.
                  </div>
                </div>
                <div className="text-xl flex items-center justify-between gap-2">
                  <div className="">Start Date</div>
                  <div className="grow border-b border-border border-dotted relative top-1.5"></div>
                  <div>
                    {format(new Date(userData.membership.start), "dd-MMM-yyy")}
                  </div>
                </div>
                <div className="text-xl flex items-center justify-between gap-2">
                  <div className="">Expiry Date</div>
                  <div className="grow border-b border-border border-dotted relative top-1.5"></div>
                  <div>
                    {format(new Date(userData.membership.end), "dd-MMM-yyy")}
                  </div>
                </div>
              </div>
            ) : (
              <div className="py-8 flex items-center justify-center flex-col gap-8">
                <img src={logo} className="w-48" />
                <div className="text-2xl font-semibold">
                  You currently aren't a member.
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      <Dialog open={previewModalOpen} onOpenChange={setPreviewModalOpen}>
        {selectedTicket && (
          <DialogContent>
            <DialogHeader>
              <DialogTitle>Your Ticket</DialogTitle>
              <DialogDescription>
                Preview of ticket {selectedTicket.id.slice(0, 4)}...
                {selectedTicket.id.slice(-4)}
              </DialogDescription>
            </DialogHeader>
            <div>
              <div className="flex justify-center w-full min-h-[300px] items-center">
                <TicketPreview ticket={selectedTicket} />
              </div>
            </div>
          </DialogContent>
        )}
      </Dialog>
    </div>
  )
}

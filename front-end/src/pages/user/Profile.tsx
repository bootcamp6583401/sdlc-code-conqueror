import { useAppDispatch, useAppSelector } from "@/app/hooks"
import { Button } from "@/components/ui/button"
import { Separator } from "@/components/ui/separator"
import { useToast } from "@/components/ui/use-toast"
import { selectUserData, updateUserData } from "@/features/auth/authSlice"
import { Pen } from "lucide-react"
import { useEffect, useState } from "react"
import { Input } from "@/components/ui/input"
import { useService } from "@/contexts/ServiceContext"

export default function Profile() {
  const userData = useAppSelector(selectUserData)
  const dispatch = useAppDispatch()

  const [selectedImageFile, setSelectedImageFile] = useState<null | File>(null)

  const [changesDetected, setChangesDetected] = useState(false)
  const [hasSetOldValues, setHasSetOldValues] = useState(false)

  const [phone, setPhone] = useState("")

  const { toast } = useToast()

  const { usersService } = useService()

  useEffect(() => {
    if (selectedImageFile) {
    }
  }, [selectedImageFile])

  useEffect(() => {
    if (userData) {
      setPhone(userData.phone || "")
      setHasSetOldValues(true)
    }
  }, [userData])

  useEffect(() => {
    if (userData && hasSetOldValues) {
      if (phone !== userData.phone) {
        setChangesDetected(true)
      } else {
        setChangesDetected(false)
      }
    }
  }, [userData, hasSetOldValues, phone])

  return (
    <div className="max-w-full overflow-hidden pb-24">
      <div className="flex items-center justify-between mb-8">
        <h2 className="text-3xl font-bold tracking-tight">Profile</h2>
      </div>
      {userData ? (
        <div className="mt-4 space-y-4 overflow-auto">
          <div className="space-y-4 lg:flex gap-4 w-full">
            <div>
              <label className="block cursor-pointer" htmlFor="profile-img">
                <img
                  src={
                    selectedImageFile
                      ? URL.createObjectURL(selectedImageFile)
                      : userData.profileImageUrl
                        ? userData.profileImageUrl.startsWith("https")
                          ? userData.profileImageUrl
                          : `http://localhost:8080/images/${userData.profileImageUrl}`
                        : "/not-found.png"
                  }
                  className="w-24 h-24 lg:w-64 lg:h-64 object-cover rounded-full border-border border-2"
                />
                <input
                  className="hidden"
                  id="profile-img"
                  type="file"
                  onChange={e => {
                    if (e.target.files) {
                      setSelectedImageFile(e.target.files[0])
                    }
                  }}
                />
              </label>
              {selectedImageFile && (
                <div className="mt-4 flex justify-center">
                  <Button
                    size="sm"
                    onClick={async () => {
                      if (userData) {
                        const newUserData =
                          await usersService.uploadProfileImage(
                            userData.id,
                            selectedImageFile,
                          )

                        dispatch(updateUserData(newUserData))
                        setSelectedImageFile(null)
                        toast({ title: "Successfully uploaded new image." })
                      }
                    }}
                  >
                    Upload New Image
                  </Button>
                </div>
              )}
            </div>
            <div className="grow">
              <div className="border-border border rounded-md p-4 grow min-h-[500px]">
                <div className="flex justify-between items-center mb-2">
                  <h3 className="text-xl font-semibold ">Profile Data</h3>
                  <Button size="icon" variant="outline">
                    <Pen size={18} />
                  </Button>
                </div>
                <Separator className="mb-2" />
                <div className="space-y-4">
                  <div className="flex justify-between items-center">
                    <div className="font-bold text-lg">Username</div>
                    <div>{userData.username}</div>
                  </div>
                  <div className="flex justify-between items-center">
                    <div className="font-bold text-lg">Email</div>
                    <div>{userData.email}</div>
                  </div>
                  <div className="flex justify-between items-center">
                    <div className="font-bold text-lg">Phone</div>
                    <div>
                      <Input
                        placeholder="Phone"
                        type="phone"
                        value={phone}
                        onChange={e => setPhone(e.target.value)}
                      />
                    </div>
                  </div>
                </div>
              </div>
              {changesDetected && (
                <div className="flex items-center justify-between mt-4">
                  <div className="font-bold">Changes Detected</div>
                  <div>
                    <Button
                      onClick={async () => {
                        if (userData) {
                          try {
                            await usersService.updateUserById(userData.id, {
                              phone,
                            })
                            setChangesDetected(false)
                            setHasSetOldValues(false)
                            dispatch(
                              updateUserData({
                                ...userData,
                                phone: phone,
                              }),
                            )
                          } catch (error) {}
                        }
                      }}
                    >
                      Save Changes
                    </Button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  )
}

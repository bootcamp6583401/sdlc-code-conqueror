import { Button } from "@/components/ui/button"
import { useEffect, useMemo, useState } from "react"
import logo from "@/assets/icons/nex-park-icon.svg"
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"
import { countDaysBetween, formRupiah } from "@/lib/utils"
import { Input } from "@/components/ui/input"
import { Separator } from "@/components/ui/separator"
import { VehicleType } from "@/types"
import { useLocation, useNavigate } from "react-router-dom"
import axios from "@/lib/axios"
import { useAppDispatch, useAppSelector } from "@/app/hooks"
import { selectUserData, updateUserData } from "@/features/auth/authSlice"
import { format } from "date-fns"
import { useService } from "@/contexts/ServiceContext"

declare global {
  interface Window {
    snap: {
      pay: (
        token: string,
        callbacks?: {
          onSuccess?: (obj: any) => void
        },
      ) => void
    }
  }
}

export default function Membership() {
  const [token, setToken] = useState("")
  const [membershipExpiryDate, setMembershipExpiryDate] = useState("")
  const [vehicleTypeId, setVehicleTypeId] = useState("1")
  const [vehicleTypes, setVehicleTypes] = useState<VehicleType[]>([])
  const [plateNumber, setPlateNumber] = useState("")
  const userData = useAppSelector(selectUserData)
  const dispatch = useAppDispatch()
  const [selectedVehicleType, setSelectedVehicleType] =
    useState<VehicleType | null>(null)
  const { search } = useLocation()
  const navigate = useNavigate()

  const { vehicleTypesService, membershipService } = useService()

  useEffect(() => {
    ;(async () => {
      const vts = await vehicleTypesService.getVehicleTypes()
      setVehicleTypes(vts)
      setVehicleTypeId(vts[0].id.toString())
    })()
  }, [])

  useEffect(() => {
    const found = vehicleTypes.find(vt => vt.id === parseInt(vehicleTypeId))

    if (found) {
      setSelectedVehicleType(found)
    }
  }, [vehicleTypeId, vehicleTypes])

  useEffect(() => {
    // You can also change below url value to any script url you wish to load,
    // for example this is snap.js for Sandbox Env (Note: remove `.sandbox` from url if you want to use production version)
    const midtransScriptUrl = "https://app.sandbox.midtrans.com/snap/snap.js"

    let scriptTag = document.createElement("script")
    scriptTag.src = midtransScriptUrl

    // Optional: set script attribute, for example snap.js have data-client-key attribute
    // (change the value according to your client-key)
    const myMidtransClientKey = "SB-Mid-client-aCSFcTgPXdtjMnOc"
    scriptTag.setAttribute("data-client-key", myMidtransClientKey)

    document.body.appendChild(scriptTag)

    return () => {
      document.body.removeChild(scriptTag)
    }
  }, [])

  const totalDays = useMemo(() => {
    return countDaysBetween(
      new Date(),
      membershipExpiryDate ? new Date(membershipExpiryDate) : new Date(),
    )
  }, [membershipExpiryDate])

  useEffect(() => {
    if (token) window.snap.pay(token, {})
  }, [token])

  const paymentStatus: {
    orderId: string
    statusCode: number
    transactionStatus: string
    plateNumber: string
    vehicleTypeId: number
    expiryDate: string
  } | null = useMemo(() => {
    const query = new URLSearchParams(search)
    const orderId = query.get("order_id")
    const statusCode = query.get("status_code")
    const transactionStatus = query.get("transaction_status")
    const plateNumber = query.get("plateNumber")
    const expiryDate = query.get("expiryDate")
    const vehicleTypeId = query.get("vehicleTypeId")
    if (
      orderId &&
      statusCode &&
      transactionStatus &&
      plateNumber &&
      expiryDate &&
      vehicleTypeId
    ) {
      return {
        orderId,
        statusCode: parseInt(statusCode),
        transactionStatus,
        plateNumber,
        expiryDate,
        vehicleTypeId: parseInt(vehicleTypeId),
      }
    }
    return null
  }, [search])

  useEffect(() => {
    ;(async () => {
      if (paymentStatus && userData) {
        const membership = await membershipService.handlePayMembershipSuccess(
          paymentStatus.orderId,
          paymentStatus.plateNumber,
          paymentStatus.expiryDate,
          paymentStatus.vehicleTypeId,
          userData.id,
        )

        dispatch(updateUserData({ ...userData, membership: membership }))
      }
    })()
  }, [paymentStatus])

  return (
    <div className="max-w-full overflow-hidden pb-24">
      <div className="flex items-center justify-between mb-8">
        <h2 className="text-3xl font-bold tracking-tight">Your Membership</h2>
      </div>
      <div className="mt-4 space-y-4 overflow-auto">
        {userData?.membership &&
        new Date(userData.membership.end).getTime() > new Date().getTime() ? (
          <div className="space-y-8">
            <div className="py-8 flex items-center justify-center flex-col gap-8">
              <img src={logo} className="w-48" />
              <div className="text-2xl font-semibold">
                You are currently a member.
              </div>
            </div>
            <div className="border border-border rounded-md p-4">
              <h3 className="font-semibold text-xl">Membership Info</h3>
              <div className="mt-4 space-y-3">
                <div className="text-xl flex items-center justify-between gap-2">
                  <div className="">Start Date</div>
                  <div className="grow border-b border-border border-dotted relative top-1.5"></div>
                  <div>
                    {format(new Date(userData.membership.start), "dd-MMM-yyy")}
                  </div>
                </div>
                <div className="text-xl flex items-center justify-between gap-2">
                  <div className="">Expiry Date</div>
                  <div className="grow border-b border-border border-dotted relative top-1.5"></div>
                  <div>
                    {format(new Date(userData.membership.end), "dd-MMM-yyy")}
                  </div>
                </div>
                <div className="text-xl flex items-center justify-between gap-2">
                  <div className="">Vehicle Type</div>
                  <div className="grow border-b border-border border-dotted relative top-1.5"></div>
                  <div>{userData.membership.vehicleTypeName}</div>
                </div>
                <div className="text-xl flex items-center justify-between gap-2">
                  <div className="">Plate Number</div>
                  <div className="grow border-b border-border border-dotted relative top-1.5"></div>
                  <div>{userData.membership.plateNumber}</div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="py-8 flex items-center justify-center flex-col gap-8">
            <img src={logo} className="w-48" />
            <div className="text-2xl font-semibold">
              {userData?.membership
                ? "Membership anda sudah habis."
                : "You currently aren't a member."}
            </div>
            <p>
              An admin needs to grant you a membership. Or you could purchase it
              on your own.
            </p>
            <Separator />
            <div>
              <h2 className="mb-8 text-2xl md:text-4xl text-center font-bold">
                View Our Plans
              </h2>
              <div className="flex justify-center w-full min-h-[300px] items-center ">
                <article className="border border-border rounded-md p-4 text-center space-y-8 max-w-full md:w-[500px] lg:[800px]">
                  <header className="text-left">
                    <div className="text-xl md:text-3xl font-semibold text-white mb-2">
                      NexPark Member
                    </div>
                    <div className="">
                      <div className="font-semibold text-2xl md:text-4xl mb-2">
                        {formRupiah(selectedVehicleType?.feePerHour || 0)}
                      </div>
                      <div className="md:text-xl text-white">per day</div>
                    </div>
                    <div className="text-white mt-6 text-xl md:text-2xl">
                      Choose your own expiry date.
                    </div>
                  </header>
                  <div className="py-16 bg-primary text-primary-foreground rounded-md relative">
                    <div className="text-2xl md:text-4xl font-bold">
                      {formRupiah(
                        totalDays * (selectedVehicleType?.feePerHour || 0),
                      )}{" "}
                      <span className="text-xl">/ {totalDays} days</span>
                    </div>
                  </div>
                  <div className="space-y-4">
                    <div className="flex gap-2 items-center justify-between">
                      <div className="md:text-xl font-semibold">
                        Expiry Date
                      </div>
                      <Input
                        type="date"
                        className="w-[150px]"
                        style={{ colorScheme: "dark" }}
                        value={membershipExpiryDate}
                        onChange={e => setMembershipExpiryDate(e.target.value)}
                      />
                    </div>

                    <div className="flex gap-2 items-center justify-between">
                      <div className="md:text-xl font-semibold">
                        Plate Number
                      </div>
                      <Input
                        type="text"
                        placeholder="B 2343"
                        required
                        value={plateNumber}
                        className="w-[150px]"
                        onChange={e => setPlateNumber(e.target.value)}
                      />
                    </div>
                    <div className="flex gap-2 items-center justify-between">
                      <div className="md:text-xl font-semibold">
                        Vehicle Type
                      </div>
                      <Select
                        onValueChange={val => setVehicleTypeId(val)}
                        value={vehicleTypeId}
                      >
                        <SelectTrigger className="w-[150px]">
                          <SelectValue />
                        </SelectTrigger>
                        <SelectContent>
                          {vehicleTypes.map(vt => (
                            <SelectItem key={vt.id} value={vt.id.toString()}>
                              {vt.name}
                            </SelectItem>
                          ))}
                        </SelectContent>
                      </Select>
                    </div>
                  </div>
                  <Separator />
                  <div className="flex justify-end">
                    <Button
                      onClick={async () => {
                        if (userData) {
                          const token =
                            await membershipService.getMembershipToken({
                              vehicleTypeId: parseInt(vehicleTypeId),
                              expiryDate: membershipExpiryDate,
                              plateNumber,
                              userId: userData.id,
                            })
                          setToken(token)
                        }
                      }}
                    >
                      Get Membership
                    </Button>
                  </div>
                </article>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

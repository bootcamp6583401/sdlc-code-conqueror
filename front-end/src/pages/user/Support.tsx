import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { ScrollArea } from "@/components/ui/scroll-area"
import Message from "@/components/Message"
import { SendHorizonal } from "lucide-react"
import icon from "@/assets/icons/nex-park-icon.svg"
import { useEffect, useMemo, useState } from "react"
import { useAbly } from "ably/react"
import { Types } from "ably"
import { AdminNotification, Message as IMessage } from "@/types"
import { useAppSelector } from "@/app/hooks"
import { selectUserData } from "@/features/auth/authSlice"
import { v4 as uuidv4 } from "uuid"

export default function Support() {
  const ablyClient = useAbly()
  const [messages, setMessages] = useState<IMessage[]>([])
  const userData = useAppSelector(selectUserData)

  const [messageContent, setMessageContent] = useState("")

  const [currentlyConnectedAdminName, setCurrentlyConnectedAdminName] =
    useState<null | string>(null)

  const [hasNotified, setHasNotified] = useState(false)

  const chatChannel = useMemo(() => {
    return ablyClient.channels.get(`message:${userData?.id}`)
  }, [ablyClient.channels, userData])

  const adminNotificationChannel = useMemo(() => {
    return ablyClient.channels.get(`admin-notification`)
  }, [ablyClient.channels])

  useEffect(() => {
    const chatListener = (ablyMessage: Types.Message) => {
      const message = ablyMessage.data as IMessage
      if (message.senderId !== userData?.id) {
        setCurrentlyConnectedAdminName(message.sender)
      }
      setMessages(prev => [...prev, message])
    }

    chatChannel.subscribe(chatListener)

    return () => {
      chatChannel.unsubscribe(chatListener)
    }
  }, [chatChannel])

  return (
    <div className="max-w-full overflow-hidden pb-24">
      <div className="flex items-center justify-between">
        <h2 className="text-3xl font-bold tracking-tight">Support</h2>{" "}
        {currentlyConnectedAdminName && (
          <div>
            Connected with Admin{" "}
            <span className="font-semibold">{currentlyConnectedAdminName}</span>
          </div>
        )}
      </div>
      <div className="mt-4 space-y-4 overflow-auto">
        {messages.length > 0 ? (
          <ScrollArea className="h-[500px] w-full rounded-md border p-4">
            <div className="flex flex-col gap-4 items-start">
              {messages.map(m => (
                <Message
                  key={m.id}
                  isSent={m.senderId !== userData?.id}
                  message={m}
                />
              ))}
            </div>
          </ScrollArea>
        ) : (
          <div className="border-border border rounded-md p-4 h-[500px] flex flex-col items-center justify-center gap-8">
            <img src={icon} className="w-48" />
            <div className="text-2xl text-center">
              Start typing messages. An admin will be with you shortly...
            </div>
          </div>
        )}

        <form
          className="flex gap-2"
          onSubmit={e => {
            e.preventDefault()
            if (messageContent && userData) {
              const message: IMessage = {
                id: uuidv4(),
                sender: userData.username,
                senderId: userData.id,
                content: messageContent,
              }
              chatChannel.publish("chat", message)
              if (!hasNotified && userData) {
                setHasNotified(true)
                const not: AdminNotification = {
                  fromId: userData.id,
                  initialMessage: message,
                }
                adminNotificationChannel.publish("notify", not)
              }
            }
          }}
        >
          <Input
            type="text"
            className="grow"
            placeholder="Type something..."
            value={messageContent}
            onChange={e => setMessageContent(e.target.value)}
          />
          <Button type="submit">
            <SendHorizonal />
          </Button>
        </form>
      </div>
    </div>
  )
}

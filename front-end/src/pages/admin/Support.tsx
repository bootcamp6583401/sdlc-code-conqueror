import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { ScrollArea } from "@/components/ui/scroll-area"
import Message from "@/components/Message"
import { SendHorizonal } from "lucide-react"
import { useEffect, useMemo, useState } from "react"
import { useAbly } from "ably/react"
import { Types } from "ably"
import { Message as IMessage, IUser } from "@/types"
import { useAppSelector } from "@/app/hooks"
import { selectUserData } from "@/features/auth/authSlice"
import { v4 as uuidv4 } from "uuid"
import { useLocation, useParams } from "react-router-dom"
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import { Separator } from "@/components/ui/separator"
import { useService } from "@/contexts/ServiceContext"

export default function AdminSupport() {
  const { id } = useParams()
  const ablyClient = useAbly()
  const [messages, setMessages] = useState<IMessage[]>([])
  const userData = useAppSelector(selectUserData)

  const { search } = useLocation()

  const [connectedUser, setConnectedUser] = useState<IUser | null>(null)
  const { usersService } = useService()

  useEffect(() => {
    ;(async () => {
      if (id) {
        const connectedUser = await usersService.getUserById(parseInt(id))

        setConnectedUser(connectedUser)
      }
    })()
  }, [id])

  const initMessage: IMessage | null = useMemo(() => {
    const query = new URLSearchParams(search)

    const sender = query.get("sender")
    const senderId = query.get("senderId")
    const content = query.get("content")

    if (sender && senderId && content) {
      return {
        id: uuidv4(),
        sender: sender,
        senderId: parseInt(senderId),
        content: content,
      }
    }
    return null
  }, [search])

  const [messageContent, setMessageContent] = useState("")

  const chatChannel = useMemo(() => {
    return ablyClient.channels.get(`message:${id}`)
  }, [ablyClient.channels, id])

  useEffect(() => {
    const chatListener = (ablyMessage: Types.Message) => {
      const message = ablyMessage.data as IMessage
      setMessages(prev => [...prev, message])
    }

    chatChannel.subscribe(chatListener)

    return () => {
      chatChannel.unsubscribe(chatListener)
    }
  }, [chatChannel])

  useEffect(() => {
    if (initMessage) {
      setMessages([initMessage])
    }
  }, [initMessage])

  return (
    <div className="max-w-full overflow-hidden pb-24">
      <div className="flex items-center justify-between">
        <h2 className="text-3xl font-bold tracking-tight">Support</h2>
        <Dialog>
          <DialogTrigger asChild>
            <Button className="">
              {connectedUser ? connectedUser.username : "Loading..."}
            </Button>
          </DialogTrigger>
          <DialogContent>
            <DialogHeader>
              <DialogTitle>User Details</DialogTitle>
              <DialogDescription>
                This is the user requesting support.
              </DialogDescription>
            </DialogHeader>
            {connectedUser && (
              <div>
                <Separator />
                <div className="flex flex-col gap-2 py-4">
                  <div className="flex justify-between">
                    <div>Username</div>
                    <div>{connectedUser.username}</div>
                  </div>
                  <div className="flex justify-between">
                    <div>Email</div>
                    <div>{connectedUser.email}</div>
                  </div>
                  <div className="flex justify-between">
                    <div>Transactions</div>
                    <div>{connectedUser.ticket.length}</div>
                  </div>
                </div>
              </div>
            )}
          </DialogContent>
        </Dialog>
      </div>
      <div className="mt-4 space-y-4 overflow-auto">
        <ScrollArea className="h-[500px] w-full rounded-md border p-4">
          <div className="flex flex-col gap-4 items-start">
            {messages.map(m => (
              <Message
                key={m.id}
                isSent={m.senderId !== userData?.id}
                message={m}
              />
            ))}
          </div>
        </ScrollArea>

        <form
          className="flex gap-2"
          onSubmit={e => {
            e.preventDefault()
            if (messageContent && userData) {
              const message: IMessage = {
                id: uuidv4(),
                sender: userData.username,
                senderId: userData.id,
                content: messageContent,
              }
              chatChannel.publish("chat", message)
            }
          }}
        >
          <Input
            type="text"
            className="grow"
            placeholder="Type something..."
            value={messageContent}
            onChange={e => setMessageContent(e.target.value)}
          />
          <Button type="submit">
            <SendHorizonal />
          </Button>
        </form>
      </div>
    </div>
  )
}

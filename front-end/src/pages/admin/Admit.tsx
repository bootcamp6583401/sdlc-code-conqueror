import { useEffect, useMemo, useState } from "react"
import { useAbly } from "ably/react"
import { Types } from "ably"
import icon from "@/assets/icons/nex-park-icon.svg"
import { Navigate } from "react-router-dom"

export default function Admit() {
  const ablyClient = useAbly()

  const [scannedId, setScannedId] = useState("")

  const channel = useMemo(() => {
    return ablyClient.channels.get(`admit`)
  }, [ablyClient.channels])

  useEffect(() => {
    const listener = (ablyMessage: Types.Message) => {
      const message = ablyMessage.data as string
      message.split("")
      setScannedId(message)
    }
    channel.subscribe(listener)

    return () => {
      channel.unsubscribe(listener)
    }
  }, [channel])
  return (
    <div className="min-h-[500px] flex flex-col">
      <h2 className="text-3xl font-bold tracking-tight mb-4">Scanner</h2>

      <div className="grow flex flex-col">
        {scannedId ? (
          <Navigate to={`/admin/tickets/${scannedId}`} />
        ) : (
          <div className="flex flex-col items-center justify-center grow gap-8 border-border border rounded-md">
            <img src={icon} className="w-48" />
            <div className="text-center">
              <div className="font-semibold text-2xl">Waiting for scan...</div>
              <div className="text-muted-foreground tracking-tighter">
                Use your phone to scan the ticket.
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

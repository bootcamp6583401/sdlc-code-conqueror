import { Button } from "@/components/ui/button"
import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import { useEffect, useState } from "react"
import { BoolFilter, IUser, Page, VehicleType } from "@/types"
import { CheckCircle2, SortAsc, SortDesc, X, XCircle } from "lucide-react"
import { filterUsers, isUserAdmin, sortUsers } from "@/lib/utils"
import { Input } from "@/components/ui/input"
import { useToast } from "@/components/ui/use-toast"
import { Label } from "@/components/ui/label"
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"
import Search from "@/components/ui/search"
import { useService } from "@/contexts/ServiceContext"

export default function Memerships() {
  const [users, setUsers] = useState<IUser[]>([])
  const [membershipExpiryDate, setMembershipExpiryDate] = useState("")
  const [plateNumber, setPlateNumber] = useState("")

  const { usersService } = useService()

  const [selectedUser, setSelectedUser] = useState<IUser | null>(null)
  const [addMembershipModalOpen, setAddMembershipModalOpen] = useState(false)
  const [userPageData, setUserPageData] = useState<Page<IUser> | null>(null)
  const [vehicleTypeId, setVehicleTypeId] = useState("1")
  const [vehicleTypes, setVehicleTypes] = useState<VehicleType[]>([])
  const [revokeMembershipModalOpen, setRevokeMembershipModalOpen] =
    useState(false)

  // Filter and sort
  const [sortDir, setSortDir] = useState<"ASC" | "DESC">("ASC")
  const [sortBy, setSortBy] = useState<"USERNAME" | "EMAIL" | "ID">("ID")
  const [membershipFilter, setMembershipFilter] = useState<BoolFilter>(
    BoolFilter.ALL,
  )

  const { membershipService, vehicleTypesService } = useService()

  const { toast } = useToast()
  useEffect(() => {
    ;(async () => {
      const users = await usersService.getPagebleUsers()
      setUserPageData(users)

      const vts = await vehicleTypesService.getVehicleTypes()
      setVehicleTypes(vts)
      setVehicleTypeId(vts[0].id.toString())
    })()
  }, [])

  useEffect(() => {
    if (userPageData) {
      setUsers(userPageData.content)
    }
  }, [userPageData])

  return (
    <div>
      <div className="flex flex-col space-y-[20px] lg:flex-row items-start lg:items-center justify-between">
        <h2 className="text-3xl font-bold tracking-tight">Users</h2>
        <Search setUserPageData={setUserPageData} />
      </div>
      <div className="mt-4 space-y-4">
        <div className="border-border rounded-md border p-4">
          <div className="flex flex-col space-y-[10px] md:space-y-0 md:flex-row md:justify-between mb-4 md:items-center">
            <h3 className="text-xl font-semibold">Users</h3>
            <div className="flex gap-4">
              <div>
                <Label>Sort</Label>
                <div className="flex gap-2">
                  <Button
                    variant="outline"
                    size="icon"
                    onClick={() =>
                      setSortDir(sortDir === "ASC" ? "DESC" : "ASC")
                    }
                  >
                    {sortDir === "ASC" ? <SortAsc /> : <SortDesc />}
                  </Button>
                  <Select
                    value={sortBy}
                    onValueChange={val =>
                      setSortBy(val as "ID" | "EMAIL" | "USERNAME")
                    }
                  >
                    <SelectTrigger className="w-[100px] border-border">
                      <SelectValue placeholder="Sort By" />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectItem value="ID">ID</SelectItem>
                      <SelectItem value="ENTRY">Email</SelectItem>
                      <SelectItem value="USERNAME">Username</SelectItem>
                    </SelectContent>
                  </Select>
                </div>
              </div>

              <div>
                <Label>Membership</Label>
                <Select
                  value={membershipFilter}
                  onValueChange={val => {
                    const value = val as BoolFilter
                    setMembershipFilter(value)
                  }}
                >
                  <SelectTrigger className="w-[100px] border-border">
                    <SelectValue placeholder="Sort By" />
                  </SelectTrigger>
                  <SelectContent>
                    <SelectItem value={BoolFilter.ALL}>All</SelectItem>
                    <SelectItem value={BoolFilter.YES}>Yes</SelectItem>
                    <SelectItem value={BoolFilter.NO}>No</SelectItem>
                  </SelectContent>
                </Select>
              </div>
            </div>
          </div>

          <Table>
            <TableCaption>List of NexPark's registered users.</TableCaption>
            <TableHeader>
              <TableRow>
                <TableHead className="w-[100px]">User ID.</TableHead>
                <TableHead>Username</TableHead>
                <TableHead>Email</TableHead>
                <TableHead>Membership</TableHead>
                <TableHead>Admin</TableHead>
                <TableHead className="text-right">Actions</TableHead>
              </TableRow>
            </TableHeader>
            <TableBody>
              {sortUsers(
                filterUsers(users, membershipFilter),
                sortDir,
                sortBy,
              ).map(u => (
                <TableRow key={u.id}>
                  <TableCell className="font-medium">{u.id}</TableCell>
                  <TableCell>{u.username}</TableCell>
                  <TableCell>{u.email}</TableCell>
                  <TableCell>
                    <div className="flex items-center gap-1">
                      {u.membership ? (
                        <>
                          <CheckCircle2 size={16} /> <span>Yes</span>
                        </>
                      ) : (
                        <>
                          <XCircle size={16} /> <span>None</span>
                        </>
                      )}
                    </div>
                  </TableCell>
                  <TableCell>
                    <div className="flex items-center gap-1">
                      {isUserAdmin(u) ? (
                        <>
                          <CheckCircle2 size={16} /> <span>Yes</span>
                        </>
                      ) : (
                        <>
                          <XCircle size={16} /> <span>No</span>
                        </>
                      )}
                    </div>
                  </TableCell>
                  <TableCell className="flex gap-2 justify-end">
                    {u.membership ? (
                      <Button
                        disabled={isUserAdmin(u)}
                        variant="outline"
                        onClick={() => {
                          setSelectedUser(u)
                          setRevokeMembershipModalOpen(true)
                        }}
                      >
                        Revoke Membership
                      </Button>
                    ) : (
                      <Button
                        disabled={isUserAdmin(u)}
                        variant="outline"
                        onClick={() => {
                          setSelectedUser(u)
                          setAddMembershipModalOpen(true)
                        }}
                      >
                        Add Membership
                      </Button>
                    )}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <div className="flex gap-2 mt-8">
            {Array.from({ length: userPageData?.totalPages || 0 }).map(
              (_, index) => (
                <Button
                  key={index + 1}
                  variant={
                    userPageData?.pageable.pageNumber === index
                      ? "default"
                      : "outline"
                  }
                  onClick={async () => {
                    setUserPageData(await usersService.getPagebleUsers(index))
                  }}
                  size="sm"
                >
                  {index + 1}
                </Button>
              ),
            )}
          </div>
        </div>
      </div>
      <Dialog
        open={addMembershipModalOpen}
        onOpenChange={setAddMembershipModalOpen}
      >
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Add Membership</DialogTitle>
            <DialogDescription>Give user a membership.</DialogDescription>
          </DialogHeader>
          {selectedUser && (
            <div>
              <div className="flex flex-col gap-2">
                <div className="flex justify-between items-center">
                  <div className="font-semibold">User ID</div>
                  <div className="font-semibold">#{selectedUser.id}</div>
                </div>
                <div className="flex justify-between items-center">
                  <div className="font-semibold">Username</div>
                  <div className="font-semibold">{selectedUser.username}</div>
                </div>
                <div className="flex justify-between items-center">
                  <div className="font-semibold">Email</div>
                  <div className="font-semibold">{selectedUser.email}</div>
                </div>
                <div className="flex justify-between items-center">
                  <div className="font-semibold">Expiry Date</div>
                  <div className="font-semibold">
                    <Input
                      type="date"
                      style={{ colorScheme: "dark" }}
                      value={membershipExpiryDate}
                      onChange={e => setMembershipExpiryDate(e.target.value)}
                    />
                  </div>
                </div>
                <div className="flex justify-between items-center">
                  <div className="font-semibold">Plate Number</div>
                  <div className="font-semibold">
                    <Input
                      type="text"
                      style={{ colorScheme: "dark" }}
                      value={plateNumber}
                      onChange={e => setPlateNumber(e.target.value)}
                    />
                  </div>
                </div>
                <div className="flex justify-between items-center">
                  <div className="font-semibold">Vehicle Type</div>
                  <div className="font-semibold">
                    <Select
                      onValueChange={val => setVehicleTypeId(val)}
                      value={vehicleTypeId}
                    >
                      <SelectTrigger className="w-full">
                        <SelectValue />
                      </SelectTrigger>
                      <SelectContent>
                        {vehicleTypes.map(vt => (
                          <SelectItem key={vt.id} value={vt.id.toString()}>
                            {vt.name}
                          </SelectItem>
                        ))}
                      </SelectContent>
                    </Select>
                  </div>
                </div>
              </div>
              <div className="mt-8 flex justify-end gap-2 items-center">
                <div className="mr-auto">Grant user membership?</div>
                <Button
                  variant="outline"
                  onClick={() => {
                    setAddMembershipModalOpen(false)
                    setSelectedUser(null)
                  }}
                >
                  Cancel
                </Button>
                <Button
                  onClick={async () => {
                    if (!membershipExpiryDate)
                      toast({ title: "Please include expirty date." })
                    else if (
                      new Date().getTime() >
                      new Date(membershipExpiryDate).getTime()
                    )
                      toast({
                        title:
                          "Expiry date can't be earlier than current date.",
                      })
                    else {
                      try {
                        const membership =
                          await membershipService.grantMembership(
                            selectedUser.id,
                            membershipExpiryDate,
                            plateNumber,
                            parseInt(vehicleTypeId),
                          )
                        setUsers(prev =>
                          prev.map(u =>
                            u.id === selectedUser.id
                              ? { ...u, membership: membership }
                              : u,
                          ),
                        )
                        toast({
                          title:
                            "Successfully granted membership to user " +
                            selectedUser.username,
                        })
                        setSelectedUser(null)
                        setAddMembershipModalOpen(false)
                      } catch (error) {}
                    }
                  }}
                >
                  Confirm
                </Button>
              </div>
            </div>
          )}
        </DialogContent>
      </Dialog>
      <Dialog
        open={revokeMembershipModalOpen}
        onOpenChange={setRevokeMembershipModalOpen}
      >
        <DialogContent>
          <DialogHeader>
            <DialogTitle>Revoke Membership</DialogTitle>
            <DialogDescription>
              Revoke membership from user #{selectedUser?.id}
            </DialogDescription>
          </DialogHeader>
          {selectedUser && (
            <div>
              <div className="mt-8 flex justify-end gap-2 items-center">
                <div className="mr-auto">Revoke user membership?</div>
                <Button
                  variant="outline"
                  onClick={() => {
                    setRevokeMembershipModalOpen(false)
                    setSelectedUser(null)
                  }}
                >
                  Cancel
                </Button>
                <Button
                  onClick={async () => {
                    if (selectedUser.membership) {
                      try {
                        await membershipService.revokeMembership(
                          selectedUser.membership.id,
                        )
                        setUsers(prev =>
                          prev.map(u =>
                            u.id === selectedUser.id
                              ? { ...u, membership: null }
                              : u,
                          ),
                        )
                        toast({
                          title:
                            "Successfully revoked membership from user " +
                            selectedUser.username,
                        })
                        setSelectedUser(null)
                        setRevokeMembershipModalOpen(false)
                      } catch (error) {}
                    }
                  }}
                >
                  Confirm
                </Button>
              </div>
            </div>
          )}
        </DialogContent>
      </Dialog>
    </div>
  )
}

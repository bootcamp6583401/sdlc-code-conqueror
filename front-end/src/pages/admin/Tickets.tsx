import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import { RefreshCcw, SortAsc, SortDesc } from "lucide-react"
import { useAppSelector } from "@/app/hooks"
import { selectAuth, selectUserData } from "@/features/auth/authSlice"
import { Button } from "@/components/ui/button"
import { useEffect, useState } from "react"
import {
  BoolFilter,
  IUser,
  Page,
  Ticket,
  TicketState,
  VehicleType,
} from "@/types"
import TicketPreview from "@/components/TicketPreview"
import { format } from "date-fns"
import { useNavigate } from "react-router-dom"
import {
  exportToExcel,
  filterAndSortUserTickets,
  filterTickets,
  filterTicketsByDateRange,
  formRupiah,
  getProjectedTicketTotal,
  mapTicketState,
} from "@/lib/utils"
import { Label } from "@/components/ui/label"
import { Input } from "@/components/ui/input"
import { useService } from "@/contexts/ServiceContext"

export default function Tickets() {
  const [selectedTicket, setSelectedTicket] = useState<Ticket | null>(null)
  const [vehicleTypeId, setVehicleTypeId] = useState("1")
  const user = useAppSelector(selectAuth)
  const [userTickets, setUserTickets] = useState<Ticket[]>([])
  const [userTicketPageData, setUserTicketPageData] =
    useState<Page<Ticket> | null>(null)
  const [vehicleTypes, setVehicleTypes] = useState<VehicleType[]>([])
  const [generateTicketModalOpen, setGenerateTicketModalOpen] = useState(false)
  const [previewModalOpen, setPreviewModalOpen] = useState(false)
  const navigate = useNavigate()
  const [ticketFilter, setTicketFilter] = useState<TicketState>(
    TicketState.PENDING,
  )
  const [membershipFilter, setMembershipFilter] = useState<BoolFilter>(
    BoolFilter.ALL,
  )
  const [vehicleTypeFilter, setVehicleTypeFilter] = useState<string>("ALL")
  const [sortDir, setSortDir] = useState<"ASC" | "DESC">("ASC")
  const [sortBy, setSortBy] = useState<"PLATE" | "ENTRY" | "VEHICLE">("ENTRY")

  const userData = useAppSelector(selectUserData)

  const [tickets, setTickets] = useState<Ticket[]>([])
  const [allTickets, setAllTickets] = useState<Ticket[]>([])
  const [startDate, setStartDate] = useState<string>("")
  const [endDate, setEndDate] = useState<string>("")
  const [filteredTickets, setFilteredTickets] = useState<Ticket[]>([])
  const [isFiltered, setIsFiltered] = useState<boolean>(false)

  const { ticketsService, vehicleTypesService } = useService()

  useEffect(() => {
    ;(async () => {
      const vts = await vehicleTypesService.getVehicleTypes()
      setVehicleTypes(vts)

      const pageData = await ticketsService.getPagebleTickets()
      setUserTicketPageData(pageData)
      setVehicleTypeId(vts[0].id.toString())
    })()
  }, [])

  useEffect(() => {
    if (userTicketPageData) {
      setTickets(userTicketPageData.content)
    }
  }, [userTicketPageData])

  useEffect(() => {
    ;(async () => {
      const tickets = await ticketsService.getAllTickets()
      setAllTickets(tickets)
    })()
  }, [])

  return (
    <div className="max-w-full overflow-hidden pb-24">
      <div className="flex justify-between">
        <div>
          <h2 className="text-3xl font-bold tracking-tight">
            List All Tickets
          </h2>
        </div>
        <div className="flex gap-2">
          <Button
            variant="outline"
            size="icon"
            onClick={async () => {
              const pageData = await ticketsService.getPagebleTickets()
              setUserTicketPageData(pageData)
            }}
          >
            <RefreshCcw />
          </Button>
          <Button
            onClick={() => exportToExcel(allTickets, "Ticket_Report")}
            variant="default"
          >
            Export All to Excel
          </Button>
        </div>
      </div>
      <div className="mt-4 space-y-8 overflow-auto">
        <div className="border-border rounded-md border p-4 min-h-[500px] flex flex-col">
          <div className="flex justify-between mb-4 overflow-auto items-end pb-5">
            <div className="flex gap-2 items-end">
              <div className="bg-primary flex gap-1 p-1 rounded-md overflow-hidden">
                <Button
                  onClick={() => setTicketFilter(TicketState.PENDING)}
                  variant={
                    ticketFilter === TicketState.PENDING
                      ? "secondary"
                      : "default"
                  }
                >
                  Pending
                </Button>
                <Button
                  onClick={() => setTicketFilter(TicketState.ACTIVE)}
                  variant={
                    ticketFilter === TicketState.ACTIVE
                      ? "secondary"
                      : "default"
                  }
                >
                  Active
                </Button>
                <Button
                  onClick={() => setTicketFilter(TicketState.PAID)}
                  variant={
                    ticketFilter === TicketState.PAID ? "secondary" : "default"
                  }
                >
                  Paid
                </Button>
              </div>
              <div className="flex gap-4">
                <div>
                  <Label>Sort</Label>
                  <div className="flex gap-2">
                    <Button
                      variant="outline"
                      size="icon"
                      onClick={() =>
                        setSortDir(sortDir === "ASC" ? "DESC" : "ASC")
                      }
                    >
                      {sortDir === "ASC" ? <SortAsc /> : <SortDesc />}
                    </Button>
                    <Select
                      value={sortBy}
                      onValueChange={val => setSortBy(val as "ENTRY" | "PLATE")}
                    >
                      <SelectTrigger className="w-[100px] border-border">
                        <SelectValue placeholder="Sort By" />
                      </SelectTrigger>
                      <SelectContent>
                        <SelectItem value="ENTRY">Entry</SelectItem>
                        <SelectItem value="PLATE">Plate</SelectItem>
                      </SelectContent>
                    </Select>
                  </div>
                </div>

                <div>
                  <Label>Membership</Label>
                  <Select
                    value={membershipFilter}
                    onValueChange={val => {
                      const value = val as BoolFilter
                      setMembershipFilter(value)
                    }}
                  >
                    <SelectTrigger className="w-[100px] border-border">
                      <SelectValue placeholder="Sort By" />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectItem value={BoolFilter.ALL}>All</SelectItem>
                      <SelectItem value={BoolFilter.YES}>Yes</SelectItem>
                      <SelectItem value={BoolFilter.NO}>No</SelectItem>
                    </SelectContent>
                  </Select>
                </div>
                <div>
                  <Label>Vehicle Type</Label>
                  <Select
                    value={vehicleTypeFilter}
                    onValueChange={val => {
                      setVehicleTypeFilter(val)
                    }}
                  >
                    <SelectTrigger className="w-[100px] border-border">
                      <SelectValue placeholder="Sort By" />
                    </SelectTrigger>
                    <SelectContent>
                      <SelectItem value="ALL">All</SelectItem>
                      {vehicleTypes.map(vt => (
                        <SelectItem key={vt.id} value={vt.name}>
                          {vt.name}
                        </SelectItem>
                      ))}
                    </SelectContent>
                  </Select>
                </div>
              </div>
              <div className="flex gap-2">
                <div>
                  <Label>Start Date</Label>
                  <Input
                    type="date"
                    style={{ colorScheme: "dark" }}
                    value={startDate}
                    onChange={e => setStartDate(e.target.value)}
                  />
                </div>
                <div>
                  <Label>End Date</Label>
                  <Input
                    type="date"
                    style={{ colorScheme: "dark" }}
                    value={endDate}
                    onChange={e => setEndDate(e.target.value)}
                  />
                </div>
              </div>
              <Button
                variant="outline"
                onClick={() => {
                  const filtered = filterTicketsByDateRange(
                    tickets,
                    startDate,
                    endDate,
                  )
                  setFilteredTickets(filtered)
                  setIsFiltered(true)
                }}
              >
                Filter
              </Button>
            </div>
          </div>
          <Table className="max-w-full overflow-scroll">
            {/* <TableCaption>A list of your tickets</TableCaption> */}
            <TableHeader>
              <TableRow>
                <TableHead className="w-[100px]">Ticket No.</TableHead>
                <TableHead>License Plate</TableHead>
                <TableHead>Entry Time</TableHead>
                <TableHead>Vehile Type</TableHead>
                <TableHead>Membership?</TableHead>
                <TableHead className="text-right">
                  {mapTicketState(
                    ticketFilter,
                    "Enter First",
                    "Projected Total",
                    "Paid Total",
                  )}
                </TableHead>
                <TableHead className="text-right">Actions</TableHead>
              </TableRow>
            </TableHeader>
            <TableBody>
              {filterTickets(
                isFiltered ? filteredTickets : tickets,
                ticketFilter,
                membershipFilter,
                vehicleTypeFilter,
              ).length === 0 && (
                <TableRow>
                  <TableCell colSpan={7} className="text-center">
                    No tickets found.
                  </TableCell>
                </TableRow>
              )}
              {filterAndSortUserTickets(
                isFiltered ? filteredTickets : tickets,
                ticketFilter,
                sortDir,
                sortBy,
                membershipFilter,
                vehicleTypeFilter,
              ).map(ut => (
                <TableRow key={ut.id}>
                  <TableCell className="font-medium">
                    {ut.id.slice(0, 5)}...
                  </TableCell>
                  <TableCell>{ut.plateNumber}</TableCell>
                  <TableCell>
                    {ut.entryTime
                      ? format(new Date(ut.entryTime), "dd MM yyyy, hh:mm:ss")
                      : "None"}
                  </TableCell>
                  <TableCell>{ut.vehicleTypeName}</TableCell>
                  <TableCell>
                    {mapTicketState(
                      ticketFilter,
                      ut.doesUserHaveActiveMembership ? "Yes" : "No",
                      ut.doesUserHaveActiveMembership ? "Yes" : "No",
                      ut.onMembership ? "Yes" : "No",
                    )}
                  </TableCell>
                  <TableCell className="text-right">
                    {mapTicketState(
                      ticketFilter,
                      "Enter First",
                      formRupiah(getProjectedTicketTotal(ut)),
                      ut.totalFee !== null ? formRupiah(ut.totalFee) : "None",
                    )}
                  </TableCell>
                  <TableCell className="flex gap-2 justify-end">
                    <Button
                      variant="outline"
                      onClick={() => {
                        setSelectedTicket(ut)
                        setPreviewModalOpen(true)
                      }}
                    >
                      Preview
                    </Button>
                    <Button
                      variant="outline"
                      onClick={() => navigate(`/admin/tickets/${ut.id}`)}
                    >
                      Details
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          <div className="flex gap-2 mt-auto">
            {Array.from({ length: userTicketPageData?.totalPages || 0 }).map(
              (_, index) => (
                <Button
                  key={index + 1}
                  variant={
                    userTicketPageData?.pageable.pageNumber === index
                      ? "default"
                      : "outline"
                  }
                  onClick={async () => {
                    setUserTicketPageData(
                      await ticketsService.getPagebleTickets(index),
                    )
                  }}
                  size="sm"
                >
                  {index + 1}
                </Button>
              ),
            )}
          </div>
        </div>
      </div>
      <Dialog open={previewModalOpen} onOpenChange={setPreviewModalOpen}>
        {selectedTicket && (
          <DialogContent>
            <DialogHeader>
              <DialogTitle>Your Ticket</DialogTitle>
              <DialogDescription>
                Preview of ticket {selectedTicket.id.slice(0, 4)}...
                {selectedTicket.id.slice(-4)}
              </DialogDescription>
            </DialogHeader>
            <div>
              <div className="flex justify-center w-full min-h-[300px] items-center">
                <TicketPreview ticket={selectedTicket} />
              </div>
            </div>
          </DialogContent>
        )}
      </Dialog>
    </div>
  )
}

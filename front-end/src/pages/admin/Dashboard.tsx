import React, { useEffect, useState } from "react"
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import { Bar, BarChart, ResponsiveContainer, XAxis, YAxis } from "recharts"
import icon from "@/assets/icons/nex-park-icon.svg"

import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"

import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableFooter,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"

import DashboardWidget from "@/components/DashboardWidget"
import { DollarSign, ParkingCircle, User, Users2 } from "lucide-react"
import { IUser, Ticket } from "@/types"
import { Button, buttonVariants } from "@/components/ui/button"
import { Link } from "react-router-dom"
import { cn, formRupiah } from "@/lib/utils"
import { format } from "date-fns"
import { useService } from "@/contexts/ServiceContext"

export default function Dashboard() {
  const [tickets, setTickets] = useState<Ticket[]>([])
  const [users, setUsers] = useState<IUser[]>([])
  const { ticketsService, usersService } = useService()
  useEffect(() => {
    ;(async () => {
      setTickets(await ticketsService.getAllTickets())
      setUsers(await usersService.getAllUsers())
    })()
  }, [])

  const now = new Date()
  const selectedDay = now.toISOString().substring(0, 10)
  const filteredTotalFee = tickets
    ? tickets
        .filter(t => t.exitTime && t.exitTime.substring(0, 10) === selectedDay)
        .map(t => t.totalFee || 0)
        .reduce((a, b) => a + b, 0)
        .toString()
    : "0"

  const currentYear = new Date().getFullYear()

  const monthlyData = Array.from({ length: 12 }, (_, i) => ({
    name: new Date(0, i).toLocaleString("default", { month: "short" }),
    total: 0,
  }))

  tickets.forEach(ticket => {
    const exitDate = new Date(ticket.exitTime || "")
    if (exitDate.getFullYear() === currentYear) {
      const monthIndex = exitDate.getMonth()
      monthlyData[monthIndex].total += ticket.totalFee || 0
    }
  })

  return (
    <div>
      <div className="flex items-center justify-between">
        <h2 className="text-3xl font-bold tracking-tight">Dashboard</h2>
        <div className="flex gap-2">
          <Link to="/admin/scan" className={cn(buttonVariants())}>
            Scan Ticket
          </Link>
        </div>
      </div>
      <div className="mt-4 space-y-4">
        <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-4">
          <DashboardWidget
            title="Today's Revenue"
            Icon={DollarSign}
            description="Revenue from today"
            value={formRupiah(parseInt(filteredTotalFee))}
          />
          <DashboardWidget
            title="Users"
            Icon={Users2}
            description="Registered Users"
            value={users.length.toString() + " Parkers"}
          />
          <DashboardWidget
            title="Members"
            Icon={User}
            description="Users in memberships"
            value={users.filter(u => !!u.membership).length.toString()}
          />
          <DashboardWidget
            title="Currently Parking"
            Icon={ParkingCircle}
            description="Vehicles currently parking"
            value={tickets
              .filter(t => !!t.entryTime && !!!t.exitTime)
              .length.toString()}
          />
        </div>
        <div className="grid gap-4 md:grid-cols-2 lg:grid-cols-7">
          <Card className="lg:col-span-4">
            <CardHeader>
              <CardTitle>Overview</CardTitle>
            </CardHeader>
            <CardContent className="pl-2">
              <ResponsiveContainer width="100%" height={350}>
                <BarChart data={monthlyData}>
                  <XAxis
                    dataKey="name"
                    stroke="#888888"
                    fontSize={12}
                    tickLine={false}
                    axisLine={false}
                  />
                  <YAxis
                    stroke="#888888"
                    fontSize={12}
                    tickLine={false}
                    axisLine={false}
                    tickFormatter={value => formRupiah(parseInt(value))}
                  />
                  <Bar
                    dataKey="total"
                    fill="currentColor"
                    radius={[4, 4, 0, 0]}
                    className="fill-primary"
                  />
                </BarChart>
              </ResponsiveContainer>
            </CardContent>
          </Card>
          <Card className="lg:col-span-3">
            <CardHeader>
              <CardTitle>Latest Tickets</CardTitle>
              <CardDescription>Show 5 Latest Tickets</CardDescription>
            </CardHeader>
            <CardContent>
              <Table>
                <TableHeader>
                  <TableRow>
                    <TableHead>Number Plate</TableHead>
                    <TableHead>Entry Time</TableHead>
                    <TableHead>Status</TableHead>
                  </TableRow>
                </TableHeader>
                <TableBody>
                  {tickets
                    .sort(
                      (a, b) =>
                        new Date(b.entryTime || "").getTime() -
                        new Date(a.entryTime || "").getTime(),
                    )
                    .slice(0, 5)
                    .map(ticket => (
                      <TableRow key={ticket.id}>
                        <TableCell>{ticket.plateNumber}</TableCell>
                        <TableCell>
                          {ticket.entryTime
                            ? format(
                                new Date(ticket.entryTime),
                                "dd MMM yyyy, hh:mm:ss",
                              )
                            : "PENDING"}
                        </TableCell>
                        <TableCell>
                          {ticket.entryTime && ticket.exitTime
                            ? "PAID"
                            : ticket.entryTime && !ticket.exitTime
                              ? "ACTIVE"
                              : "PENDING"}
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  )
}

import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table"
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import { RefreshCcw, SortAsc, SortDesc } from "lucide-react"
import { useAppSelector } from "@/app/hooks"
import { selectAuth, selectUserData } from "@/features/auth/authSlice"
import { Button } from "@/components/ui/button"
import { useEffect, useState } from "react"
import {
  BoolFilter,
  IUser,
  Page,
  Ticket,
  TicketState,
  VehicleType,
} from "@/types"
import TicketPreview from "@/components/TicketPreview"
import { format } from "date-fns"
import { useNavigate } from "react-router-dom"
import {
  exportToExcel,
  filterAndSortUserTickets,
  filterTickets,
  filterTicketsByDateRange,
  formRupiah,
  getProjectedTicketTotal,
  mapTicketState,
  sortVehicleTypes,
} from "@/lib/utils"
import { Label } from "@/components/ui/label"
import { Input } from "@/components/ui/input"
import { useService } from "@/contexts/ServiceContext"
import { useToast } from "@/components/ui/use-toast"

export default function VehicleTypes() {
  const user = useAppSelector(selectAuth)
  const [vehicleTypes, setVehicleTypes] = useState<VehicleType[]>([])
  const navigate = useNavigate()
  const [sortDir, setSortDir] = useState<"ASC" | "DESC">("ASC")
  const [sortBy, setSortBy] = useState<"NAME" | "FEE" | "ID">("ID")
  const [deleteModalOpen, setDeleteModalOpen] = useState(false)
  const [selectedVehicleType, setSelectedVehicleType] =
    useState<null | VehicleType>(null)

  const { toast } = useToast()

  const [dataModalOpen, setDataModalOpen] = useState(false)

  //data modal
  const [name, setName] = useState("")
  const [feePerHour, setfeePerHour] = useState(0)

  const userData = useAppSelector(selectUserData)

  const { vehicleTypesService } = useService()

  useEffect(() => {
    ;(async () => {
      const vts = await vehicleTypesService.getVehicleTypes()
      setVehicleTypes(vts)
    })()
  }, [])

  useEffect(() => {
    if (selectedVehicleType) {
      setName(selectedVehicleType.name)
      setfeePerHour(selectedVehicleType.feePerHour)
    }
  }, [selectedVehicleType])

  const handleDataSubmit = async () => {
    if (selectedVehicleType) {
      const updatedVt = await vehicleTypesService.updateVehicleTypeById(
        selectedVehicleType.id,
        name,
        feePerHour,
      )
      setVehicleTypes(prev =>
        prev.map(vt =>
          vt.id === selectedVehicleType.id ? { ...vt, name, feePerHour } : vt,
        ),
      )
      setName("")
      setfeePerHour(0)
    } else {
      const newVt = await vehicleTypesService.createVehicleType(
        name,
        feePerHour,
      )
      setVehicleTypes(prev => [...prev, newVt])
    }
    setSelectedVehicleType(null)
    setDataModalOpen(false)
  }

  return (
    <div className="max-w-full overflow-hidden pb-24">
      <div className="flex justify-between">
        <div>
          <h2 className="text-3xl font-bold tracking-tight">Vehicle Types</h2>
        </div>
        <div className="flex gap-2">
          <Button variant="default" onClick={() => setDataModalOpen(true)}>
            Create
          </Button>
        </div>
      </div>
      <div className="mt-4 space-y-8 overflow-auto">
        <div className="border-border rounded-md border p-4 min-h-[500px] flex flex-col">
          <div className="flex justify-between mb-4 overflow-auto items-end pb-5">
            <div className="flex gap-2 items-end">
              <div className="flex gap-4">
                <div>
                  <Label>Sort</Label>
                  <div className="flex gap-2">
                    <Button
                      variant="outline"
                      size="icon"
                      onClick={() =>
                        setSortDir(sortDir === "ASC" ? "DESC" : "ASC")
                      }
                    >
                      {sortDir === "ASC" ? <SortAsc /> : <SortDesc />}
                    </Button>
                    <Select
                      value={sortBy}
                      onValueChange={val =>
                        setSortBy(val as "NAME" | "FEE" | "ID")
                      }
                    >
                      <SelectTrigger className="w-[100px] border-border">
                        <SelectValue placeholder="Sort By" />
                      </SelectTrigger>
                      <SelectContent>
                        <SelectItem value="ID">ID</SelectItem>
                        <SelectItem value="NAME">Name</SelectItem>
                        <SelectItem value="FEE">Fee</SelectItem>
                      </SelectContent>
                    </Select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Table className="max-w-full overflow-scroll">
            {/* <TableCaption>A list of your tickets</TableCaption> */}
            <TableHeader>
              <TableRow>
                <TableHead className="w-[100px]">ID</TableHead>
                <TableHead>Name</TableHead>
                <TableHead>Fee per Hour</TableHead>
                <TableHead className="text-right">Actions</TableHead>
              </TableRow>
            </TableHeader>
            <TableBody>
              {vehicleTypes.length === 0 && (
                <TableRow>
                  <TableCell colSpan={7} className="text-center">
                    No types found. It's recommended that one is created.
                  </TableCell>
                </TableRow>
              )}
              {sortVehicleTypes(vehicleTypes, sortDir, sortBy).map(ut => (
                <TableRow key={ut.id}>
                  <TableCell className="font-medium">{ut.id}</TableCell>
                  <TableCell>{ut.name}</TableCell>
                  <TableCell>{formRupiah(ut.feePerHour)}</TableCell>
                  <TableCell className="flex gap-2 justify-end">
                    <Button
                      variant="outline"
                      onClick={() => {
                        setSelectedVehicleType(ut)
                        setDataModalOpen(true)
                      }}
                    >
                      Update
                    </Button>
                    <Button
                      variant="destructive"
                      onClick={async () => {
                        setDeleteModalOpen(true)
                        setSelectedVehicleType(ut)
                      }}
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </div>
      <Dialog open={deleteModalOpen} onOpenChange={setDeleteModalOpen}>
        {selectedVehicleType && (
          <DialogContent>
            <DialogHeader>
              <DialogTitle>Are you sure?</DialogTitle>
              <DialogDescription>Delete type?</DialogDescription>
            </DialogHeader>
            <DialogFooter>
              <Button
                onClick={async () => {
                  if (selectedVehicleType) {
                    try {
                      await vehicleTypesService.deleteVehicleTypeById(
                        selectedVehicleType.id,
                      )
                      setVehicleTypes(prev =>
                        prev.filter(vt => vt.id !== selectedVehicleType.id),
                      )
                    } catch (error) {
                      toast({ title: "Failed to delete." })
                    }
                    setDeleteModalOpen(false)
                    setSelectedVehicleType(null)
                  }
                }}
              >
                Confirm
              </Button>
            </DialogFooter>
          </DialogContent>
        )}
      </Dialog>
      <Dialog open={dataModalOpen} onOpenChange={setDataModalOpen}>
        <DialogContent>
          <DialogHeader>
            <DialogTitle>
              {selectedVehicleType ? "Update" : "Create"} Vehicle Type
            </DialogTitle>
            <DialogDescription>
              Create or update vehicle types
            </DialogDescription>
          </DialogHeader>
          <div className="space-y-4">
            <div className="flex justify-between items-center">
              <div className="font-semibold">Name</div>
              <div className="font-semibold">
                <Input
                  type="text"
                  style={{ colorScheme: "dark" }}
                  value={name}
                  onChange={e => setName(e.target.value)}
                />
              </div>
            </div>
            <div className="flex justify-between items-center">
              <div className="font-semibold">Fee per Hour</div>
              <div className="font-semibold">
                <Input
                  type="number"
                  style={{ colorScheme: "dark" }}
                  value={feePerHour}
                  onChange={e => {
                    const val = parseInt(e.target.value)

                    if (!isNaN(val)) {
                      setfeePerHour(val)
                    } else {
                      setfeePerHour(0)
                    }
                  }}
                />
              </div>
            </div>
          </div>
          <DialogFooter>
            <Button onClick={handleDataSubmit}>
              {selectedVehicleType ? "Update" : "Create"}
            </Button>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </div>
  )
}

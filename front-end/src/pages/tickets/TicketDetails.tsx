import { useEffect, useMemo, useState } from "react"
import { useParams } from "react-router-dom"
import {
  Card,
  CardContent,
  CardDescription,
  CardFooter,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"
import { Button } from "@/components/ui/button"
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select"
import {
  Car,
  CheckCircle2,
  CircleDollarSign,
  LogOut,
  ParkingCircle,
  RectangleHorizontal,
  Timer,
  UsersRound,
  XCircle,
} from "lucide-react"
import QRCode from "react-qr-code"

import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog"
import { Separator } from "@/components/ui/separator"
import { Label } from "@/components/ui/label"
import AdminHeader from "@/components/layouts/AdminHeader"
import TicketPreview from "@/components/TicketPreview"
import { Ticket } from "@/types"
import { format, setDate } from "date-fns"
import { countHoursBetween, formRupiah } from "@/lib/utils"
import { Input } from "@/components/ui/input"
import { useService } from "@/contexts/ServiceContext"

export default function TicketDetails({
  adminView = false,
}: {
  adminView?: boolean
}) {
  const { id } = useParams()
  const [ticket, setTicket] = useState<null | Ticket>(null)
  const [now, setNow] = useState(new Date())
  const [customerCash, setCustomerCash] = useState(0)
  const { ticketsService } = useService()

  useEffect(() => {
    if (id) {
      ;(async () => {
        setTicket(await ticketsService.getTicketById(id))
      })()
    }
  }, [id])

  useEffect(() => {
    let intId: NodeJS.Timeout
    if (ticket && !ticket.paymentTime) {
      intId = setInterval(() => {
        setNow(new Date())
      }, 1000)
    }

    return () => clearInterval(intId)
  }, [ticket])

  const totalTime = useMemo(() => {
    if (!ticket || !ticket?.entryTime) return 0
    return countHoursBetween(
      new Date(ticket.entryTime),
      ticket?.exitTime ? new Date(ticket.exitTime) : now,
    )
  }, [ticket, now])

  const totalFee = useMemo(() => {
    return (
      totalTime *
      (ticket
        ? ticket.doesUserHaveActiveMembership
          ? 0
          : ticket.feePerHour
        : 0)
    )
  }, [totalTime])

  return (
    <div>
      <main className="">
        <Card className="">
          <CardHeader>
            <CardTitle>Ticket #{id}</CardTitle>
            <CardDescription>Guest Ticket</CardDescription>
          </CardHeader>
          <CardContent>
            {ticket && (
              <div className="flex flex-col space-y-10 md:flex-row md:gap-4 md:justify-between">
                <div className="flex grow">
                  <div className="space-y-4 grow">
                    <div className="">
                      <div className="font-bold tracking-tighter flex items-center gap-2">
                        <RectangleHorizontal /> <span>License Plate</span>
                      </div>
                      <div className="text-lg">{ticket.plateNumber} </div>
                    </div>
                    <div className="">
                      <div className="font-bold tracking-tighter flex items-center gap-2">
                        <ParkingCircle /> <span>Entry</span>
                      </div>
                      <div className="text-lg">
                        {ticket.entryTime
                          ? format(
                              new Date(ticket.entryTime),
                              "dd MMM yyyy, hh:mm:ss",
                            )
                          : "Ticket needs admission"}
                      </div>
                    </div>
                    <div className="">
                      <div className="font-bold tracking-tighter  flex items-center gap-2">
                        <LogOut /> <span>Exit</span>
                      </div>
                      <div className="text-lg">
                        {format(
                          ticket?.exitTime ? new Date(ticket.exitTime) : now,
                          "dd MMM yyyy, hh:mm:ss",
                        )}
                      </div>
                    </div>
                    <div className="">
                      <div className="font-bold tracking-tighter  flex items-center gap-2">
                        <Timer /> <span>Total Time</span>
                      </div>
                      <div className="text-lg">{totalTime} Hours</div>
                    </div>
                    <div className="">
                      <div className="font-bold tracking-tighter  flex items-center gap-2">
                        <Car /> <span>Vehicle Type</span>
                      </div>
                      <div className="text-lg">
                        <span>{ticket.vehicleTypeName}</span>
                        {" - "}
                        <span className="text-sm">
                          {formRupiah(ticket.feePerHour)} / hour
                        </span>
                      </div>
                    </div>
                    <div className="">
                      <div className="font-bold tracking-tighter  flex items-center gap-2">
                        <UsersRound /> <span>Membership</span>
                      </div>
                      <div className="text-lg mt-2">
                        {ticket.doesUserHaveActiveMembership ? (
                          <CheckCircle2 className="text-green-600" />
                        ) : (
                          <XCircle className="text-red-600" />
                        )}
                      </div>
                    </div>
                    <div className="">
                      <div className="font-bold tracking-tighter  flex items-center gap-2">
                        <CircleDollarSign /> <span>Total Fee</span>
                      </div>
                      <div className="text-lg">
                        {formRupiah(
                          ticket.doesUserHaveActiveMembership ? 0 : totalFee,
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="flex justify-center">
                  {id ? (
                    <div>
                      <QRCode
                        value={`https://nex-park-scanner.netlify.app/${ticket.id}`}
                      />
                      <Dialog>
                        <DialogTrigger asChild>
                          <Button
                            variant="outline"
                            className="w-full block mt-4"
                          >
                            Preview Ticket
                          </Button>
                        </DialogTrigger>
                        <DialogContent>
                          <DialogHeader>
                            <DialogTitle>Ticket Preview</DialogTitle>
                            <DialogDescription>
                              This is the printed ticket.
                            </DialogDescription>
                          </DialogHeader>
                          <div>
                            {id ? (
                              <TicketPreview ticket={ticket} />
                            ) : (
                              <div>Loading...</div>
                            )}
                          </div>
                        </DialogContent>
                      </Dialog>
                    </div>
                  ) : (
                    <div>Loading...</div>
                  )}
                </div>
              </div>
            )}
          </CardContent>
          <CardFooter className="gap-4 justify-end">
            {!ticket?.entryTime && adminView && (
              <Button
                variant="outline"
                onClick={async () => {
                  if (ticket) {
                    const tick = await ticketsService.admitTicket(ticket.id)
                    setTicket({ ...ticket, entryTime: tick.entryTime })
                  }
                }}
              >
                Admit Ticket
              </Button>
            )}
            {adminView && (
              <Dialog>
                <DialogTrigger asChild>
                  <Button
                    disabled={!!ticket?.paymentTime || !ticket?.entryTime}
                  >
                    {ticket?.paymentTime ? "Ticket Paid" : "Confirm Payment"}
                  </Button>
                </DialogTrigger>
                <DialogContent>
                  <DialogHeader>
                    <DialogTitle>
                      {ticket?.paymentTime
                        ? "Ticket Paid."
                        : "Confirm Payment?"}
                    </DialogTitle>
                    <DialogDescription>
                      {ticket?.paymentTime
                        ? "This ticket has already been paid"
                        : "Make sure you have received payment from the customer."}
                    </DialogDescription>
                  </DialogHeader>
                  <div>
                    {ticket?.paymentTime ? (
                      <div className="flex justify-center">
                        <CheckCircle2 size={64} />
                      </div>
                    ) : (
                      <div className="flex flex-col gap-2">
                        <div className="flex items-center justify-between gap-2">
                          <div>Amount to Pay</div>
                          <div className="font-bold text-xl">
                            {formRupiah(totalFee)}
                          </div>
                        </div>
                        <div className="flex items-center justify-between gap-2">
                          <div>Cash</div>
                          <div className="text-xl">
                            <Input
                              placeholder="Cash"
                              value={customerCash}
                              type="number"
                              onChange={e =>
                                setCustomerCash(parseInt(e.target.value))
                              }
                            />
                          </div>
                        </div>
                        <div className="flex items-center justify-between gap-2">
                          <div>Change</div>
                          <div className="font-bold text-xl">
                            {formRupiah(customerCash - totalFee)}
                          </div>
                        </div>
                        <Button
                          className="mt-8"
                          onClick={async () => {
                            if (ticket) {
                              try {
                                const tick = await ticketsService.payTicket(
                                  ticket.id,
                                )
                                setTicket(prev =>
                                  prev
                                    ? { ...prev, paymentTime: tick.paymentTime }
                                    : prev,
                                )
                              } catch (error) {}
                            }
                          }}
                        >
                          Confirm
                        </Button>
                      </div>
                    )}
                  </div>
                </DialogContent>
              </Dialog>
            )}
          </CardFooter>
        </Card>
      </main>
    </div>
  )
}

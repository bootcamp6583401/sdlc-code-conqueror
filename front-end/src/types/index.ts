export type User = {
  id: number
  username: string
  email: string
  roles: string[]
  accessToken: string
  tokenType: string
}

export type Ticket = {
  id: string
  plateNumber: string
  entryTime: string | null
  exitTime: null | string
  vehicleTypeName: string
  feePerHour: number
  paymentTime: null | string
  totalTime: null | number
  onMembership: boolean | null
  totalFee: null | number
  doesUserHaveActiveMembership: boolean
}

export type VehicleType = {
  id: number
  name: string
  feePerHour: number
}

export type Role = {
  id: number
  name: string
}

export type IUser = {
  id: number
  username: string
  email: string
  roles: Role[]
  ticket: Ticket[]
  membership: null | Membership
  profileImageUrl: string | null
  phone: string | null
}

export type Membership = {
  id: number
  start: string
  end: string
  plateNumber: string
  vehicleTypeName: string
  vehicleTypeId: number
}

export enum TicketState {
  PENDING = "PENDING",
  ACTIVE = "ACTIVE",
  PAID = "PAID",
}

export type Message = {
  sender: string
  content: string
  senderId: number
  id: string
}

export type AdminNotification = {
  fromId: number
  initialMessage?: Message
}

export enum BoolFilter {
  YES = "YES",
  NO = "NO",
  ALL = "ALL",
}
export type Page<T> = {
  content: T[]
  pageable: {
    pageNumber: number
    pageSize: number
    sort: {
      empty: boolean
      sorted: boolean
      unsorted: boolean
    }
    offset: number
    unpaged: boolean
    paged: boolean
  }
  last: boolean
  totalPages: number
  totalElements: number
  size: number
  number: number
  sort: {
    empty: boolean
    sorted: boolean
    unsorted: boolean
  }
  numberOfElements: number
  first: boolean
  empty: boolean
}

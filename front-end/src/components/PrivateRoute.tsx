import { useAppSelector } from "@/app/hooks"
import { ServiceContextProvider } from "@/contexts/ServiceContext"
import { selectAuth, selectLoading } from "@/features/auth/authSlice"
import React, { ReactNode } from "react"
import { Navigate } from "react-router-dom"

export default function PrivateRoute({ children }: { children: ReactNode }) {
  const user = useAppSelector(selectAuth)
  const authLoading = useAppSelector(selectLoading)
  return authLoading ? (
    <h1>Loading...</h1>
  ) : user ? (
    <ServiceContextProvider user={user}>{children}</ServiceContextProvider>
  ) : (
    <Navigate to="/auth/login" />
  )
}

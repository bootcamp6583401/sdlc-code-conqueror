import { cn } from "@/lib/utils"
import { Message as IMessage } from "@/types"

export default function Message({
  message,
  isSent = false,
}: {
  message: IMessage
  isSent?: boolean
}) {
  return (
    <div
      className={cn(
        "rounded-md p-2 min-w-24",
        isSent ? "border border-border" : "ml-auto bg-primary text-black ",
      )}
    >
      <div className="font-semibold text-xs">{message.sender}</div>
      <p>{message.content}</p>
    </div>
  )
}

import { cn } from "@/lib/utils"
import React from "react"
import { NavLink } from "react-router-dom"
import symbol from "@/assets/icons/nex-park-symbol.svg"

export default function ActiveLink({
  text,
  href,
}: {
  text: string
  href: string
}) {
  return (
    <NavLink
      to={href}
      className={({ isActive }) =>
        cn([isActive ? "text-primary" : "active", "pl-6 block"])
      }
    >
      {({ isActive }) => (
        <div className="flex items-center gap-0.5 relative">
          {isActive && (
            <img
              src={symbol}
              className="w-3 absolute -left-4 top-1/2 -translate-y-1/2"
            />
          )}
          <span>{text}</span>
        </div>
      )}
    </NavLink>
  )
}

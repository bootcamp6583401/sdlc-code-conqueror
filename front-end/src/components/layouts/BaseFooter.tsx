import React from "react"
import Social from "@/components/ui/social"
import { url } from "inspector"
import twitter from "@/assets/icons/mdi_twitter.png"
import instagram from "@/assets/icons/ri_instagram-fill.png"
import tiktok from "@/assets/icons/ic_baseline-tiktok.png"
import logoDark from "@/assets/icons/nex-park-logo-dark.svg"

export default function BaseFooter() {
  return (
    <footer className="bg-primary text-primary-foreground w-full py-8 grid">
      <div className="space-y-10 mx-auto container lg:flex justify-between  items-center lg:space-y-0">
        <div className="space-y-[32px]">
          <div className="text-xl lg:text-3xl font-bold">
            <img src={logoDark} className="w-48" />
          </div>
          <div className="lg:text-xl max-w-[506px] font-semibold">
            Scientia Business Park Tower 2, Jl. Boulevard Raya Gading Serpong,
            Curug Sangereng, Kelapa Dua, Tangerang Regency, Banten 15810
          </div>
        </div>
        <div className="text-sm lg:text-[20px] space-y-[40px] sm:space-y-0 lg:flex items-center lg:space-x-[40px]">
          <div className="font-semibold  flex-col space-y-[20px] grid">
            <h4>About Us</h4>
            <h4>Contact Us</h4>
            <h4>Terms of Service</h4>
            <h4>Privacy Policy</h4>
          </div>
          <div className="font-semibold  flex-col space-y-[20px]">
            <h4>FAQs</h4>
            <h4>Help Center</h4>
            <h4>Careers</h4>
            <h4>Feedback</h4>
          </div>
          <div className="font-semibold  flex-col space-y-[20px]">
            <h4 className="font-bold">Follow us on :</h4>
            <div className="space-y-[20px]">
              <Social iconUrl={twitter} desc={"@nexpark"} />
              <Social iconUrl={instagram} desc={"@nexpark"} />
              <Social iconUrl={tiktok} desc={"@nexpark"} />
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

import React from "react"
import { Outlet } from "react-router-dom"
import { Button } from "../ui/button"
import BaseHeader from "./BaseHeader"
import BaseFooter from "./BaseFooter"
import bgImage from "@/assets/images/auth-hero.jpg"

export default function AuthLayout() {
  return (
    <div className="bg-secondary min-h-screen flex flex-col">
      <BaseHeader />
      <div className="flex flex-col lg:flex-row items-center">
        <div className="w-full py-24 lg:py-0 lg:w-1/2 order-last ">
          <Outlet />
        </div>
        <div className="lg:w-1/2 lg:order-last">
          <div
            className="bg-black bg-hero order-first lg:order-last h-[400px] lg:h-[960px] bg-center bg-no-repeat flex items-center justify-center md:px-8"
            style={{ backgroundImage: `url(${bgImage})` }}
          >
            <h1 className=" text-[50px] lg:text-[128px] font-bold text-primary text-center md:text-left max-w-[1024px] leading-tight">
              Nex-Level Paperless Parking
            </h1>
          </div>
        </div>
      </div>
      <BaseFooter />
    </div>
  )
}

import React from "react"
import { Outlet } from "react-router-dom"
import AdminHeader from "./AdminHeader"
import UserHeader from "./UserHeader"
import { CircleUserRound, Ticket, User } from "lucide-react"
import { useAppSelector } from "@/app/hooks"
import { selectAuth } from "@/features/auth/authSlice"

export default function UserLayout() {
  const user = useAppSelector(selectAuth)
  return (
    <div>
      <UserHeader />
      <div className="flex ">
        {/* <aside className="p-4 min-w-[300px]">
          <div className="rounded-md border border-border p-4">
            <header className="border-b border-border flex gap-4 items-center pb-4">
              <CircleUserRound size={40} />
              <div className="text-3xl font-bold">{user?.username}</div>
            </header>
            <ul className="flex flex-col gap-4 mt-4">
              <li className="flex items-center gap-4">
                <Ticket /> <span className="text-xl">Invoice History</span>
              </li>
              <li className="flex items-center gap-4">
                <Ticket /> <span className="text-xl">Invoice History</span>
              </li>
              <li className="flex items-center gap-4">
                <Ticket /> <span className="text-xl">Invoice History</span>
              </li>
            </ul>
          </div>
        </aside> */}

        {/* <div className="grow mt-4 mr-4 max-w-full overflow-auto"> */}
        <div className="py-8 max-w-[1024px] mx-auto container overflow-auto">
          <Outlet />
        </div>
      </div>
    </div>
  )
}

import React from "react"
import { Outlet } from "react-router-dom"
import AdminHeader from "./AdminHeader"

export default function AdminLayout() {
  return (
    <div className="min-h-screen flex flex-col">
      <AdminHeader />
      <div className="flex grow">
        {/* <aside className="border-r border-border p-4"></aside> */}
        <div className="py-8 max-w-[1024px] mx-auto container">
          <Outlet />
        </div>
      </div>
    </div>
  )
}

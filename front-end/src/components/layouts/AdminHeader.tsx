import React, { useEffect, useMemo, useState } from "react"
import { Link, NavLink } from "react-router-dom"
import logo from "@/assets/icons/nex-park-logo.svg"
import ActiveLink from "../ActiveLink"
import { useAppDispatch } from "@/app/hooks"
import { logout } from "@/features/auth/authSlice"
import { Button } from "../ui/button"
import { useAbly } from "ably/react"
import icon from "@/assets/icons/nex-park-icon.svg"

import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover"
import { Bell, X } from "lucide-react"
import { Types } from "ably"
import { AdminNotification } from "@/types"
import { Menu } from "lucide-react"

export default function AdminHeader() {
  const ablyClient = useAbly()

  const dispatch = useAppDispatch()

  const adminNotificationChannel = useMemo(() => {
    return ablyClient.channels.get(`admin-notification`)
  }, [ablyClient.channels])

  const [adminNotifications, setAdminNotifications] = useState<
    AdminNotification[]
  >([])

  useEffect(() => {
    const notifListener = (ablyMessage: Types.Message) => {
      const notif = ablyMessage.data as AdminNotification
      setAdminNotifications(prev => [...prev, notif])
    }

    adminNotificationChannel.subscribe(notifListener)

    return () => {
      adminNotificationChannel.unsubscribe(notifListener)
    }
  }, [adminNotificationChannel])

  const [isOpen, setIsOpen] = useState(false)
  const toggleNavbar = () => {
    setIsOpen(!isOpen)
  }
  return (
    <header className="p-4 bg-secondary text-secondary-foreground drop-shadow-shadow-pri z-[20] sticky top-0">
      <nav className="flex items-center gap-5">
        <div className="order-2 md:order-1  font-bold text-4xl">
          <Link to="/">
            <img src={logo} className="hidden w-48 md:flex" />
            <img src={icon} alt="icon" className="w-[30px] md:hidden" />
          </Link>
        </div>
        <div className="md:order-2">
          <ul className="hidden md:flex gap-2 text-lg">
            <li>
              <ActiveLink href="/admin/dashboard" text="Dashboard" />
            </li>
            <li>
              <ActiveLink href="/admin/users" text="Users" />
            </li>
            <li>
              <ActiveLink href="/admin/tickets" text="Tickets" />
            </li>
            <li>
              <ActiveLink href="/admin/scan" text="Scan" />
            </li>
            <li>
              <ActiveLink href="/admin/vehicle-types" text="Vehicles" />
            </li>
          </ul>
          <div className="md:hidden">
            <button onClick={toggleNavbar}>{isOpen ? <X /> : <Menu />}</button>
          </div>
        </div>
        <div className="order-3 ml-auto flex items-center gap-4">
          <Popover>
            <PopoverTrigger asChild>
              <Button size="icon" variant="ghost" className="relative">
                {adminNotifications.length > 0 && (
                  <div className="absolute bg-primary text-xs w-4 h-4 rounded-full text-primary-foreground top-0 right-0">
                    {adminNotifications.length}
                  </div>
                )}
                <Bell />
              </Button>
            </PopoverTrigger>
            <PopoverContent>
              <div className="flex flex-col gap-1">
                {adminNotifications.map((an, index) => {
                  return (
                    <Link
                      to={`/admin/support/${an.fromId}?${an.initialMessage ? `sender=${an.initialMessage.sender}&senderId=${an.initialMessage.senderId}&content=${an.initialMessage.content}&` : ""}`}
                      key={an.fromId + "" + index}
                      className="block hover:bg-zinc-200 p-2 rounded-md"
                    >
                      User with ID #{an.fromId} requests support!
                    </Link>
                  )
                })}
              </div>
            </PopoverContent>
          </Popover>
          <Button variant="outline" onClick={() => dispatch(logout())}>
            Logout
          </Button>
        </div>
      </nav>
      {isOpen && (
        <ul className="md:flex gap-2 text-lg mt-3">
          <li>
            <ActiveLink href="/admin/dashboard" text="Dashboard" />
          </li>
          <li>
            <ActiveLink href="/admin/users" text="Users" />
          </li>
          <li>
            <ActiveLink href="/admin/tickets" text="Tickets" />
          </li>
          <li>
            <ActiveLink href="/admin/scan" text="Scan" />
          </li>
          <li>
            <ActiveLink href="/admin/vehicle-types" text="Vehicles" />
          </li>
        </ul>
      )}
    </header>
  )
}

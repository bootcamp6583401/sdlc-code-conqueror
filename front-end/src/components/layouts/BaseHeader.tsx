import React from "react"
import { Button, buttonVariants } from "../ui/button"
import UrlProfile from "@/assets/icons/mingcute_user-4-fill.png"
import logo from "@/assets/icons/nex-park-logo.svg"
import { useAppSelector } from "@/app/hooks"
import { selectAuth, selectUserData } from "@/features/auth/authSlice"
import { Link } from "react-router-dom"
import { cn, isUserAdmin } from "@/lib/utils"
import { UserCircle2 } from "lucide-react"

export default function BaseHeader() {
  const user = useAppSelector(selectUserData)
  const auth = useAppSelector(selectAuth)
  return (
    <header className="z-10 p-4 bg-secondary text-secondary-foreground drop-shadow-shadow-pri sticky top-0 py-[26px] px-[20px] lg:px-[67px]">
      <nav className="flex gap-4 items-center justify-between">
        <div className="font-bold text-[40px]">
          <Link to="/">
            <img src={logo} className="w-40 lg:w-48" />
          </Link>
        </div>
        {user && auth ? (
          <Link to={isUserAdmin(user) ? "/admin/dashboard" : "/user/dashboard"}>
            <UserCircle2
              size={32}
              className="text-primary hover:text-yellow-300"
            />
          </Link>
        ) : (
          <Link to="/auth/register" className={cn(buttonVariants())}>
            Get Started
          </Link>
        )}
      </nav>
    </header>
  )
}

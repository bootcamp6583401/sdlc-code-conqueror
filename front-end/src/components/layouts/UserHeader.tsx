import { useAppDispatch } from "@/app/hooks"
import { logout } from "@/features/auth/authSlice"
import React, { useState } from "react"
import { Link } from "react-router-dom"
import logo from "@/assets/icons/nex-park-logo.svg"
import ActiveLink from "../ActiveLink"
import { Button } from "../ui/button"
import icon from "@/assets/icons/nex-park-icon.svg"
import { Menu, X } from "lucide-react"

export default function UserHeader() {
  const dispatch = useAppDispatch()
  const [isOpen, setIsOpen] = useState(false)
  const toggleNavbar = () => {
    setIsOpen(!isOpen)
  }
  return (
    <header className="p-4 bg-secondary text-secondary-foreground drop-shadow-shadow-pri z-[20] sticky top-0">
      <nav className="flex gap-4 items-center">
        <div className="font-bold text-4xl order-2 md:order-1">
          <Link to="/">
            <img src={logo} className="w-48 hidden md:flex" />
            <img src={icon} alt="icon" className="w-[30px] md:hidden" />
          </Link>
        </div>
        <div className="md:order-2">
          <ul className="hidden md:flex gap-2">
            <li>
              <ActiveLink href="/user/dashboard" text="Dashboard" />
            </li>
            <li>
              <ActiveLink href="/user/profile" text="Profile" />
            </li>
            <li>
              <ActiveLink href="/user/membership" text="Membership" />
            </li>
            <li>
              <ActiveLink href="/user/support" text="Support" />
            </li>
          </ul>
          <div className="md:hidden">
            <button onClick={toggleNavbar}>{isOpen ? <X /> : <Menu />}</button>
          </div>
        </div>
        <div className="ml-auto order-3">
          <Button variant="outline" onClick={() => dispatch(logout())}>
            Logout
          </Button>
        </div>
      </nav>
      {isOpen && (
        <ul className="mt-4">
          <li>
            <ActiveLink href="/user/dashboard" text="Dashboard" />
          </li>
          <li>
            <ActiveLink href="/user/profile" text="Profile" />
          </li>
          <li>
            <ActiveLink href="/user/membership" text="Membership" />
          </li>
          <li>
            <ActiveLink href="/user/support" text="Support" />
          </li>
        </ul>
      )}
    </header>
  )
}

import React, { useState, useEffect } from "react"
import { Input } from "./input"
import { IUser, Page, User } from "@/types"

const BASE_URL = "http://localhost:8080"

function useDebounce<T>(value: T, delay: number): T {
  const [debouncedValue, setDebouncedValue] = useState<T>(value)

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value)
    }, delay)

    return () => {
      clearTimeout(handler)
    }
  }, [value, delay])

  return debouncedValue
}

export default function Search({
  setUserPageData,
}: {
  setUserPageData: React.Dispatch<React.SetStateAction<Page<IUser> | null>>
}): JSX.Element {
  const [searchTerm, setSearchTerm] = useState<string>("")
  const debouncedSearchTerm = useDebounce<string>(searchTerm, 500)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [error, setError] = useState<string | null>(null)

  useEffect(() => {
    const fetchSearchResults = async () => {
      if (debouncedSearchTerm) {
        setIsLoading(true)
        try {
          const user = JSON.parse(localStorage.getItem("user")!) as User
          const token = user.accessToken
          const response = await fetch(
            `${BASE_URL}/user/search?keyword=${debouncedSearchTerm}`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            },
          )
          if (response.ok) {
            const data = (await response.json()) as Page<IUser>
            setUserPageData(data)
            setError(null)
          } else {
            setError("Error fetching search results")
          }
        } catch (error) {
          setError("Error fetching search results")
        } finally {
          setIsLoading(false)
        }
      } else {
        setUserPageData(null)
      }
    }

    fetchSearchResults()
  }, [debouncedSearchTerm])

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value)
  }

  return (
    <Input
      className="lg:w-[500px]"
      value={searchTerm}
      onChange={handleInputChange}
      placeholder="Search User"
    />
  )
}

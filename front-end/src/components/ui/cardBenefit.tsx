import React from "react"

interface cardBenefitProps {
  imgUrl: string
  title: string
  description: string
}

const cardBenefit: React.FC<cardBenefitProps> = props => {
  return (
    <div className="my-5 md:my-6 bg-primary text-black flex flex-col lg:flex-col md:flex-row py-[50px] px-[30px]  items-center rounded-lg">
      <img src={props.imgUrl} alt={props.title} />
      <div className="lg:text-center md:text-start md:pl-5 lg:pl-0 lg:mt-auto">
        <h2 className="text-[30px] font-bold">{props.title}</h2>
        <p>{props.description}</p>
      </div>
    </div>
  )
}

export default cardBenefit

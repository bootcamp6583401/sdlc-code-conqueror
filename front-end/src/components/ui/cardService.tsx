import React from "react"

interface cardServiceProps {
  title: string
  iconUrl: string
  description: string
}

const cardService: React.FC<cardServiceProps> = props => {
  return (
    <div className="flex flex-col items-center text-center border rounded-md py-[50px] px-[20px] space-y-[20px] justify-center">
      <h1 className="text-[30px] font-bold">{props.title}</h1>
      <img src={props.iconUrl} alt="" />
      <p>{props.description}</p>
    </div>
  )
}

export default cardService

import React from 'react'

interface socialProps{
    iconUrl : string,
    desc : string
}

const social : React.FC<socialProps> = (props) => {
  return (
    <div className='flex space-x-[10px] font-medium'>
        <img src={props.iconUrl} alt={props.desc} />
        <p>{props.desc}</p>
    </div>
  )
}

export default social;
import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardDescription,
  CardContent,
} from "./ui/card"
import { Separator } from "./ui/separator"
import QRCode from "react-qr-code"
import { Ticket } from "@/types"

export default function TicketPreview({ ticket }: { ticket: Ticket }) {
  return (
    <Card className="bg-white text-black">
      <CardHeader className="text-center">
        <CardTitle>NEX PARKING</CardTitle>
        <CardDescription>Parking Struct</CardDescription>
        <Separator className="bg-black" />
      </CardHeader>
      <CardContent>
        <div className="space-y-6">
          <div className="flex flex-col items-center">
            <div className="flex gap-2 text-sm">
              <div className="">TICKET NO</div>
              <div>:</div>
              <div className="">{ticket.id}</div>
            </div>
            <div className="flex gap-2 text-sm">
              <div className="">JENIS KENDARAAN</div>
              <div>:</div>
              <div className="">{ticket.vehicleTypeName}</div>
            </div>
            <div className="flex gap-2 text-sm">
              <div className="">ENTRY TIME</div>
              <div>:</div>
              <div className="">{ticket.entryTime || "PENDING"}</div>
            </div>
          </div>
          <div className="flex justify-center">
            <QRCode
              value={`https://nex-park-scanner.netlify.app/${ticket.id}`}
              size={150}
            />
          </div>
          <Separator className="bg-black" />
          <div className="text-center font-semibold tracking-tighter text-lg">
            Do not leave your tickets and valuables. THANK YOU
          </div>
        </div>
      </CardContent>
    </Card>
  )
}

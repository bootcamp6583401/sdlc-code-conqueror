import type { PayloadAction } from "@reduxjs/toolkit"
import { createAppSlice } from "../../app/createAppSlice"
import { IUser, User } from "@/types"
import axios from "@/lib/axios"
import { AxiosError } from "axios"
import usersService from "@/services/users"

export enum AuthStatus {
  REGISTER_SUCCESS = "REGISTER_SUCCESS",
  IDLE = "IDLE",
}

export interface AuthSliceState {
  loading: boolean
  user: User | null
  error: null | string
  status: AuthStatus
  userData: IUser | null
}

const initialState: AuthSliceState = {
  user: null,
  loading: true,
  status: AuthStatus.IDLE,
  error: null,
  userData: null,
}

export const authSlice = createAppSlice({
  name: "auth",
  initialState,
  reducers: create => ({
    clear: create.reducer(state => {
      state.error = null
    }),
    init: create.reducer(state => {
      const localUser = localStorage.getItem("user")
      if (localUser) {
        const user = JSON.parse(localUser) as User
        state.user = user
      }
      state.loading = false
    }),
    updateUserData: create.reducer((state, action: PayloadAction<IUser>) => {
      state.userData = action.payload
    }),
    initComplete: create.asyncThunk(
      async () => {
        const localUser = localStorage.getItem("user")
        if (localUser) {
          const user = JSON.parse(localUser) as User
          const service = usersService(user.accessToken)
          const userData = await service.getUserById(user.id)

          return { user, userData }
        }

        return { user: null, userData: null }
      },
      {
        pending: state => {
          state.loading = true
        },
        fulfilled: (state, action) => {
          state.user = action.payload.user
          state.userData = action.payload.userData
          state.loading = false
        },
        rejected: state => {
          state.loading = false
        },
      },
    ),
    logout: create.reducer(state => {
      state.user = null
      state.userData = null
      localStorage.removeItem("user")
    }),
    login: create.asyncThunk(
      async (
        {
          username,
          password,
        }: {
          username: string
          password: string
        },

        { rejectWithValue },
      ) => {
        try {
          const response = await axios.post("/auth/signin", {
            username,
            password,
          })
          localStorage.setItem("user", JSON.stringify(response.data))
          const authUser = response.data as User

          const service = usersService(authUser.accessToken)
          const userData = await service.getUserById(authUser.id)
          return { authUser, userData }
        } catch (error) {
          return rejectWithValue("Incorrect credentials.")
        }
      },
      {
        pending: state => {
          state.loading = true
        },
        fulfilled: (state, action) => {
          if (action.payload) {
            state.user = action.payload.authUser
            state.userData = action.payload.userData
          }
          state.loading = false
        },
        rejected: (state, action) => {
          state.loading = false
          state.error = action.payload as string
        },
      },
    ),

    loginGoogle: create.asyncThunk(
      async ({ token }: { token: string }) => {
        const response = await axios.post(
          "/api/v1/auth/google",
          {},
          { headers: { Token: token } },
        )
        localStorage.setItem("user", JSON.stringify(response.data))
        const authUser = response.data as User
        const service = usersService(authUser.accessToken)

        const userData = await service.getUserById(authUser.id)
        return { authUser, userData }
      },
      {
        pending: state => {
          state.loading = true
        },
        fulfilled: (state, action) => {
          state.user = action.payload.authUser
          state.userData = action.payload.userData
          state.loading = false
        },
        rejected: state => {
          state.loading = false
        },
      },
    ),

    register: create.asyncThunk(
      async (
        {
          username,
          password,
          email,
        }: {
          username: string
          password: string
          email: string
        },
        { rejectWithValue },
      ) => {
        try {
          const res = await axios.post("/auth/signup", {
            username,
            password,
            email,
          })
          return res.data as { message: string }
        } catch (error: any) {
          const err: AxiosError = error
          const data = err.response!.data as { message: string }

          return rejectWithValue(data.message)
        }
      },
      {
        pending: state => {
          state.loading = true
        },
        fulfilled: (state, action) => {
          state.loading = false
          state.status = AuthStatus.REGISTER_SUCCESS
        },
        rejected: (state, action) => {
          state.error = action.payload as string
          state.loading = false
        },
      },
    ),
  }),
  selectors: {
    selectAuth: state => state.user,
    selectLoading: state => state.loading,
    selectError: state => state.error,
    selectAuthStatus: state => state.status,
    selectUserData: state => state.userData,
  },
})

export const {
  login,
  logout,
  init,
  register,
  clear,
  initComplete,
  updateUserData,
  loginGoogle,
} = authSlice.actions

export const {
  selectAuth,
  selectLoading,
  selectError,
  selectAuthStatus,
  selectUserData,
} = authSlice.selectors

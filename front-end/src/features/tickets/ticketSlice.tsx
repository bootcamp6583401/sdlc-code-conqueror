import { createAppSlice } from "../../app/createAppSlice"
import { Ticket } from "@/types"
import { getUserTickets } from "@/services/tickets"

export interface TicketSliceState {
  loading: boolean
  userTickets: Ticket[]
  error: null | string
}

const initialState: TicketSliceState = {
  userTickets: [],
  loading: false,
  error: null,
}

export const ticketSlice = createAppSlice({
  name: "tickets",
  initialState,
  reducers: create => ({
    getTickets: create.asyncThunk(
      async (userId: number) => {
        const tickets = await getUserTickets(userId)
        return tickets
      },
      {
        pending: state => {
          state.loading = true
        },
        fulfilled: (state, action) => {
          state.userTickets = action.payload.content
          state.loading = false
        },
        rejected: state => {
          state.loading = false
        },
      },
    ),
  }),
  selectors: {
    selectUserTickets: state => state.userTickets,
    selectTicketLoading: state => state.loading,
    selectTicketError: state => state.error,
  },
})

// Action creators are generated for each case reducer function.
export const { getTickets } = ticketSlice.actions

// Selectors returned by `slice.selectors` take the root state as their first argument.
export const { selectTicketError, selectTicketLoading, selectUserTickets } =
  ticketSlice.selectors

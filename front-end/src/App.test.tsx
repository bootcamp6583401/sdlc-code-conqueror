import { screen, waitFor } from "@testing-library/react"
import App from "./App"
import { renderWithProviders } from "./utils/test-utils"
import Home from "./pages/Home"
import AppMain from "./AppMain"

test("App should have correct initial render", () => {
  renderWithProviders(<AppMain />)

  // The app should be rendered correctly
  expect(screen.getByText(/Discover Modern/i)).toBeInTheDocument()
})

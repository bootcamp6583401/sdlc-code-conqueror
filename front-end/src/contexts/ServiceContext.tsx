import membershipService from "@/services/memberships"
import ticketsService from "@/services/tickets"
import usersService from "@/services/users"
import vehicleTypesService from "@/services/vehicleTypes"
import { IUser, User } from "@/types"
import React, {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react"

export interface ServiceContextData {
  membershipService: ReturnType<typeof membershipService>
  ticketsService: ReturnType<typeof ticketsService>
  usersService: ReturnType<typeof usersService>
  vehicleTypesService: ReturnType<typeof vehicleTypesService>
}

export const ServiceContext = createContext<ServiceContextData>({
  membershipService: membershipService(""),
  ticketsService: ticketsService(""),
  vehicleTypesService: vehicleTypesService(""),
  usersService: usersService(""),
})

export const ServiceContextProvider: React.FC<{
  children: ReactNode
  user: User
}> = ({ children, user }) => {
  return (
    <ServiceContext.Provider
      value={{
        membershipService: membershipService(user.accessToken),
        ticketsService: ticketsService(user.accessToken),
        vehicleTypesService: vehicleTypesService(user.accessToken),
        usersService: usersService(user.accessToken),
      }}
    >
      {children}
    </ServiceContext.Provider>
  )
}

export const useService = (): ServiceContextData => {
  const context = useContext(ServiceContext)
  if (!context) {
    throw new Error("useService must be used within a ServiceContextProvider")
  }
  return context
}

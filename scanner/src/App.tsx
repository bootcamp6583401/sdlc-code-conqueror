import "./App.css";
import { Realtime } from "ably";
import { AblyProvider } from "ably/react";
import Scanner from "./Scanner";
import { RouterProvider, createBrowserRouter } from "react-router-dom";

const client = new Realtime.Promise({
  key: import.meta.env.VITE_ABLY_API_KEY,
  clientId: "nex-park-scanner",
});

const router = createBrowserRouter([
  {
    path: "/:id",
    element: <Scanner />,
  },
  {
    path: "/:id/admit",
    element: <Scanner />,
  },
]);

function App() {
  return (
    <>
      <div>
        <AblyProvider client={client}>
          <RouterProvider router={router} />
        </AblyProvider>
      </div>
    </>
  );
}

export default App;

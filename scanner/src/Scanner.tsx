import { useEffect, useMemo } from "react";
import { useAbly } from "ably/react";
// import { Types } from "ably";
import { useParams } from "react-router-dom";

export default function Scanner() {
  const { id } = useParams();
  const ablyClient = useAbly();

  const channel = useMemo(() => {
    return ablyClient.channels.get(`scanner`);
  }, [ablyClient.channels]);

  //   useEffect(() => {
  //     const listener = (ablyMessage: Types.Message) => {
  //       const message = ablyMessage.data as string;
  //     };
  //     channel.subscribe(listener);

  //     return () => {
  //       channel.unsubscribe(listener);
  //     };
  //   }, [channel]);

  useEffect(() => {
    if (id) {
      channel.publish("scan", id);
    }
  }, [id, channel]);

  return <div>Scanner</div>;
}

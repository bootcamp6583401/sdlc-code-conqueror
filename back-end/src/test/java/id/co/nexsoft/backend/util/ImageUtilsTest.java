package id.co.nexsoft.backend.util;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.UnsupportedEncodingException;

import org.junit.jupiter.api.Test;

class ImageUtilsTest {
    /**
     * Method under test: {@link ImageUtils#compressImage(byte[])}
     */
    @Test
    void testCompressImage() throws UnsupportedEncodingException {
        // Arrange, Act and Assert
        assertArrayEquals(new byte[]{'x', -38, 's', -116, 'p', 4, 'C', 0, '\n', -100, 2, 'e'},
                ImageUtils.compressImage("AXAXAXAX".getBytes("UTF-8")));
    }

    /**
     * Method under test: {@link ImageUtils#decompressImage(byte[])}
     */
    @Test
    void testDecompressImage() throws UnsupportedEncodingException {
        // Arrange, Act and Assert
        assertEquals(0, ImageUtils.decompressImage("AXAXAXAX".getBytes("UTF-8")).length);
    }
}

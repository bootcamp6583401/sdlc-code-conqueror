package id.co.nexsoft.backend.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.co.nexsoft.backend.model.VehicleType;
import id.co.nexsoft.backend.repository.VehicleTypeRepository;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {VehicleTypeServiceImpl.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class VehicleTypeServiceImplTest {
    @MockBean
    private VehicleTypeRepository vehicleTypeRepository;

    @Autowired
    private VehicleTypeServiceImpl vehicleTypeServiceImpl;

    /**
     * Method under test: {@link VehicleTypeServiceImpl#getVehicleTypes()}
     */
    @Test
    void testGetVehicleTypes() {
        // Arrange
        ArrayList<VehicleType> vehicleTypeList = new ArrayList<>();
        when(vehicleTypeRepository.findAll()).thenReturn(vehicleTypeList);

        // Act
        Iterable<VehicleType> actualVehicleTypes = vehicleTypeServiceImpl.getVehicleTypes();

        // Assert
        verify(vehicleTypeRepository).findAll();
        assertTrue(((List<VehicleType>) actualVehicleTypes).isEmpty());
        assertSame(vehicleTypeList, actualVehicleTypes);
    }

    /**
     * Method under test: {@link VehicleTypeServiceImpl#getVehicleTypeById(int)}
     */
    @Test
    void testGetVehicleTypeById() {
        // Arrange
        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());
        when(vehicleTypeRepository.findById(anyInt())).thenReturn(vehicleType);

        // Act
        VehicleType actualVehicleTypeById = vehicleTypeServiceImpl.getVehicleTypeById(1);

        // Assert
        verify(vehicleTypeRepository).findById(eq(1));
        assertSame(vehicleType, actualVehicleTypeById);
    }

    /**
     * Method under test:
     * {@link VehicleTypeServiceImpl#createVehicleType(VehicleType)}
     */
    @Test
    void testCreateVehicleType() {
        // Arrange
        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());
        when(vehicleTypeRepository.save(Mockito.<VehicleType>any())).thenReturn(vehicleType);

        VehicleType vehicleType2 = new VehicleType();
        vehicleType2.setFeePerHour(1);
        vehicleType2.setId(1);
        vehicleType2.setName("Name");
        vehicleType2.setTicket(new ArrayList<>());

        // Act
        VehicleType actualCreateVehicleTypeResult = vehicleTypeServiceImpl.createVehicleType(vehicleType2);

        // Assert
        verify(vehicleTypeRepository).save(Mockito.<VehicleType>any());
        assertSame(vehicleType, actualCreateVehicleTypeResult);
    }

    /**
     * Method under test: {@link VehicleTypeServiceImpl#deleteVehicleTypeById(int)}
     */
    @Test
    void testDeleteVehicleTypeById() {
        // Arrange
        doNothing().when(vehicleTypeRepository).deleteById(anyInt());

        // Act
        vehicleTypeServiceImpl.deleteVehicleTypeById(1);

        // Assert that nothing has changed
        verify(vehicleTypeRepository).deleteById(eq(1));
        assertTrue(((List<VehicleType>) vehicleTypeServiceImpl.getVehicleTypes()).isEmpty());
    }

    /**
     * Method under test:
     * {@link VehicleTypeServiceImpl#updateVehicleType(VehicleType, int)}
     */
    @Test
    void testUpdateVehicleType() {
        // Arrange
        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());

        VehicleType vehicleType2 = new VehicleType();
        vehicleType2.setFeePerHour(1);
        vehicleType2.setId(1);
        vehicleType2.setName("Name");
        vehicleType2.setTicket(new ArrayList<>());
        when(vehicleTypeRepository.save(Mockito.<VehicleType>any())).thenReturn(vehicleType2);
        when(vehicleTypeRepository.findById(anyInt())).thenReturn(vehicleType);

        VehicleType vehicleType3 = new VehicleType();
        vehicleType3.setFeePerHour(1);
        vehicleType3.setId(1);
        vehicleType3.setName("Name");
        vehicleType3.setTicket(new ArrayList<>());

        // Act
        ResponseEntity<Object> actualUpdateVehicleTypeResult = vehicleTypeServiceImpl.updateVehicleType(vehicleType3, 1);

        // Assert
        verify(vehicleTypeRepository).findById(eq(1));
        verify(vehicleTypeRepository).save(Mockito.<VehicleType>any());
        assertNull(actualUpdateVehicleTypeResult.getBody());
        assertEquals(1, vehicleType3.getId().intValue());
        assertEquals(204, actualUpdateVehicleTypeResult.getStatusCodeValue());
        assertTrue(actualUpdateVehicleTypeResult.getHeaders().isEmpty());
    }
}

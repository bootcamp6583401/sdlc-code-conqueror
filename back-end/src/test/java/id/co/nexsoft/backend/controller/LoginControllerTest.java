package id.co.nexsoft.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class LoginControllerTest {
    /**
     * Method under test: {@link LoginController#getLoginPage()}
     */
    @Test
    void testGetLoginPage() {

        // Arrange, Act and Assert
        assertEquals("login", (new LoginController()).getLoginPage());
    }
}

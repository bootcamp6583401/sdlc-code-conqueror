package id.co.nexsoft.backend.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.co.nexsoft.backend.model.ERole;
import id.co.nexsoft.backend.model.Membership;
import id.co.nexsoft.backend.model.Source;
import id.co.nexsoft.backend.model.Ticket;
import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.repository.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {UserServiceImpl.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class UserServiceImplTest {
    @MockBean
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userServiceImpl;

    /**
     * Method under test: {@link UserServiceImpl#getUsers(int)}
     */
    @Test
    void testGetUsers() {
        // Arrange
        PageImpl<User> pageImpl = new PageImpl<>(new ArrayList<>());
        when(userRepository.findAll(Mockito.<Pageable>any())).thenReturn(pageImpl);

        // Act
        Page<User> actualUsers = userServiceImpl.getUsers(10);

        // Assert
        verify(userRepository).findAll(Mockito.<Pageable>any());
        assertTrue(actualUsers.toList().isEmpty());
        assertSame(pageImpl, actualUsers);
    }

    /**
     * Method under test: {@link UserServiceImpl#getAllUsers()}
     */
    @Test
    void testGetAllUsers() {
        // Arrange
        ArrayList<User> userList = new ArrayList<>();
        when(userRepository.findAll()).thenReturn(userList);

        // Act
        Iterable<User> actualAllUsers = userServiceImpl.getAllUsers();

        // Assert
        verify(userRepository).findAll();
        assertTrue(((List<User>) actualAllUsers).isEmpty());
        assertSame(userList, actualAllUsers);
    }

    /**
     * Method under test: {@link UserServiceImpl#getUserById(int)}
     */
    @Test
    void testGetUserById() {
        // Arrange
        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(new Membership());
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user2);

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership2);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");
        when(userRepository.findById(anyInt())).thenReturn(user3);

        // Act
        User actualUserById = userServiceImpl.getUserById(1);

        // Assert
        verify(userRepository).findById(eq(1));
        assertSame(user3, actualUserById);
    }

    /**
     * Method under test: {@link UserServiceImpl#findByEmail(String)}
     */
    @Test
    void testFindByEmail() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user2);
        when(userRepository.findByEmail(Mockito.<String>any())).thenReturn(ofResult);

        // Act
        Optional<User> actualFindByEmailResult = userServiceImpl.findByEmail("jane.doe@example.org");

        // Assert
        verify(userRepository).findByEmail(eq("jane.doe@example.org"));
        assertTrue(actualFindByEmailResult.isPresent());
        assertSame(ofResult, actualFindByEmailResult);
    }

    /**
     * Method under test: {@link UserServiceImpl#save(User)}
     */
    @Test
    void testSave() {
        // Arrange
        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(new Membership());
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        ArrayList<Ticket> ticket = new ArrayList<>();
        user.setTicket(ticket);
        user.setUsername("janedoe");

        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user2);

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership2);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");
        when(userRepository.save(Mockito.<User>any())).thenReturn(user3);

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(new User());

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership3);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(user4);

        User user5 = new User();
        user5.setEmail("jane.doe@example.org");
        user5.setId(1);
        user5.setMembership(membership4);
        user5.setPassword("iloveyou");
        user5.setProfileImageUrl("https://example.org/example");
        user5.setRole(ERole.ROLE_USER);
        user5.setRoles(new HashSet<>());
        user5.setSource(Source.GOOGLE);
        user5.setTicket(new ArrayList<>());
        user5.setUsername("janedoe");

        // Act
        userServiceImpl.save(user5);

        // Assert that nothing has changed
        verify(userRepository).save(Mockito.<User>any());
        assertEquals("https://example.org/example", user5.getProfileImageUrl());
        assertEquals("iloveyou", user5.getPassword());
        assertEquals("jane.doe@example.org", user5.getEmail());
        assertEquals("janedoe", user5.getUsername());
        assertEquals(1, user5.getId().intValue());
        assertEquals(ERole.ROLE_USER, user5.getRole());
        assertEquals(Source.GOOGLE, user5.getSource());
        assertTrue(((List<User>) userServiceImpl.getAllUsers()).isEmpty());
        assertTrue(user5.getRoles().isEmpty());
        assertEquals(ticket, user5.getTicket());
        assertSame(membership4, user5.getMembership());
    }

    /**
     * Method under test: {@link UserServiceImpl#createUser(User)}
     */
    @Test
    void testCreateUser() {
        // Arrange
        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(new Membership());
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user2);

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership2);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");
        when(userRepository.save(Mockito.<User>any())).thenReturn(user3);

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(new User());

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership3);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(user4);

        User user5 = new User();
        user5.setEmail("jane.doe@example.org");
        user5.setId(1);
        user5.setMembership(membership4);
        user5.setPassword("iloveyou");
        user5.setProfileImageUrl("https://example.org/example");
        user5.setRole(ERole.ROLE_USER);
        user5.setRoles(new HashSet<>());
        user5.setSource(Source.GOOGLE);
        user5.setTicket(new ArrayList<>());
        user5.setUsername("janedoe");

        // Act
        User actualCreateUserResult = userServiceImpl.createUser(user5);

        // Assert
        verify(userRepository).save(Mockito.<User>any());
        assertSame(user3, actualCreateUserResult);
    }

    /**
     * Method under test: {@link UserServiceImpl#deleteUserById(int)}
     */
    @Test
    void testDeleteUserById() {
        // Arrange
        doNothing().when(userRepository).deleteById(anyInt());

        // Act
        userServiceImpl.deleteUserById(1);

        // Assert that nothing has changed
        verify(userRepository).deleteById(eq(1));
        assertTrue(((List<User>) userServiceImpl.getAllUsers()).isEmpty());
    }

    /**
     * Method under test: {@link UserServiceImpl#updateUser(User, int)}
     */
    @Test
    void testUpdateUser() {
        // Arrange
        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(new Membership());
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user2);

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership2);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(new Membership());
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(user4);

        User user5 = new User();
        user5.setEmail("jane.doe@example.org");
        user5.setId(1);
        user5.setMembership(membership3);
        user5.setPassword("iloveyou");
        user5.setProfileImageUrl("https://example.org/example");
        user5.setRole(ERole.ROLE_USER);
        user5.setRoles(new HashSet<>());
        user5.setSource(Source.GOOGLE);
        user5.setTicket(new ArrayList<>());
        user5.setUsername("janedoe");

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(user5);

        User user6 = new User();
        user6.setEmail("jane.doe@example.org");
        user6.setId(1);
        user6.setMembership(membership4);
        user6.setPassword("iloveyou");
        user6.setProfileImageUrl("https://example.org/example");
        user6.setRole(ERole.ROLE_USER);
        user6.setRoles(new HashSet<>());
        user6.setSource(Source.GOOGLE);
        user6.setTicket(new ArrayList<>());
        user6.setUsername("janedoe");
        when(userRepository.save(Mockito.<User>any())).thenReturn(user6);
        when(userRepository.findById(anyInt())).thenReturn(user3);

        Membership membership5 = new Membership();
        membership5.setEnd(LocalDate.of(1970, 1, 1));
        membership5.setId(1);
        membership5.setStart(LocalDate.of(1970, 1, 1));
        membership5.setUser(new User());

        User user7 = new User();
        user7.setEmail("jane.doe@example.org");
        user7.setId(1);
        user7.setMembership(membership5);
        user7.setPassword("iloveyou");
        user7.setProfileImageUrl("https://example.org/example");
        user7.setRole(ERole.ROLE_USER);
        user7.setRoles(new HashSet<>());
        user7.setSource(Source.GOOGLE);
        user7.setTicket(new ArrayList<>());
        user7.setUsername("janedoe");

        Membership membership6 = new Membership();
        membership6.setEnd(LocalDate.of(1970, 1, 1));
        membership6.setId(1);
        membership6.setStart(LocalDate.of(1970, 1, 1));
        membership6.setUser(user7);

        User user8 = new User();
        user8.setEmail("jane.doe@example.org");
        user8.setId(1);
        user8.setMembership(membership6);
        user8.setPassword("iloveyou");
        user8.setProfileImageUrl("https://example.org/example");
        user8.setRole(ERole.ROLE_USER);
        user8.setRoles(new HashSet<>());
        user8.setSource(Source.GOOGLE);
        user8.setTicket(new ArrayList<>());
        user8.setUsername("janedoe");

        // Act
        ResponseEntity<Object> actualUpdateUserResult = userServiceImpl.updateUser(user8, 1);

        // Assert
        verify(userRepository).findById(eq(1));
        verify(userRepository).save(Mockito.<User>any());
        assertNull(actualUpdateUserResult.getBody());
        assertEquals(1, user8.getId().intValue());
        assertEquals(204, actualUpdateUserResult.getStatusCodeValue());
        assertTrue(actualUpdateUserResult.getHeaders().isEmpty());
    }

    /**
     * Method under test: {@link UserServiceImpl#searchUsers(String, int)}
     */
    @Test
    void testSearchUsers() {
        // Arrange
        PageImpl<User> pageImpl = new PageImpl<>(new ArrayList<>());
        when(userRepository.searchUsersByKeyword(Mockito.<String>any(), Mockito.<Pageable>any())).thenReturn(pageImpl);

        // Act
        Page<User> actualSearchUsersResult = userServiceImpl.searchUsers("Keyword", 10);

        // Assert
        verify(userRepository).searchUsersByKeyword(eq("keyword"), Mockito.<Pageable>any());
        assertTrue(actualSearchUsersResult.toList().isEmpty());
        assertSame(pageImpl, actualSearchUsersResult);
    }
}

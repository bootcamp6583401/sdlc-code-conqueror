package id.co.nexsoft.backend.payload.response;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class MessageResponseTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link MessageResponse#MessageResponse(String)}
     *   <li>{@link MessageResponse#setMessage(String)}
     *   <li>{@link MessageResponse#getMessage()}
     * </ul>
     */
    @Test
    void testGettersAndSetters() {
        // Arrange and Act
        MessageResponse actualMessageResponse = new MessageResponse("Not all who wander are lost");
        actualMessageResponse.setMessage("Not all who wander are lost");

        // Assert that nothing has changed
        assertEquals("Not all who wander are lost", actualMessageResponse.getMessage());
    }
}

package id.co.nexsoft.backend.security.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import id.co.nexsoft.backend.model.ERole;
import id.co.nexsoft.backend.model.Membership;
import id.co.nexsoft.backend.model.Source;
import id.co.nexsoft.backend.model.Ticket;
import id.co.nexsoft.backend.model.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.jupiter.api.Test;

class UserDetailsImplTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link UserDetailsImpl#build(User)}
     *   <li>{@link UserDetailsImpl#getPassword()}
     *   <li>{@link UserDetailsImpl#getEmail()}
     *   <li>{@link UserDetailsImpl#getUsername()}
     *   <li>{@link UserDetailsImpl#getId()}
     *   <li>{@link UserDetailsImpl#isAccountNonExpired()}
     *   <li>{@link UserDetailsImpl#isAccountNonLocked()}
     *   <li>{@link UserDetailsImpl#isCredentialsNonExpired()}
     *   <li>{@link UserDetailsImpl#isEnabled()}
     *   <li>{@link UserDetailsImpl#getAuthorities()}
     * </ul>
     */
    @Test
    void testBuild() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        ArrayList<Ticket> ticket = new ArrayList<>();
        user.setTicket(ticket);
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");
        UserDetailsImpl buildResult = UserDetailsImpl.build(user2);

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(new User());

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership3);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(user3);

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership4);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        // Act
        UserDetailsImpl actualBuildResult = buildResult.build(user4);

        // Assert
        assertEquals("iloveyou", actualBuildResult.getPassword());
        assertEquals("jane.doe@example.org", actualBuildResult.getEmail());
        assertEquals("janedoe", actualBuildResult.getUsername());
        assertEquals(1, actualBuildResult.getId().intValue());
        assertTrue(actualBuildResult.isAccountNonExpired());
        assertTrue(actualBuildResult.isAccountNonLocked());
        assertTrue(actualBuildResult.isCredentialsNonExpired());
        assertTrue(actualBuildResult.isEnabled());
        assertEquals(ticket, actualBuildResult.getAuthorities());
    }

    /**
     * Method under test: {@link UserDetailsImpl#equals(Object)}
     */
    @Test
    void testEquals() {
        // Arrange, Act and Assert
        assertNotEquals(UserDetailsImpl.build(new User()), null);
        assertNotEquals(UserDetailsImpl.build(new User()), "Different type to UserDetailsImpl");
    }

    /**
     * Method under test: {@link UserDetailsImpl#equals(Object)}
     */
    @Test
    void testEquals2() {
        // Arrange
        User user = mock(User.class);
        when(user.getId()).thenReturn(1);
        when(user.getEmail()).thenReturn("jane.doe@example.org");
        when(user.getPassword()).thenReturn("iloveyou");
        when(user.getUsername()).thenReturn("janedoe");
        when(user.getRoles()).thenReturn(new HashSet<>());
        UserDetailsImpl buildResult = UserDetailsImpl.build(user);

        // Act and Assert
        assertNotEquals(buildResult, UserDetailsImpl.build(new User()));
    }

    /**
     * Method under test: {@link UserDetailsImpl#equals(Object)}
     */
    @Test
    void testEqualsAndHashCode() {
        // Arrange
        UserDetailsImpl buildResult = UserDetailsImpl.build(new User());

        // Act and Assert
        assertEquals(buildResult, buildResult);
        int expectedHashCodeResult = buildResult.hashCode();
        assertEquals(expectedHashCodeResult, buildResult.hashCode());
    }

    /**
     * Method under test: {@link UserDetailsImpl#equals(Object)}
     */
    @Test
    void testEqualsAndHashCode2() {
        // Arrange
        UserDetailsImpl buildResult = UserDetailsImpl.build(new User());
        UserDetailsImpl buildResult2 = UserDetailsImpl.build(new User());

        // Act and Assert
        assertEquals(buildResult, buildResult2);
        int notExpectedHashCodeResult = buildResult.hashCode();
        assertNotEquals(notExpectedHashCodeResult, buildResult2.hashCode());
    }
}

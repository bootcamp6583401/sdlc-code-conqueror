package id.co.nexsoft.backend.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.co.nexsoft.backend.model.ERole;
import id.co.nexsoft.backend.model.Membership;
import id.co.nexsoft.backend.model.Source;
import id.co.nexsoft.backend.model.Ticket;
import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.model.VehicleType;
import id.co.nexsoft.backend.repository.TicketRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {TicketServiceImpl.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class TicketServiceImplTest {
    @MockBean
    private TicketRepository ticketRepository;

    @Autowired
    private TicketServiceImpl ticketServiceImpl;

    /**
     * Method under test: {@link TicketServiceImpl#getTicketPages(int)}
     */
    @Test
    void testGetTicketPages() {
        // Arrange
        PageImpl<Ticket> pageImpl = new PageImpl<>(new ArrayList<>());
        when(ticketRepository.findAll(Mockito.<Pageable>any())).thenReturn(pageImpl);

        // Act
        Page<Ticket> actualTicketPages = ticketServiceImpl.getTicketPages(10);

        // Assert
        verify(ticketRepository).findAll(Mockito.<Pageable>any());
        assertTrue(actualTicketPages.toList().isEmpty());
        assertSame(pageImpl, actualTicketPages);
    }

    /**
     * Method under test: {@link TicketServiceImpl#getTickets()}
     */
    @Test
    void testGetTickets() {
        // Arrange
        ArrayList<Ticket> ticketList = new ArrayList<>();
        when(ticketRepository.findAll()).thenReturn(ticketList);

        // Act
        Iterable<Ticket> actualTickets = ticketServiceImpl.getTickets();

        // Assert
        verify(ticketRepository).findAll();
        assertTrue(((List<Ticket>) actualTickets).isEmpty());
        assertSame(ticketList, actualTickets);
    }

    /**
     * Method under test: {@link TicketServiceImpl#getUserTickets(int, int)}
     */
    @Test
    void testGetUserTickets() {
        // Arrange
        PageImpl<Ticket> pageImpl = new PageImpl<>(new ArrayList<>());
        when(ticketRepository.findByUserId(anyInt(), Mockito.<Pageable>any())).thenReturn(pageImpl);

        // Act
        Iterable<Ticket> actualUserTickets = ticketServiceImpl.getUserTickets(1, 10);

        // Assert
        verify(ticketRepository).findByUserId(eq(1), Mockito.<Pageable>any());
        assertTrue(((PageImpl<Ticket>) actualUserTickets).toList().isEmpty());
        assertSame(pageImpl, actualUserTickets);
    }

    /**
     * Method under test: {@link TicketServiceImpl#getTicketById(String)}
     */
    @Test
    void testGetTicketById() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());

        Ticket ticket = new Ticket();
        ticket.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setId("42");
        ticket.setOnMembership(true);
        ticket.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setPlateNumber("42");
        ticket.setUser(user2);
        ticket.setVehicleType(vehicleType);
        when(ticketRepository.findById(Mockito.<String>any())).thenReturn(ticket);

        // Act
        Ticket actualTicketById = ticketServiceImpl.getTicketById("42");

        // Assert
        verify(ticketRepository).findById(eq("42"));
        assertSame(ticket, actualTicketById);
    }

    /**
     * Method under test: {@link TicketServiceImpl#createTicket(Ticket)}
     */
    @Test
    void testCreateTicket() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());

        Ticket ticket = new Ticket();
        ticket.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setId("42");
        ticket.setOnMembership(true);
        ticket.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setPlateNumber("42");
        ticket.setUser(user2);
        ticket.setVehicleType(vehicleType);
        when(ticketRepository.save(Mockito.<Ticket>any())).thenReturn(ticket);

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(new Membership());
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(user3);

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership3);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        VehicleType vehicleType2 = new VehicleType();
        vehicleType2.setFeePerHour(1);
        vehicleType2.setId(1);
        vehicleType2.setName("Name");
        vehicleType2.setTicket(new ArrayList<>());

        Ticket ticket2 = new Ticket();
        ticket2.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setId("42");
        ticket2.setOnMembership(true);
        ticket2.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setPlateNumber("42");
        ticket2.setUser(user4);
        ticket2.setVehicleType(vehicleType2);

        // Act
        Ticket actualCreateTicketResult = ticketServiceImpl.createTicket(ticket2);

        // Assert
        verify(ticketRepository).save(Mockito.<Ticket>any());
        assertSame(ticket, actualCreateTicketResult);
    }

    /**
     * Method under test: {@link TicketServiceImpl#deleteTicketById(String)}
     */
    @Test
    void testDeleteTicketById() {
        // Arrange
        doNothing().when(ticketRepository).deleteById(Mockito.<String>any());

        // Act
        ticketServiceImpl.deleteTicketById("42");

        // Assert that nothing has changed
        verify(ticketRepository).deleteById(eq("42"));
        assertTrue(((List<Ticket>) ticketServiceImpl.getTickets()).isEmpty());
    }

    /**
     * Method under test: {@link TicketServiceImpl#updateTicket(Ticket, String)}
     */
    @Test
    void testUpdateTicket() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());

        Ticket ticket = new Ticket();
        ticket.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setId("42");
        ticket.setOnMembership(true);
        ticket.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setPlateNumber("42");
        ticket.setUser(user2);
        ticket.setVehicleType(vehicleType);

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(new User());

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership3);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(user3);

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership4);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        VehicleType vehicleType2 = new VehicleType();
        vehicleType2.setFeePerHour(1);
        vehicleType2.setId(1);
        vehicleType2.setName("Name");
        vehicleType2.setTicket(new ArrayList<>());

        Ticket ticket2 = new Ticket();
        ticket2.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setId("42");
        ticket2.setOnMembership(true);
        ticket2.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setPlateNumber("42");
        ticket2.setUser(user4);
        ticket2.setVehicleType(vehicleType2);
        when(ticketRepository.save(Mockito.<Ticket>any())).thenReturn(ticket2);
        when(ticketRepository.findById(Mockito.<String>any())).thenReturn(ticket);

        User user5 = new User();
        user5.setEmail("jane.doe@example.org");
        user5.setId(1);
        user5.setMembership(new Membership());
        user5.setPassword("iloveyou");
        user5.setProfileImageUrl("https://example.org/example");
        user5.setRole(ERole.ROLE_USER);
        user5.setRoles(new HashSet<>());
        user5.setSource(Source.GOOGLE);
        user5.setTicket(new ArrayList<>());
        user5.setUsername("janedoe");

        Membership membership5 = new Membership();
        membership5.setEnd(LocalDate.of(1970, 1, 1));
        membership5.setId(1);
        membership5.setStart(LocalDate.of(1970, 1, 1));
        membership5.setUser(user5);

        User user6 = new User();
        user6.setEmail("jane.doe@example.org");
        user6.setId(1);
        user6.setMembership(membership5);
        user6.setPassword("iloveyou");
        user6.setProfileImageUrl("https://example.org/example");
        user6.setRole(ERole.ROLE_USER);
        user6.setRoles(new HashSet<>());
        user6.setSource(Source.GOOGLE);
        user6.setTicket(new ArrayList<>());
        user6.setUsername("janedoe");

        VehicleType vehicleType3 = new VehicleType();
        vehicleType3.setFeePerHour(1);
        vehicleType3.setId(1);
        vehicleType3.setName("Name");
        vehicleType3.setTicket(new ArrayList<>());

        Ticket ticket3 = new Ticket();
        ticket3.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket3.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket3.setId("42");
        ticket3.setOnMembership(true);
        ticket3.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket3.setPlateNumber("42");
        ticket3.setUser(user6);
        ticket3.setVehicleType(vehicleType3);

        // Act
        ResponseEntity<Object> actualUpdateTicketResult = ticketServiceImpl.updateTicket(ticket3, "42");

        // Assert
        verify(ticketRepository).findById(eq("42"));
        verify(ticketRepository).save(Mockito.<Ticket>any());
        assertEquals("42", ticket3.getId());
        assertNull(actualUpdateTicketResult.getBody());
        assertEquals(204, actualUpdateTicketResult.getStatusCodeValue());
        assertTrue(actualUpdateTicketResult.getHeaders().isEmpty());
    }

    /**
     * Method under test: {@link TicketServiceImpl#payTicket(String)}
     */
    @Test
    void testPayTicket() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());

        Ticket ticket = new Ticket();
        ticket.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setId("42");
        ticket.setOnMembership(true);
        ticket.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setPlateNumber("42");
        ticket.setUser(user2);
        ticket.setVehicleType(vehicleType);

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(new User());

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership3);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(user3);

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership4);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        VehicleType vehicleType2 = new VehicleType();
        vehicleType2.setFeePerHour(1);
        vehicleType2.setId(1);
        vehicleType2.setName("Name");
        vehicleType2.setTicket(new ArrayList<>());

        Ticket ticket2 = new Ticket();
        ticket2.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setId("42");
        ticket2.setOnMembership(true);
        ticket2.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setPlateNumber("42");
        ticket2.setUser(user4);
        ticket2.setVehicleType(vehicleType2);
        when(ticketRepository.save(Mockito.<Ticket>any())).thenReturn(ticket2);
        when(ticketRepository.findById(Mockito.<String>any())).thenReturn(ticket);

        // Act
        Ticket actualPayTicketResult = ticketServiceImpl.payTicket("42");

        // Assert
        verify(ticketRepository).findById(eq("42"));
        verify(ticketRepository).save(Mockito.<Ticket>any());
        assertSame(ticket2, actualPayTicketResult);
    }

    /**
     * Method under test: {@link TicketServiceImpl#admitTicket(String)}
     */
    @Test
    void testAdmitTicket() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());

        Ticket ticket = new Ticket();
        ticket.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setId("42");
        ticket.setOnMembership(true);
        ticket.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket.setPlateNumber("42");
        ticket.setUser(user2);
        ticket.setVehicleType(vehicleType);

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(new User());

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership3);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(user3);

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership4);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        VehicleType vehicleType2 = new VehicleType();
        vehicleType2.setFeePerHour(1);
        vehicleType2.setId(1);
        vehicleType2.setName("Name");
        vehicleType2.setTicket(new ArrayList<>());

        Ticket ticket2 = new Ticket();
        ticket2.setEntryTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setExitTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setId("42");
        ticket2.setOnMembership(true);
        ticket2.setPaymentTime(LocalDate.of(1970, 1, 1).atStartOfDay());
        ticket2.setPlateNumber("42");
        ticket2.setUser(user4);
        ticket2.setVehicleType(vehicleType2);
        when(ticketRepository.save(Mockito.<Ticket>any())).thenReturn(ticket2);
        when(ticketRepository.findById(Mockito.<String>any())).thenReturn(ticket);

        // Act
        Ticket actualAdmitTicketResult = ticketServiceImpl.admitTicket("42");

        // Assert
        verify(ticketRepository).findById(eq("42"));
        verify(ticketRepository).save(Mockito.<Ticket>any());
        assertSame(ticket2, actualAdmitTicketResult);
    }
}

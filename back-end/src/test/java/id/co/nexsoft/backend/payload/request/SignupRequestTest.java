package id.co.nexsoft.backend.payload.request;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

class SignupRequestTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link SignupRequest#setEmail(String)}
     *   <li>{@link SignupRequest#setPassword(String)}
     *   <li>{@link SignupRequest#setRole(Set)}
     *   <li>{@link SignupRequest#setUsername(String)}
     *   <li>{@link SignupRequest#getEmail()}
     *   <li>{@link SignupRequest#getPassword()}
     *   <li>{@link SignupRequest#getRole()}
     *   <li>{@link SignupRequest#getUsername()}
     * </ul>
     */
    @Test
    void testGettersAndSetters() {
        // Arrange
        SignupRequest signupRequest = new SignupRequest();

        // Act
        signupRequest.setEmail("jane.doe@example.org");
        signupRequest.setPassword("iloveyou");
        HashSet<String> role = new HashSet<>();
        signupRequest.setRole(role);
        signupRequest.setUsername("janedoe");
        String actualEmail = signupRequest.getEmail();
        String actualPassword = signupRequest.getPassword();
        Set<String> actualRole = signupRequest.getRole();

        // Assert that nothing has changed
        assertEquals("iloveyou", actualPassword);
        assertEquals("jane.doe@example.org", actualEmail);
        assertEquals("janedoe", signupRequest.getUsername());
        assertSame(role, actualRole);
    }
}

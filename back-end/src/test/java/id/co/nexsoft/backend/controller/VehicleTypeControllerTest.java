package id.co.nexsoft.backend.controller;

import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.nexsoft.backend.model.VehicleType;
import id.co.nexsoft.backend.service.VehicleTypeService;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {VehicleTypeController.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class VehicleTypeControllerTest {
    @Autowired
    private VehicleTypeController vehicleTypeController;

    @MockBean
    private VehicleTypeService vehicleTypeService;

    /**
     * Method under test: {@link VehicleTypeController#getVehicleTypeById(int)}
     */
    @Test
    void testGetVehicleTypeById() throws Exception {
        // Arrange
        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());
        when(vehicleTypeService.getVehicleTypeById(anyInt())).thenReturn(vehicleType);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/vehicle-type/{id}", 1);

        // Act and Assert
        MockMvcBuilders.standaloneSetup(vehicleTypeController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(
                        MockMvcResultMatchers.content().string("{\"id\":1,\"name\":\"Name\",\"feePerHour\":1,\"ticket\":[]}"));
    }

    /**
     * Method under test:
     * {@link VehicleTypeController#createVehicleType(VehicleType)}
     */
    @Test
    void testCreateVehicleType() throws Exception {
        // Arrange
        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());
        when(vehicleTypeService.createVehicleType(Mockito.<VehicleType>any())).thenReturn(vehicleType);

        VehicleType vehicleType2 = new VehicleType();
        vehicleType2.setFeePerHour(1);
        vehicleType2.setId(1);
        vehicleType2.setName("Name");
        vehicleType2.setTicket(new ArrayList<>());
        String content = (new ObjectMapper()).writeValueAsString(vehicleType2);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/vehicle-type")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);

        // Act
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(vehicleTypeController)
                .build()
                .perform(requestBuilder);

        // Assert
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(
                        MockMvcResultMatchers.content().string("{\"id\":1,\"name\":\"Name\",\"feePerHour\":1,\"ticket\":[]}"));
    }

    /**
     * Method under test:
     * {@link VehicleTypeController#updateVehicleType(VehicleType, int)}
     */
    @Test
    void testUpdateVehicleType() throws Exception {
        // Arrange
        when(vehicleTypeService.updateVehicleType(Mockito.<VehicleType>any(), anyInt())).thenReturn(null);

        VehicleType vehicleType = new VehicleType();
        vehicleType.setFeePerHour(1);
        vehicleType.setId(1);
        vehicleType.setName("Name");
        vehicleType.setTicket(new ArrayList<>());
        String content = (new ObjectMapper()).writeValueAsString(vehicleType);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/vehicle-type/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);

        // Act and Assert
        MockMvcBuilders.standaloneSetup(vehicleTypeController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Method under test: {@link VehicleTypeController#deleteVehicleType(int)}
     */
    @Test
    void testDeleteVehicleType() throws Exception {
        // Arrange
        doNothing().when(vehicleTypeService).deleteVehicleTypeById(anyInt());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/vehicle-type/{id}", 1);

        // Act and Assert
        MockMvcBuilders.standaloneSetup(vehicleTypeController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Method under test: {@link VehicleTypeController#deleteVehicleType(int)}
     */
    @Test
    void testDeleteVehicleType2() throws Exception {
        // Arrange
        doNothing().when(vehicleTypeService).deleteVehicleTypeById(anyInt());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/vehicle-type/{id}", 1);
        requestBuilder.contentType("https://example.org/example");

        // Act and Assert
        MockMvcBuilders.standaloneSetup(vehicleTypeController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Method under test: {@link VehicleTypeController#getAllVehicleTypes()}
     */
    @Test
    void testGetAllVehicleTypes() throws Exception {
        // Arrange
        when(vehicleTypeService.getVehicleTypes()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/vehicle-type");

        // Act and Assert
        MockMvcBuilders.standaloneSetup(vehicleTypeController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    /**
     * Method under test: {@link VehicleTypeController#getAllVehicleTypesPublic()}
     */
    @Test
    void testGetAllVehicleTypesPublic() throws Exception {
        // Arrange
        when(vehicleTypeService.getVehicleTypes()).thenReturn(new ArrayList<>());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/vehicle-type/all");

        // Act and Assert
        MockMvcBuilders.standaloneSetup(vehicleTypeController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }
}

package id.co.nexsoft.backend.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.co.nexsoft.backend.model.ERole;
import id.co.nexsoft.backend.model.Membership;
import id.co.nexsoft.backend.model.Source;
import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.repository.MembershipRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = { MembershipServiceImpl.class })
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class MembershipServiceImplTest {
    @MockBean
    private MembershipRepository membershipRepository;

    @Autowired
    private MembershipServiceImpl membershipServiceImpl;

    /**
     * Method under test: {@link MembershipServiceImpl#getMemberships()}
     */
    @Test
    void testGetMemberships() {
        // Arrange
        ArrayList<Membership> membershipList = new ArrayList<>();
        when(membershipRepository.findAll()).thenReturn(membershipList);

        // Act
        Iterable<Membership> actualMemberships = membershipServiceImpl.getMemberships();

        // Assert
        verify(membershipRepository).findAll();
        assertTrue(((List<Membership>) actualMemberships).isEmpty());
        assertSame(membershipList, actualMemberships);
    }

    /**
     * Method under test: {@link MembershipServiceImpl#getMembershipById(int)}
     */
    @Test
    void testGetMembershipById() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(user2);
        when(membershipRepository.findById(anyInt())).thenReturn(membership3);

        // Act
        Membership actualMembershipById = membershipServiceImpl.getMembershipById(1);

        // Assert
        verify(membershipRepository).findById(eq(1));
        assertSame(membership3, actualMembershipById);
    }

    /**
     * Method under test: {@link MembershipServiceImpl#createMembership(Membership)}
     */
    @Test
    void testCreateMembership() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(user2);
        when(membershipRepository.save(Mockito.<Membership>any())).thenReturn(membership3);

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(new Membership());
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(user3);

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership4);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        Membership membership5 = new Membership();
        membership5.setEnd(LocalDate.of(1970, 1, 1));
        membership5.setId(1);
        membership5.setStart(LocalDate.of(1970, 1, 1));
        membership5.setUser(user4);

        // Act
        Membership actualCreateMembershipResult = membershipServiceImpl.createMembership(membership5);

        // Assert
        verify(membershipRepository).save(Mockito.<Membership>any());
        assertSame(membership3, actualCreateMembershipResult);
    }

    /**
     * Method under test: {@link MembershipServiceImpl#deleteMembershipById(int)}
     */
    @Test
    void testDeleteMembershipById() {
        // Arrange
        doNothing().when(membershipRepository).deleteById(anyInt());

        // Act
        membershipServiceImpl.deleteMembershipById(1);

        // Assert that nothing has changed
        verify(membershipRepository).deleteById(eq(1));
        assertTrue(((List<Membership>) membershipServiceImpl.getMemberships()).isEmpty());
    }

    /**
     * Method under test:
     * {@link MembershipServiceImpl#updateMembership(Membership, int)}
     */
    @SuppressWarnings("deprecation")
    @Test
    void testUpdateMembership() {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");

        Membership membership3 = new Membership();
        membership3.setEnd(LocalDate.of(1970, 1, 1));
        membership3.setId(1);
        membership3.setStart(LocalDate.of(1970, 1, 1));
        membership3.setUser(user2);

        Membership membership4 = new Membership();
        membership4.setEnd(LocalDate.of(1970, 1, 1));
        membership4.setId(1);
        membership4.setStart(LocalDate.of(1970, 1, 1));
        membership4.setUser(new User());

        User user3 = new User();
        user3.setEmail("jane.doe@example.org");
        user3.setId(1);
        user3.setMembership(membership4);
        user3.setPassword("iloveyou");
        user3.setProfileImageUrl("https://example.org/example");
        user3.setRole(ERole.ROLE_USER);
        user3.setRoles(new HashSet<>());
        user3.setSource(Source.GOOGLE);
        user3.setTicket(new ArrayList<>());
        user3.setUsername("janedoe");

        Membership membership5 = new Membership();
        membership5.setEnd(LocalDate.of(1970, 1, 1));
        membership5.setId(1);
        membership5.setStart(LocalDate.of(1970, 1, 1));
        membership5.setUser(user3);

        User user4 = new User();
        user4.setEmail("jane.doe@example.org");
        user4.setId(1);
        user4.setMembership(membership5);
        user4.setPassword("iloveyou");
        user4.setProfileImageUrl("https://example.org/example");
        user4.setRole(ERole.ROLE_USER);
        user4.setRoles(new HashSet<>());
        user4.setSource(Source.GOOGLE);
        user4.setTicket(new ArrayList<>());
        user4.setUsername("janedoe");

        Membership membership6 = new Membership();
        membership6.setEnd(LocalDate.of(1970, 1, 1));
        membership6.setId(1);
        membership6.setStart(LocalDate.of(1970, 1, 1));
        membership6.setUser(user4);
        when(membershipRepository.save(Mockito.<Membership>any())).thenReturn(membership6);
        when(membershipRepository.findById(anyInt())).thenReturn(membership3);

        User user5 = new User();
        user5.setEmail("jane.doe@example.org");
        user5.setId(1);
        user5.setMembership(new Membership());
        user5.setPassword("iloveyou");
        user5.setProfileImageUrl("https://example.org/example");
        user5.setRole(ERole.ROLE_USER);
        user5.setRoles(new HashSet<>());
        user5.setSource(Source.GOOGLE);
        user5.setTicket(new ArrayList<>());
        user5.setUsername("janedoe");

        Membership membership7 = new Membership();
        membership7.setEnd(LocalDate.of(1970, 1, 1));
        membership7.setId(1);
        membership7.setStart(LocalDate.of(1970, 1, 1));
        membership7.setUser(user5);

        User user6 = new User();
        user6.setEmail("jane.doe@example.org");
        user6.setId(1);
        user6.setMembership(membership7);
        user6.setPassword("iloveyou");
        user6.setProfileImageUrl("https://example.org/example");
        user6.setRole(ERole.ROLE_USER);
        user6.setRoles(new HashSet<>());
        user6.setSource(Source.GOOGLE);
        user6.setTicket(new ArrayList<>());
        user6.setUsername("janedoe");

        Membership membership8 = new Membership();
        membership8.setEnd(LocalDate.of(1970, 1, 1));
        membership8.setId(1);
        membership8.setStart(LocalDate.of(1970, 1, 1));
        membership8.setUser(user6);

        // Act
        ResponseEntity<Object> actualUpdateMembershipResult = membershipServiceImpl.updateMembership(membership8, 1);

        // Assert
        verify(membershipRepository).findById(eq(1));
        verify(membershipRepository).save(Mockito.<Membership>any());
        assertNull(actualUpdateMembershipResult.getBody());
        assertEquals(1, membership8.getId().intValue());
        assertEquals(204, actualUpdateMembershipResult.getStatusCodeValue());
        assertTrue(actualUpdateMembershipResult.getHeaders().isEmpty());
    }


    // @Test
    // void testUpdateMembership_Success() {
    // // Arrange
    // Membership existingMembership = new Membership();
    // existingMembership.setId(1);

    // when(membershipRepository.findById(1)).thenReturn(existingMembership);
    // when(membershipRepository.save(existingMembership)).thenReturn(existingMembership);

    // // Act
    // ResponseEntity<Object> response =
    // membershipServiceImpl.updateMembership(existingMembership, 1);

    // // Assert
    // verify(membershipRepository).findById(1);
    // verify(membershipRepository).save(existingMembership);
    // assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    // }

    // @Test
    // void testUpdateMembership_NotFound() {
    // // Arrange
    // Membership updatedMembership = new Membership();

    // when(membershipRepository.findById(1)).thenReturn(updatedMembership);

    // // Act
    // ResponseEntity<Object> response =
    // membershipServiceImpl.updateMembership(updatedMembership, 1);

    // // Assert
    // verify(membershipRepository).findById(1);
    // assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    // }
}

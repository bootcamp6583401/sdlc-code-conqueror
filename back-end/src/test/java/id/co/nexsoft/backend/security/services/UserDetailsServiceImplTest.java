package id.co.nexsoft.backend.security.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.co.nexsoft.backend.model.ERole;
import id.co.nexsoft.backend.model.Membership;
import id.co.nexsoft.backend.model.Role;
import id.co.nexsoft.backend.model.Source;
import id.co.nexsoft.backend.model.Ticket;
import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.repository.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {UserDetailsServiceImpl.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class UserDetailsServiceImplTest {
    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @MockBean
    private UserRepository userRepository;

    /**
     * Method under test: {@link UserDetailsServiceImpl#loadUserByUsername(String)}
     */
    @Test
    void testLoadUserByUsername() throws UsernameNotFoundException {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        ArrayList<Ticket> ticket = new ArrayList<>();
        user.setTicket(ticket);
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(new HashSet<>());
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user2);
        when(userRepository.findByUsername(Mockito.<String>any())).thenReturn(ofResult);

        // Act
        UserDetails actualLoadUserByUsernameResult = userDetailsServiceImpl.loadUserByUsername("janedoe");

        // Assert
        verify(userRepository).findByUsername(eq("janedoe"));
        assertEquals("iloveyou", actualLoadUserByUsernameResult.getPassword());
        assertEquals("jane.doe@example.org", ((UserDetailsImpl) actualLoadUserByUsernameResult).getEmail());
        assertEquals("janedoe", actualLoadUserByUsernameResult.getUsername());
        assertEquals(1, ((UserDetailsImpl) actualLoadUserByUsernameResult).getId().intValue());
        assertEquals(ticket, actualLoadUserByUsernameResult.getAuthorities());
    }

    /**
     * Method under test: {@link UserDetailsServiceImpl#loadUserByUsername(String)}
     */
    @Test
    void testLoadUserByUsername2() throws UsernameNotFoundException {
        // Arrange
        Membership membership = new Membership();
        membership.setEnd(LocalDate.of(1970, 1, 1));
        membership.setId(1);
        membership.setStart(LocalDate.of(1970, 1, 1));
        membership.setUser(new User());

        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1);
        user.setMembership(membership);
        user.setPassword("iloveyou");
        user.setProfileImageUrl("https://example.org/example");
        user.setRole(ERole.ROLE_USER);
        user.setRoles(new HashSet<>());
        user.setSource(Source.GOOGLE);
        user.setTicket(new ArrayList<>());
        user.setUsername("janedoe");

        Membership membership2 = new Membership();
        membership2.setEnd(LocalDate.of(1970, 1, 1));
        membership2.setId(1);
        membership2.setStart(LocalDate.of(1970, 1, 1));
        membership2.setUser(user);

        Role role = new Role();
        role.setId(1);
        role.setName(ERole.ROLE_USER);

        HashSet<Role> roles = new HashSet<>();
        roles.add(role);

        User user2 = new User();
        user2.setEmail("jane.doe@example.org");
        user2.setId(1);
        user2.setMembership(membership2);
        user2.setPassword("iloveyou");
        user2.setProfileImageUrl("https://example.org/example");
        user2.setRole(ERole.ROLE_USER);
        user2.setRoles(roles);
        user2.setSource(Source.GOOGLE);
        user2.setTicket(new ArrayList<>());
        user2.setUsername("janedoe");
        Optional<User> ofResult = Optional.of(user2);
        when(userRepository.findByUsername(Mockito.<String>any())).thenReturn(ofResult);

        // Act
        UserDetails actualLoadUserByUsernameResult = userDetailsServiceImpl.loadUserByUsername("janedoe");

        // Assert
        verify(userRepository).findByUsername(eq("janedoe"));
        Collection<? extends GrantedAuthority> authorities = actualLoadUserByUsernameResult.getAuthorities();
        assertEquals(1, authorities.size());
        assertEquals("ROLE_USER", ((List<? extends GrantedAuthority>) authorities).get(0).getAuthority());
        assertEquals("iloveyou", actualLoadUserByUsernameResult.getPassword());
        assertEquals("jane.doe@example.org", ((UserDetailsImpl) actualLoadUserByUsernameResult).getEmail());
        assertEquals("janedoe", actualLoadUserByUsernameResult.getUsername());
        assertEquals(1, ((UserDetailsImpl) actualLoadUserByUsernameResult).getId().intValue());
    }

    /**
     * Method under test: {@link UserDetailsServiceImpl#loadUserByUsername(String)}
     */
    @Test
    void testLoadUserByUsername3() throws UsernameNotFoundException {
        // Arrange
        Optional<User> emptyResult = Optional.empty();
        when(userRepository.findByUsername(Mockito.<String>any())).thenReturn(emptyResult);

        // Act and Assert
        assertThrows(UsernameNotFoundException.class, () -> userDetailsServiceImpl.loadUserByUsername("janedoe"));
        verify(userRepository).findByUsername(eq("janedoe"));
    }

    /**
     * Method under test: {@link UserDetailsServiceImpl#loadUserByUsername(String)}
     */
    @Test
    void testLoadUserByUsername4() throws UsernameNotFoundException {
        // Arrange
        when(userRepository.findByUsername(Mockito.<String>any())).thenThrow(new UsernameNotFoundException("Msg"));

        // Act and Assert
        assertThrows(UsernameNotFoundException.class, () -> userDetailsServiceImpl.loadUserByUsername("janedoe"));
        verify(userRepository).findByUsername(eq("janedoe"));
    }
}

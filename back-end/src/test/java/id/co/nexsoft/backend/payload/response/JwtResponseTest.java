package id.co.nexsoft.backend.payload.response;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class JwtResponseTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link JwtResponse#JwtResponse()}
     *   <li>{@link JwtResponse#setAccessToken(String)}
     *   <li>{@link JwtResponse#setEmail(String)}
     *   <li>{@link JwtResponse#setId(Integer)}
     *   <li>{@link JwtResponse#setTokenType(String)}
     *   <li>{@link JwtResponse#setUsername(String)}
     *   <li>{@link JwtResponse#getAccessToken()}
     *   <li>{@link JwtResponse#getEmail()}
     *   <li>{@link JwtResponse#getId()}
     *   <li>{@link JwtResponse#getRoles()}
     *   <li>{@link JwtResponse#getTokenType()}
     *   <li>{@link JwtResponse#getUsername()}
     * </ul>
     */
    @Test
    void testGettersAndSetters() {
        // Arrange and Act
        JwtResponse actualJwtResponse = new JwtResponse();
        actualJwtResponse.setAccessToken("ABC123");
        actualJwtResponse.setEmail("jane.doe@example.org");
        actualJwtResponse.setId(1);
        actualJwtResponse.setTokenType("ABC123");
        actualJwtResponse.setUsername("janedoe");
        String actualAccessToken = actualJwtResponse.getAccessToken();
        String actualEmail = actualJwtResponse.getEmail();
        Integer actualId = actualJwtResponse.getId();
        actualJwtResponse.getRoles();
        String actualTokenType = actualJwtResponse.getTokenType();

        // Assert that nothing has changed
        assertEquals("ABC123", actualAccessToken);
        assertEquals("ABC123", actualTokenType);
        assertEquals("jane.doe@example.org", actualEmail);
        assertEquals("janedoe", actualJwtResponse.getUsername());
        assertEquals(1, actualId.intValue());
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link JwtResponse#JwtResponse(String, Integer, String, String, List)}
     *   <li>{@link JwtResponse#setAccessToken(String)}
     *   <li>{@link JwtResponse#setEmail(String)}
     *   <li>{@link JwtResponse#setId(Integer)}
     *   <li>{@link JwtResponse#setTokenType(String)}
     *   <li>{@link JwtResponse#setUsername(String)}
     *   <li>{@link JwtResponse#getAccessToken()}
     *   <li>{@link JwtResponse#getEmail()}
     *   <li>{@link JwtResponse#getId()}
     *   <li>{@link JwtResponse#getRoles()}
     *   <li>{@link JwtResponse#getTokenType()}
     *   <li>{@link JwtResponse#getUsername()}
     * </ul>
     */
    @Test
    void testGettersAndSetters2() {
        // Arrange
        ArrayList<String> roles = new ArrayList<>();

        // Act
        JwtResponse actualJwtResponse = new JwtResponse("ABC123", 1, "janedoe", "jane.doe@example.org", roles);
        actualJwtResponse.setAccessToken("ABC123");
        actualJwtResponse.setEmail("jane.doe@example.org");
        actualJwtResponse.setId(1);
        actualJwtResponse.setTokenType("ABC123");
        actualJwtResponse.setUsername("janedoe");
        String actualAccessToken = actualJwtResponse.getAccessToken();
        String actualEmail = actualJwtResponse.getEmail();
        Integer actualId = actualJwtResponse.getId();
        List<String> actualRoles = actualJwtResponse.getRoles();
        String actualTokenType = actualJwtResponse.getTokenType();

        // Assert that nothing has changed
        assertEquals("ABC123", actualAccessToken);
        assertEquals("ABC123", actualTokenType);
        assertEquals("jane.doe@example.org", actualEmail);
        assertEquals("janedoe", actualJwtResponse.getUsername());
        assertEquals(1, actualId.intValue());
        assertSame(roles, actualRoles);
    }
}

package id.co.nexsoft.backend.auth;

import org.springframework.aot.generate.Generated;

import com.fasterxml.jackson.annotation.JsonProperty;

@Generated
public class GoogleModel {
    private String id;
    private String email;

    @JsonProperty("verified_email")
    private boolean verifiedEmail;
    private String name;

    @JsonProperty("given_name")
    private String givenName;
    private String picture;
    private String locale;

    @JsonProperty("family_name")
    private String familyName;

    public String getFamilyName() {
        return familyName;
    }

    public void setFamily_name(String familyName) {
        this.familyName = familyName;
    }

    public GoogleModel() {
    }

    public GoogleModel(String id, String email, boolean verifiedEmail, String name, String givenName, String picture,
            String locale) {
        this.id = id;
        this.email = email;
        this.verifiedEmail = verifiedEmail;
        this.name = name;
        this.givenName = givenName;
        this.picture = picture;
        this.locale = locale;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isVerifiedEmail() {
        return verifiedEmail;
    }

    public void setVerifiedEmail(boolean verifiedEmail) {
        this.verifiedEmail = verifiedEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}

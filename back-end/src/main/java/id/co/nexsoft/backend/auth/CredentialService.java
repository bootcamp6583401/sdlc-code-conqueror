package id.co.nexsoft.backend.auth;

import java.util.List;

public interface CredentialService<T> {
    List<T> getAllDataByEmail(String email);
    List<T> getAllDataByPhone(String phone);
}


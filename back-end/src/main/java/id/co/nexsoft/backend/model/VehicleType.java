package id.co.nexsoft.backend.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter

@Entity
@Table(name = "vehicle_type")
public class VehicleType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private int feePerHour;

    @OneToMany(mappedBy = "vehicleType")
    @JsonManagedReference(value = "vehicleType-ticket")
    private List<Ticket> ticket;

    @OneToMany(mappedBy = "vehicleType")
    @JsonManagedReference(value = "vehicleType-membership")
    private List<Membership> memberships;

    public VehicleType() {}

    public VehicleType(String name, int feePerHour) {
        this.name = name;
        this.feePerHour = feePerHour;
    }
}
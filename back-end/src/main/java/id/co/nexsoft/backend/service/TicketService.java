package id.co.nexsoft.backend.service;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import id.co.nexsoft.backend.model.Ticket;

public interface TicketService {
    Page<Ticket> getTicketPages(int pageNumber);
    Iterable<Ticket> getTickets();
    Ticket getTicketById(String id);
    Ticket createTicket(Ticket ticket);
    void deleteTicketById(String id);
    ResponseEntity<Object> updateTicket(Ticket ticket, String id);
    Iterable<Ticket> getUserTickets(int userId, int page);
    Ticket payTicket(String id);
    Ticket admitTicket(String id);
}

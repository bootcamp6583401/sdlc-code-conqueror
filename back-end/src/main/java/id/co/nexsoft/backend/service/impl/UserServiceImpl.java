package id.co.nexsoft.backend.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.repository.UserRepository;
import id.co.nexsoft.backend.service.UserService;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository repo;

    @Override
    public Page<User> getUsers(int pageNumber) {
        Pageable paginatedData = PageRequest.of(pageNumber, 10);

        return repo.findAll(paginatedData);
    }

    @Override
    public Iterable<User> getAllUsers() {

        return repo.findAll();
    }

    @Override
    public User getUserById(int id) {
        return repo.findById(id);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return repo.findByEmail(email);
    }

    @Override
    public void save(User user) {
        repo.save(user);
    }

    @Override
    public User createUser(User user) {
        return repo.save(user);
    }

    @Override
    public void deleteUserById(int id) {
        repo.deleteById(id);
    }

    @Override
    public ResponseEntity<Object> updateUser(User user, int id) {
        Optional<User> userOptional = Optional.ofNullable(repo.findById(id));

        if (!userOptional.isPresent())
            return ResponseEntity.notFound().build();

        User existingUser = userOptional.get();

        existingUser.setPhone(user.getPhone());
        repo.save(existingUser);
        return ResponseEntity.noContent().build();
    }

    @Override
    public Page<User> searchUsers(String keyword, int pageNumber) {
        Pageable paginatedData = PageRequest.of(pageNumber, 10);

        Page<User> users = repo.searchUsersByKeyword(keyword.toLowerCase(), paginatedData);
        return users;
    }

}

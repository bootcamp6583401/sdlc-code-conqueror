package id.co.nexsoft.backend.dto;

public class ResultDTO {
    private String qrValue;

    public String getQrValue() {
        return qrValue;
    }

    public void setQrValue(String qrValue) {
        this.qrValue = qrValue;
    }
}

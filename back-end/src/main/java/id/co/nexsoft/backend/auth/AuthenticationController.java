package id.co.nexsoft.backend.auth;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.aot.generate.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.payload.response.JwtResponse;

@RequestMapping("/api/v1/auth")
@Generated
@RestController
public class AuthenticationController {
    @Autowired
    private AuthenticationService service;

    // @Autowired
    private CredentialService<User> uuidService;

    private static final String GOOGLE_URL = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=";

    @PostMapping("/google")
    public ResponseEntity<?> googleSignIn(@RequestHeader("Token") String userToken) {
        GoogleModel result = getGoogleData(userToken);
        return service.googleSignIn(result);
    }

    public GoogleModel getGoogleData(String userToken) {
        HttpResponse<String> response = getData(userToken);
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            JsonNode jsonNode = objectMapper.readTree(response.body());
            GoogleModel person = objectMapper.treeToValue(jsonNode, GoogleModel.class);
            return person;
        } catch (Exception e) {
            System.out.println("error : " + e.getMessage());
            return null;
        }
    }

    public static HttpResponse<String> getData(String urlString) {
        String url = GOOGLE_URL + urlString;

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + urlString)
                .build();

        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            return httpResponse;
        } catch (Exception e) {
            return null;
        }
    }

    public Map<String, Object> returnDataGoogle(JwtResponse token, GoogleModel request) {
        Map<String, Object> returnData = new HashMap<>();
        returnData.put("user", userDataGoogle(request));
        returnData.put("token", token);
        return returnData;
    }

    public Map<String, Object> userDataGoogle(GoogleModel request) {
        Map<String, Object> returnData = new HashMap<>();
        List<User> user = uuidService.getAllDataByEmail(request.getEmail());
        User userData = user.get(0);
        returnData.put("id", userData.getId());
        returnData.put("name", userData.getUsername());
        returnData.put("role", userData.getRole());
        return returnData;
    }
}

package id.co.nexsoft.backend.service;

import org.springframework.http.ResponseEntity;

import id.co.nexsoft.backend.model.Membership;

public interface MembershipService {
    Iterable<Membership> getMemberships();
    Membership getMembershipById(int id);
    Membership createMembership(Membership membership);
    void deleteMembershipById(int id);
    ResponseEntity<Object> updateMembership(Membership membership, int id);
}

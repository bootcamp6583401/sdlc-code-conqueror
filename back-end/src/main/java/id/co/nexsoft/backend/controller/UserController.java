package id.co.nexsoft.backend.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.springframework.aot.generate.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import id.co.nexsoft.backend.model.Ticket;
import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.service.TicketService;
import id.co.nexsoft.backend.service.UserService;

@RestController
@RequestMapping(path = "/user")
@Generated
public class UserController {
    @Autowired
    private UserService service;

    @Autowired
    private TicketService ticketService;

    private final String FOLDER_PATH="/Users/Nexsoft/springbootimages";


    @GetMapping("/paginated")
    public Iterable<User> getPagniatedUsers(@RequestParam(value = "page", defaultValue = "0") int pageNumber) {
        return service.getUsers(pageNumber);
    }

    @GetMapping
    public Iterable<User> getAllUsers() {
        return service.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable(value = "id") int id) {
        return service.getUserById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody User user) {
        return service.createUser(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable(value = "id") int id) {
        service.deleteUserById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateUser(
            @RequestBody User user,
            @PathVariable int id) {
        return service.updateUser(user, id);
    }

    @PutMapping("/{id}/update-profile-image")
    public ResponseEntity<User> updateProfileImage(
            @RequestParam("image") MultipartFile file,
            @PathVariable int id) {
                try {
                    User user = service.getUserById(id);
                   
                    File folder = new File(FOLDER_PATH);

                    if(folder.exists()){
                    }else {
                        folder.mkdirs();
                    }
                    String originalFileName = file.getOriginalFilename();
                    String ext = originalFileName.substring(originalFileName.lastIndexOf("."));
                    String newFileName = user.getId().toString() + ext;

                    File newFile = new File(FOLDER_PATH,  newFileName);
                    Path filePath = newFile.toPath();
                    file.transferTo(filePath);
                    user.setProfileImageUrl(newFileName);
                    service.save(user);

                    return ResponseEntity.ok(user);
                } catch (IOException e) {
                    return ResponseEntity.ok(null);

                }

    }

    @GetMapping("/{id}/tickets")
    public ResponseEntity<Iterable<Ticket>> getUserTickets(@PathVariable int id, @RequestParam(value = "page", defaultValue = "0") int pageNumber) {
        return ResponseEntity.ok(ticketService.getUserTickets(id, pageNumber));
    }

    @GetMapping("/search")
    public ResponseEntity<Page<User>> searchUsers(@RequestParam String keyword, @RequestParam(value = "page", defaultValue = "0") int pageNumber) {
        Page<User> users = service.searchUsers(keyword, pageNumber);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

}
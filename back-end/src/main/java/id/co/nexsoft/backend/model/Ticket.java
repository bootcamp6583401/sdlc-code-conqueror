package id.co.nexsoft.backend.model;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;

    private String plateNumber;
    private LocalDateTime entryTime;
    private LocalDateTime exitTime;
    private LocalDateTime paymentTime;

    private boolean onMembership;

    @ManyToOne
    @JoinColumn(name = "vehicle_type_id", referencedColumnName = "id")
    @JsonBackReference(value = "vehicleType-ticket")
    private VehicleType vehicleType;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonBackReference(value = "user-ticket")
    private User user;

    

    public Ticket() {
    }

    public Ticket(String plateNumber, LocalDateTime entryTime, LocalDateTime exitTime, LocalDateTime paymentTime) {
        this.plateNumber = plateNumber;
        this.entryTime = entryTime;
        this.exitTime = exitTime;
        this.paymentTime = paymentTime;
    }

    public boolean getDoesUserHaveActiveMembership() {
        return this.getUser() !=null 
        
            && this.getUser().getMembership() != null 
            && this.getUser().getMembership().getEnd().isAfter(LocalDate.now())
            && this.getUser().getMembership().getPlateNumber().equals(this.getPlateNumber())
            ;
    }

    public String getVehicleTypeName() {
        return this.vehicleType.getName();
    }

    public Integer getFeePerHour() {
        return this.vehicleType.getFeePerHour();
    }

    public Integer getTotalTime() {
        if (exitTime != null) {
            Duration exclusiveDuration = Duration.between(this.getEntryTime(), this.getExitTime());
            double hours = (double) exclusiveDuration.toSeconds() / 3600;
            return (int) Math.ceil(hours);
        }
        return null;
    }

    public Integer getTotalFee() {
        if (this.getTotalTime() != null) {
            if(this.isOnMembership() 
                // && this.getUser().getMembership().getPlateNumber().equals(this.getPlateNumber())
            ){
                return 0;
            }
            return this.getTotalTime() * this.getFeePerHour();
        }
        return null;
    }
}
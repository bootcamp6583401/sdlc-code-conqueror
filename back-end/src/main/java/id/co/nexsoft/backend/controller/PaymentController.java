package id.co.nexsoft.backend.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.midtrans.Midtrans;
import com.midtrans.httpclient.SnapApi;
import com.midtrans.httpclient.error.MidtransError;

import id.co.nexsoft.backend.dto.TransactionStatusModel;
import id.co.nexsoft.backend.model.Membership;
import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.model.VehicleType;
import id.co.nexsoft.backend.payload.request.MembershipRequest;
import id.co.nexsoft.backend.service.MembershipService;
import id.co.nexsoft.backend.service.UserService;
import id.co.nexsoft.backend.service.VehicleTypeService;
import java.net.http.HttpClient;



@RestController
@RequestMapping("/pay")
public class PaymentController {
    
    @Autowired
    private VehicleTypeService vhservice;

    @Autowired private MembershipService mmservice;

    @Autowired private UserService uservice;
    
    @PostMapping("/membership")
    public ResponseEntity<String> pay(@RequestBody MembershipRequest req) throws MidtransError{
        Midtrans.serverKey = "SB-Mid-server-qwtE1RJycbs3WSWAgCCQKK2o";


        // Set value to true if you want Production Environment (accept real transaction).
        Midtrans.isProduction = false;

        VehicleType vehicleType = vhservice.getVehicleTypeById(req.getVehicleTypeId());

        System.out.println(vehicleType);
        System.out.println(req.getVehicleTypeId());
        System.out.println(req.getPlateNumber());
        System.out.println(req.getExpiryDate());
        System.out.println(req.getTotalDays());

        int transactionTotal = vehicleType.getFeePerHour() * req.getTotalDays();
        
        UUID idRand = UUID.randomUUID();
        Map<String, Object> params = new HashMap<>();

        Map<String, Object> transactionDetails = new HashMap<>();
        transactionDetails.put("order_id", idRand);
        transactionDetails.put("gross_amount", transactionTotal);

        Map<String, Object> customerDetails = new HashMap<>();
        customerDetails.put("email", "eren@email.com");
        customerDetails.put("first_name", "Eren Yaeger");

        Map<String, Object> callbacks = new HashMap<>();
        callbacks.put("finish", "http://localhost:5173/user/membership?" 
            + "plateNumber=" + req.getPlateNumber() 
            + "&expiryDate=" + req.getExpiryDate() 
            + "&vehicleTypeId=" + req.getVehicleTypeId());

        Map<String, String> creditCard = new HashMap<>();
        creditCard.put("secure", "true");

        params.put("transaction_details", transactionDetails);
        params.put("credit_card", creditCard);
        params.put("callbacks", callbacks);
        params.put("customer_details", customerDetails);
        String transactionToken = SnapApi.createTransactionToken(params);
        return ResponseEntity.ok(transactionToken);

    }

    @PostMapping("/handle-success/{orderId}")
    public ResponseEntity<Membership> getStatus(@PathVariable String orderId, @RequestBody MembershipRequest membershipRequest) throws Exception{
        String url = "https://api.sandbox.midtrans.com/v2/" + orderId + "/status";
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .header("Content-Type", "application/json")
                .header("Authorization", "Basic U0ItTWlkLXNlcnZlci1xd3RFMVJKeWNiczNXU1dBZ0NDUUtLMm86").
                header("Accept", "application/json")
                .build();

        HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        System.out.println(httpResponse.body());


        JsonNode jsonNode = objectMapper.readTree(httpResponse.body());
        TransactionStatusModel status = objectMapper.treeToValue(jsonNode, TransactionStatusModel.class);

        try {
            if(status.getStatusCode() != 200) throw new Exception("Payment failed");


            Membership membership = new Membership();
            User user = uservice.getUserById(membershipRequest.getUserId());

            // && user.getMembership().getEnd().isAfter(LocalDate.now())
            if(user.getMembership() != null){
                if(user.getMembership().getTransactionId() != null && user.getMembership().getTransactionId().equals(status.getTransactionId())){
                   
                    // user already has membership, and its the same transaction id -> can't remake it duh
                    throw new Exception("A membership has already been created from this transaction id");
                }else {
                    // already a membershp, but different transaction id
                    System.out.println("DELETING MEMBERSHIP WITH DIFFERENT TRANSACTION ID");
                    mmservice.deleteMembershipById(user.getMembership().getId());
                }
            }

            VehicleType vType = vhservice.getVehicleTypeById(membershipRequest.getVehicleTypeId());
            membership.setEnd(membershipRequest.getExpiryDate());
            membership.setPlateNumber(membershipRequest.getPlateNumber());
            membership.setUser(user);
            membership.setVehicleType(vType);
            membership.setTransactionId(status.getTransactionId());

            mmservice.createMembership(membership);
            return ResponseEntity.ok(membership);

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e.getMessage());
            System.out.println("WOWOWOOWOWO OWOWO OWO OWOWOW");
            throw new Exception();

        }
             
    }

}

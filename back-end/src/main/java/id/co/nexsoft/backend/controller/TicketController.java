package id.co.nexsoft.backend.controller;

import org.springframework.aot.generate.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;

import id.co.nexsoft.backend.model.Ticket;
import id.co.nexsoft.backend.service.TicketService;

@RestController
@Generated
@RequestMapping(path = "/ticket")
public class TicketController {
    @Autowired
    private TicketService service;

    @GetMapping("/paginated")
    public Iterable<Ticket> getPagniatedTickets(@RequestParam(value = "page", defaultValue = "0") int pageNumber) {
        return service.getTicketPages(pageNumber);
    }

    @GetMapping
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Iterable<Ticket> getAllTickets() {
        return service.getTickets();
    }

    @GetMapping("/{id}")
    public Ticket getTicketById(@PathVariable(value = "id") String id) {
        return service.getTicketById(id);
    }

    @GetMapping("/guest/{id}")
    public Ticket getTicketByIdGuest(@PathVariable(value = "id") String id) {
        Ticket ticket = service.getTicketById(id);
        if(ticket.getUser() != null) {
            throw new BadCredentialsException("Cannot access. Please log in.");
        }
        return ticket;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Ticket createTicket(@RequestBody Ticket ticket) {
        return service.createTicket(ticket);
    }

    @PostMapping("/guest")
    @ResponseStatus(HttpStatus.CREATED)
    public Ticket createGuestTicket(@RequestBody Ticket ticket) {
        return service.createTicket(ticket);
    }

    @DeleteMapping("/{id}")
    public void deleteTicket(@PathVariable(value = "id") String id) {
        service.deleteTicketById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateTicket(
            @RequestBody Ticket ticket,
            @PathVariable String id) {
        return service.updateTicket(ticket, id);
    }

    @PutMapping("/{id}/pay")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> payTicket(
            @PathVariable String id) {
                
        return ResponseEntity.ok(service.payTicket(id));
    }

    @PutMapping("/{id}/admit")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> admitTicket(
            @PathVariable String id) {
                
        return ResponseEntity.ok(service.admitTicket(id));
    }
}
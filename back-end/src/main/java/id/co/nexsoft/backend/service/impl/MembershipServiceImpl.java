package id.co.nexsoft.backend.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.model.Membership;
import id.co.nexsoft.backend.repository.MembershipRepository;
import id.co.nexsoft.backend.service.MembershipService;

@Service
public class MembershipServiceImpl implements MembershipService {
    @Autowired
    private MembershipRepository repo;

    @Override
    public Iterable<Membership> getMemberships() {
        return repo.findAll();
    }

    @Override
    public Membership getMembershipById(int id) {
        return repo.findById(id);
    }

    @Override
    public Membership createMembership(Membership membership) {
        return repo.save(membership);
    }

    @Override
    public void deleteMembershipById(int id) {
        repo.deleteById(id);
    }

    @Override
    public ResponseEntity<Object> updateMembership(Membership membership, int id) {
        Optional<Membership> membershipOptional = Optional.ofNullable(repo.findById(id));

        if (!membershipOptional.isPresent())
            return ResponseEntity.notFound().build();

        membership.setId(id);
        repo.save(membership);
        return ResponseEntity.noContent().build();
    }
}

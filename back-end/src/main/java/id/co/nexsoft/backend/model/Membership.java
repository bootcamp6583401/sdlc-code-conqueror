package id.co.nexsoft.backend.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity
@Table(name = "membership")
public class Membership {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private LocalDate start = LocalDate.now();
    private LocalDate end;

    private String plateNumber;

    private String transactionId;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonBackReference(value = "user-membership")
    private User user;

    @ManyToOne
    @JoinColumn(name = "vehicle_type_id", referencedColumnName = "id")
    @JsonBackReference(value = "vehicleType-membership")
    private VehicleType vehicleType;

    public Membership() {}

    public Membership(LocalDate start, LocalDate end) {
        this.start = start;
        this.end = end;
    }

    public String getVehicleTypeName() {
        return this.vehicleType.getName();
    }

    public int getVehicleTypeId() {
        return this.vehicleType.getId();
    }

}
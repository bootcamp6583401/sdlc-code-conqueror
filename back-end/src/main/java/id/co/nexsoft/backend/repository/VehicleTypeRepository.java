package id.co.nexsoft.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.model.VehicleType;

public interface VehicleTypeRepository extends JpaRepository<VehicleType, Integer> {
    VehicleType findById(int id);
    List<VehicleType> findAll();
    void deleteById(int id);
}

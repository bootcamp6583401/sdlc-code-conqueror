package id.co.nexsoft.backend.payload.request;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

public class MembershipRequest {
    @NotBlank
    @Getter
    @Setter
    private String plateNumber;

    @NotBlank
    @Getter
    @Setter
    private LocalDate expiryDate;

    @NotBlank
    @Getter
    @Setter
    private int vehicleTypeId;

    @NotBlank
    @Getter
    @Setter
    private int userId;

    public int getTotalDays(){
        LocalDate today = LocalDate.now();
        Duration exclusiveDuration = Duration.between(
            LocalDateTime.of(today.getYear(), today.getMonth(), today.getDayOfMonth(), 0, 0, 0)    
        
        , LocalDateTime.of(expiryDate.getYear(), expiryDate.getMonth(), expiryDate.getDayOfMonth(), 23, 59, 59));
        double days = (double) exclusiveDuration.toDays();
        if(days <= 0) days++;
        return (int) Math.ceil(days);
    }

    public MembershipRequest(@NotBlank String plateNumber, @NotBlank LocalDate expiryDate,
            @NotBlank int vehicleTypeId) {
        this.plateNumber = plateNumber;
        this.expiryDate = expiryDate;
        this.vehicleTypeId = vehicleTypeId;
    }

    

}

package id.co.nexsoft.backend.auth;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.aot.generate.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.model.ERole;
import id.co.nexsoft.backend.model.Role;
import id.co.nexsoft.backend.model.User;
import id.co.nexsoft.backend.payload.request.LoginRequest;
import id.co.nexsoft.backend.payload.response.JwtResponse;
import id.co.nexsoft.backend.repository.RoleRepository;
import id.co.nexsoft.backend.repository.UserRepository;
import id.co.nexsoft.backend.security.jwt.JwtUtils;
import id.co.nexsoft.backend.security.services.UserDetailsImpl;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Service
@Generated
public class AuthenticationService {
    @Autowired
    private UserRepository repository;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;

     @Autowired
    RoleRepository roleRepository;

    public ResponseEntity<?> googleSignIn(GoogleModel request) {
        LoginRequest userReq = new LoginRequest();
        Optional<User> user = repository.findByUsername(request.getName());

        if (!user.isEmpty()) {
            userReq.setUsername(user.get().getUsername());
            userReq.setPassword("123456");
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(userReq.getUsername(), userReq.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(item -> item.getAuthority())
                    .collect(Collectors.toList());

            return ResponseEntity.ok(new JwtResponse(jwt,
                    userDetails.getId(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    roles));
        } else {
            User userGoogle = new User();
            userGoogle.setUsername(request.getName());
            userGoogle.setEmail(request.getEmail());
            userGoogle.setProfileImageUrl(request.getPicture());
             Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            Set<Role> userRoles = new HashSet<>();
            userRoles.add(userRole);
            userGoogle.setRoles(userRoles);
            userGoogle.setPassword(encoder.encode("123456"));
            repository.save(userGoogle);

            userReq.setUsername(userGoogle.getUsername());
            userReq.setPassword(userGoogle.getPassword());
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(userGoogle.getUsername(), "123456"));


            SecurityContextHolder.getContext().setAuthentication(authentication);

            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(item -> item.getAuthority())
                    .collect(Collectors.toList());


            return ResponseEntity.ok(new JwtResponse(jwt,
                    userDetails.getId(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    roles));
        }
    }

}

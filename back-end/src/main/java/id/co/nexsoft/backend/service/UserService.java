package id.co.nexsoft.backend.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import id.co.nexsoft.backend.model.User;

public interface UserService {
    Page<User> getUsers(int pageNumber);
    Iterable<User> getAllUsers();
    User getUserById(int id);
    User createUser(User user);
    void deleteUserById(int id);
    ResponseEntity<Object> updateUser(User user, int id);

    Optional<User> findByEmail(String email);
    void save(User userEntity);
    Page<User> searchUsers(String keyword, int pageNumber);
}

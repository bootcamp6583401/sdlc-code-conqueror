package id.co.nexsoft.backend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.model.Membership;

public interface MembershipRepository extends JpaRepository<Membership, Integer> {
    Membership findById(int id);
    List<Membership> findAll();
    void deleteById(int id);
}

package id.co.nexsoft.backend.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.model.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
    Ticket findById(String id);
    List<Ticket> findAll();
    void deleteById(String id);
    Page<Ticket> findByUserId(int userId, Pageable pageable);
}

package id.co.nexsoft.backend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.model.ERole;
import id.co.nexsoft.backend.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(ERole name);
}

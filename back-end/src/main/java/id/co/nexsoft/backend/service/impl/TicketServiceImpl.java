package id.co.nexsoft.backend.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.model.Ticket;
import id.co.nexsoft.backend.repository.TicketRepository;
import id.co.nexsoft.backend.service.TicketService;
import java.time.LocalDateTime;

@Service
public class TicketServiceImpl implements TicketService {
    @Autowired
    private TicketRepository repo;

    @Override
    public Page<Ticket> getTicketPages(int pageNumber) {
        Pageable paginatedData = PageRequest.of(pageNumber, 10);

        return repo.findAll(paginatedData);
    }

    @Override
    public Iterable<Ticket> getTickets() {
        return repo.findAll();
    }

    @Override
    public Iterable<Ticket> getUserTickets(int userId, int pageNumber) {
        Pageable paginatedData = PageRequest.of(pageNumber, 5);

        return repo.findByUserId(userId, paginatedData);
    }

    @Override
    public Ticket getTicketById(String id) {
        return repo.findById(id);
    }

    @Override
    public Ticket createTicket(Ticket ticket) {
        return repo.save(ticket);
    }

    @Override
    public void deleteTicketById(String id) {
        repo.deleteById(id);
    }

    @Override
    public ResponseEntity<Object> updateTicket(Ticket ticket, String id) {
        Optional<Ticket> ticketOptional = Optional.ofNullable(repo.findById(id));

        if (!ticketOptional.isPresent())
            return ResponseEntity.notFound().build();

        ticket.setId(id);
        repo.save(ticket);
        return ResponseEntity.noContent().build();
    }

    @Override
    public Ticket payTicket(String id) {
        Ticket ticket = repo.findById(id);
        LocalDateTime nowTime = LocalDateTime.now();
        ticket.setPaymentTime(nowTime);
        ticket.setExitTime(nowTime);
        if (ticket.getUser() != null && ticket.getUser().getMembership() != null
                && ticket.getUser().getMembership().getEnd().isAfter(nowTime.toLocalDate())
                && ticket.getUser().getMembership().getPlateNumber().equals(ticket.getPlateNumber())
        ) {
            ticket.setOnMembership(true);
        } else {
            ticket.setOnMembership(false);
        }

        return repo.save(ticket);
    }

    @Override
    public Ticket admitTicket(String id) {
        Ticket ticket = repo.findById(id);
        LocalDateTime nowTime = LocalDateTime.now();
        ticket.setEntryTime(nowTime);
        return repo.save(ticket);
    }
}

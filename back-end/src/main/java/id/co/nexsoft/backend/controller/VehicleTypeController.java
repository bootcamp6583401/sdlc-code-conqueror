package id.co.nexsoft.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import id.co.nexsoft.backend.model.VehicleType;
import id.co.nexsoft.backend.service.VehicleTypeService;

@RestController
@RequestMapping(path = "/vehicle-type")
public class VehicleTypeController {
    @Autowired
    private VehicleTypeService service;

    @GetMapping
    public Iterable<VehicleType> getAllVehicleTypes() {
        return service.getVehicleTypes();
    }

    @GetMapping("/all")
    public Iterable<VehicleType> getAllVehicleTypesPublic() {
        return service.getVehicleTypes();
    }

    @GetMapping("/{id}")
    public VehicleType getVehicleTypeById(@PathVariable(value = "id") int id) {
        return service.getVehicleTypeById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public VehicleType createVehicleType(@RequestBody VehicleType vehicleType) {
        return service.createVehicleType(vehicleType);
    }

    @DeleteMapping("/{id}")
    public void deleteVehicleType(@PathVariable(value = "id") int id) {
        service.deleteVehicleTypeById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateVehicleType(
            @RequestBody VehicleType vehicleType,
            @PathVariable int id) {
        return service.updateVehicleType(vehicleType, id);
    }
}
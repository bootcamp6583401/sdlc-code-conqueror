package id.co.nexsoft.backend.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.co.nexsoft.backend.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findById(int id);
    List<User> findAll();
    void deleteById(int id);

    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);

    Optional<User> findByEmail(String email);

    @Query(value = "SELECT * FROM user u WHERE LOWER(u.username) LIKE %:keyword% OR LOWER(u.email) LIKE %:keyword%", nativeQuery = true)
    Page<User> searchUsersByKeyword(String keyword, Pageable pageNumber);

}

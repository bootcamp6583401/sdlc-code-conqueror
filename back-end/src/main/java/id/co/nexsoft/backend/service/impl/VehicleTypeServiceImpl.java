package id.co.nexsoft.backend.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.model.VehicleType;
import id.co.nexsoft.backend.repository.VehicleTypeRepository;
import id.co.nexsoft.backend.service.VehicleTypeService;

@Service
public class VehicleTypeServiceImpl implements VehicleTypeService {
    @Autowired
    private VehicleTypeRepository repo;

    @Override
    public Iterable<VehicleType> getVehicleTypes() {
        return repo.findAll();
    }

    @Override
    public VehicleType getVehicleTypeById(int id) {
        return repo.findById(id);
    }

    @Override
    public VehicleType createVehicleType(VehicleType vehicleType) {
        return repo.save(vehicleType);
    }

    @Override
    public void deleteVehicleTypeById(int id) {
        repo.deleteById(id);
    }

    @Override
    public ResponseEntity<Object> updateVehicleType(VehicleType vehicleType, int id) {
        Optional<VehicleType> vehicleTypeOptional = Optional.ofNullable(repo.findById(id));

        if (!vehicleTypeOptional.isPresent())
            return ResponseEntity.notFound().build();

        vehicleType.setId(id);
        repo.save(vehicleType);
        return ResponseEntity.noContent().build();
    }
}

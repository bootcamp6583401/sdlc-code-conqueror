package id.co.nexsoft.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

public class TransactionStatusModel {

    @JsonProperty("status_code")
    @Getter
    @Setter
    private int statusCode;

    @JsonProperty("transaction_id")
    @Getter
    @Setter
    private String transactionId;

    @JsonProperty("gross_amount")
    @Getter
    @Setter
    private double grossAmount;
    public TransactionStatusModel() {
    }


    public TransactionStatusModel(int statusCode, String transactionId, double grossAmount) {
        this.statusCode = statusCode;
        this.transactionId = transactionId;
        this.grossAmount = grossAmount;
    }

   
}

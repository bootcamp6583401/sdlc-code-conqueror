package id.co.nexsoft.backend.controller;

import org.springframework.aot.generate.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import id.co.nexsoft.backend.model.Membership;
import id.co.nexsoft.backend.service.MembershipService;

@RestController
@RequestMapping(path = "/membership")
@Generated
public class MembershipController {
    @Autowired
    private MembershipService service;

    @GetMapping
    public Iterable<Membership> getAllMemberships() {
        return service.getMemberships();
    }

    @GetMapping("/{id}")
    public Membership getMembershipById(@PathVariable(value = "id") int id) {
        return service.getMembershipById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Membership createMembership(@RequestBody Membership membership) {
        return service.createMembership(membership);
    }

    @DeleteMapping("/{id}")
    public void deleteMembership(@PathVariable(value = "id") int id) {
        service.deleteMembershipById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateMembership(
            @RequestBody Membership membership,
            @PathVariable int id) {
        return service.updateMembership(membership, id);
    }
}
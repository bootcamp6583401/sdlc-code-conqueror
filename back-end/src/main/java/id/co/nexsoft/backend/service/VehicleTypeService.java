package id.co.nexsoft.backend.service;

import org.springframework.http.ResponseEntity;

import id.co.nexsoft.backend.model.VehicleType;

public interface VehicleTypeService {
    Iterable<VehicleType> getVehicleTypes();
    VehicleType getVehicleTypeById(int id);
    VehicleType createVehicleType(VehicleType vehicleType);
    void deleteVehicleTypeById(int id);
    ResponseEntity<Object> updateVehicleType(VehicleType vehicleType, int id);
}
